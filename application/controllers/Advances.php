<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advances extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($name_id=0, $start=0) {
		
		$this->template_data->set('name_id', $name_id);

		$total = new $this->Advances_liquidation_model('a');
		$total->set_select('COUNT(*) as all_count');
			$c_liq = "SELECT COUNT(*) FROM advances_liquidation a2 WHERE a2.liquidated=1";
			$c_uliq = "SELECT COUNT(*) FROM advances_liquidation a2 WHERE a2.liquidated=0";
			if( $name_id ) {
				$total->setNameId($name_id,true);
				$c_liq .= " AND a2.name_id=" . $name_id;
				$c_uliq .= " AND a2.name_id=" . $name_id;
			}
			if( $this->input->get('group') ) {
				$total->setGroupId($this->input->get('group'),true);
				$c_liq .= " AND a2.group_id=" . $this->input->get('group');
				$c_uliq .= " AND a2.group_id=" . $this->input->get('group');
			}
			if( $this->input->get('class') ) {
				$total->setClassId($this->input->get('class'),true);
				$c_liq .= " AND a2.class_id=" . $this->input->get('class');
				$c_uliq .= " AND a2.class_id=" . $this->input->get('class');
			}
		$total->set_select("({$c_liq}) as liquidated");
		$total->set_select("({$c_uliq}) as unliquidated");
		$this->template_data->set('total', $total->get());

		$this->template_data->set('filter', (($this->input->get('filter')) ? $this->input->get('filter') : 'unliquidated'));

		$adv = new $this->Advances_liquidation_model('a');
		$adv->set_select('a.*');
		$adv->set_select('a.amount as adv_amount');
		$adv->set_select('a.id as adv_id');
		$adv->set_select('(SELECT p.payee FROM payee p WHERE p.id=a.name_id) as payee');
		$adv->set_select('(SELECT SUM(al.amount) FROM advances_liquidated al WHERE al.adv_id=a.id) as liquidated_amount');
		switch($this->input->get('filter')) {
			case 'all':
			break;
			case 'liquidated':
				$adv->set_where('a.liquidated=1');
			break;
			case 'unliquidated':
			default:
				$adv->set_where('a.liquidated=0');
			break;
		}
		if( $name_id ) {
			$adv->setNameId($name_id,true);
		}
		if( $this->input->get('group') ) {
			$adv->setGroupId($this->input->get('group'),true);
		}
		if( $this->input->get('class') ) {
			$adv->setClassId($this->input->get('class'),true);
		}
		$adv->set_join('reports_disbursements rd', 'rd.id=a.db_id');
		$adv->set_select('rd.*');
		$adv->set_select('(SELECT c.cv_id FROM checks c WHERE c.db_id=a.db_id AND c.voided=0 LIMIT 1) as cv_id');
		//$adv->set_limit(2);
		$adv->set_start($start);
		$adv->set_order('rd.report_date', 'ASC');
		$adv->set_order('cv_id', 'ASC');
		$this->template_data->set('advances', $adv->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => site_url("advances/index/{$name_id}"),
			'total_rows' => $adv->count_all_results(),
			'per_page' => $adv->get_limit(),
		), '?filter=' . $this->input->get('filter') ));

		$names = new $this->Advances_liquidation_model('a');
		$names->set_select('a.name_id');
		$names->set_select('(SELECT p.payee FROM payee p WHERE p.id=a.name_id) as payee');
		$names->set_group_by('a.name_id');
		$names->set_order('payee', 'ASC');
		$names->set_limit(0);
		$this->template_data->set('names', $names->populate());

		$current_name = new $this->Payee_model('p');
		$current_name->setId($name_id,true);
		$this->template_data->set('current_name', $current_name->get());

		$adv_groups = new $this->Advances_groups_model;
		$adv_groups->set_order('name', 'ASC');
		$adv_groups->set_limit(0);
		$this->template_data->set('adv_groups', $adv_groups->populate());

		$current_group = new $this->Advances_groups_model;
		$current_group->setId($this->input->get('group'),true);
		$this->template_data->set('current_group', $current_group->get());

		$current_class = new $this->Advances_groups_model;
		$current_class->setId($this->input->get('class'),true);
		$this->template_data->set('current_class', $current_class->get());

		$this->load->view('advances/advances', $this->template_data->get_data());
	}

	public function liquidated($adv_id) {
		$adliq = new $this->Advances_liquidation_model();
		$adliq->setId($adv_id,true,false);
		$adliq->setLiquidated(1,false,true);
		$adliq->update();
		redirect(site_url("advances/liquidate/{$adv_id}") . "?next=" . $this->input->get('next'), 'location');
	}

	public function unliquidated($adv_id) {
		$adliq = new $this->Advances_liquidation_model();
		$adliq->setId($adv_id,true,false);
		$adliq->setLiquidated(0,false,true);
		$adliq->update();
		redirect(site_url("advances/liquidate/{$adv_id}") . "?next=" . $this->input->get('next'), 'location');
	}

	public function liquidate($adv_id) {

		$liq = new $this->Advances_liquidated_model();
		$liq->setAdvId($adv_id,true);

		if( $this->input->get('delete_item') ) {
			$liq->setId($this->input->get('delete_item'),true);
			$liq->delete();
			redirect( site_url( uri_string() ) . "?next=" . $this->input->get('next') );
		}

		if( $this->input->post('action') == 'add_item' ) {
			$liq->setCvId($this->input->post('cv_id'));
			$liq->setCvDate( date('Y-m-d', strtotime($this->input->post('cv_date')) ) );
			$liq->setAmount(str_replace(",", "", $this->input->post('amount')) );
			$liq->setNum($this->input->post('number'));
			$liq->setNotes($this->input->post('notes'));
			$liq->insert();
			//redirect( (($this->input->get('next')) ? $this->input->get('next') : uri_string()) , 'location');
		}
		$this->template_data->set('liquidated', $liq->populate());

		$adv = new $this->Advances_liquidation_model('a');
		$adv->setId($adv_id,true);
		$adv->set_select('a.*');
		$adv->set_select('a.amount as adv_amount');
		$adv->set_select('a.id as adv_id');
		$adv->set_select('a.liquidated as status');
		$adv->set_select('(SELECT c.cv_id FROM checks c WHERE c.db_id=a.db_id AND c.voided=0 LIMIT 1) as cv_id');
		$adv->set_select('(SELECT p.payee FROM payee p WHERE p.id=a.name_id) as payee');
		$adv->set_join('reports_disbursements rd', 'rd.id=a.db_id');
		$adv->set_select('rd.*');
		$adv->set_select('(SELECT SUM(al.amount) FROM advances_liquidated al WHERE al.adv_id=a.id) as liquidated');
		$adv_data = $adv->get();
		$this->template_data->set('adv', $adv_data); 

		if( ($adv_data->status==0) && ($this->input->get('edit_item')) ) {
			$current_item = new $this->Advances_liquidated_model();
			$current_item->setId($this->input->get('edit_item'), true );

			if( $this->input->post('action') == 'edit_item' ) {
				$current_item->setCvId($this->input->post('cv_id'),false,true);
				$current_item->setCvDate( date('Y-m-d', strtotime($this->input->post('cv_date')) ),false,true );
				$current_item->setAmount(str_replace(",", "", $this->input->post('amount')),false,true );
				$current_item->setNum($this->input->post('number'),false,true);
				$current_item->setNotes($this->input->post('notes'),false,true);
				$current_item->update();
				redirect( site_url(uri_string()) . "?next=" . $this->input->get('next') );
			}

			$this->template_data->set('current_item', $current_item->get());
		}

		$this->load->view('advances/liquidate', $this->template_data->get_data());
	}


	public function groups() {
		$groups = new $this->Advances_groups_model;
		$groups->set_order('type', 'ASC');
		$groups->set_order('name', 'ASC');
		$groups->set_limit(0);
		$groups_data = $groups->populate();

		$this->template_data->set('groups', $groups_data);
		$this->load->view('advances/groups/index', $this->template_data->get_data());
	}

	public function groups_add() {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Fund Name', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$groups = new $this->Advances_groups_model;
				$groups->setName($this->input->post('name')); 
				$groups->setPriority($this->input->post('priority')); 
				$groups->setType($this->input->post('type')); 
				$groups->insert();
				redirect( site_url(($this->input->get('next')) ? $this->input->get('next') : 'advances/groups') . "?new_id=" . $groups->get_inserted_id() );
			}
		}
		$this->load->view('advances/groups/add', $this->template_data->get_data());
	}

	public function groups_edit($id) {
		$groups = new $this->Advances_groups_model;
		$groups->setId($id,true,false);
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Group Name', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$groups->setName($this->input->post('name'),false,true); 
				$groups->setPriority($this->input->post('priority'),false,true); 
				$groups->setType($this->input->post('type'),false,true); 
				if( $groups->nonEmpty() ) {
					$groups->update();
				} 
				redirect(($this->input->get('next')) ? $this->input->get('next') :'advances/groups');
			}
		}
		$this->template_data->set('group', $groups->get());
		$this->load->view('advances/groups/edit', $this->template_data->get_data());
	}

	public function groups_delete($id) {
		$groups = new $this->Advances_groups_model;
		$groups->setId($id,true);
		$groups->delete();
		redirect(($this->input->get('next')) ? $this->input->get('next') :'advances/groups');
	}

}
