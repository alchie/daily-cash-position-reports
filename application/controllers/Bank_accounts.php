<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_accounts extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		redirect('funds');
	}

	public function add($fund_id) {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required');
			$this->form_validation->set_rules('account_number', 'Account Number', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$bank_accounts = new $this->Bank_accounts_model;
				$bank_accounts->setFundId($fund_id); 
				$bank_accounts->setBankName($this->input->post('bank_name')); 
				$bank_accounts->setAccountNumber($this->input->post('account_number')); 
				$bank_accounts->setAcctType($this->input->post('acct_type')); 
				$bank_accounts->setFundCategory($this->input->post('fund_category')); 
				$bank_accounts->setPriority($this->input->post('priority')); 
				$bank_accounts->setClass($this->input->post('class')); 
				$bank_accounts->setBranch($this->input->post('branch')); 
				$bank_accounts->setMemo($this->input->post('memo')); 
				$bank_accounts->setShortName($this->input->post('short_name')); 
				$bank_accounts->insert();
				redirect('bank_accounts');
			}
		}

		$funds = new $this->Funds_model;
		$funds->set_select("*");
		$funds->set_select("(SELECT name FROM fund_groups WHERE id=funds.group) as group_name");
		$funds->set_select("(SELECT COUNT(*) FROM bank_accounts WHERE fund_id=funds.id) as bank_accounts_count");
		$funds->set_order('priority', 'ASC');
		$funds->set_limit(0);
		$funds_data = $funds->populate();
		$this->template_data->set('funds_data', $funds_data);

		$groups = new $this->Fund_groups_model;
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		$groups->setType('class',true); 
		$groups_data = $groups->populate();
		$this->template_data->set('classes', $groups_data);

		$this->load->view('bank_accounts/add', $this->template_data->get_data());
	}

	public function edit($id) {
		$bank_accounts = new $this->Bank_accounts_model;
		$bank_accounts->setId($id,true,false);
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required');
			$this->form_validation->set_rules('account_number', 'Account Number', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$bank_accounts->setBankName($this->input->post('bank_name'),false,true); 
				$bank_accounts->setAccountNumber($this->input->post('account_number'),false,true);
				$bank_accounts->setAcctType($this->input->post('acct_type'),false,true);  
				$bank_accounts->setFundCategory($this->input->post('fund_category'),false,true); 
				$bank_accounts->setFundId($this->input->post('fund_id'),false,true); 
				$bank_accounts->setPriority($this->input->post('priority'),false,true); 
				$bank_accounts->setClass($this->input->post('class'),false,true); 
				$bank_accounts->setBranch($this->input->post('branch'),false,true); 
				$bank_accounts->setMemo($this->input->post('memo'),false,true); 
				$bank_accounts->setShortName($this->input->post('short_name'),false,true); 
				if( $bank_accounts->nonEmpty() ) {
					$bank_accounts->update();
				} 
				redirect('bank_accounts');
			}
		}

		$funds = new $this->Funds_model;
		$funds->set_select("*");
		$funds->set_select("(SELECT name FROM fund_groups WHERE id=funds.group) as group_name");
		$funds->set_select("(SELECT COUNT(*) FROM bank_accounts WHERE fund_id=funds.id) as bank_accounts_count");
		$funds->set_order('priority', 'ASC');
		$funds->set_limit(0);
		$funds_data = $funds->populate();
		$this->template_data->set('funds_data', $funds_data);

		$groups = new $this->Fund_groups_model;
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		$groups->setType('class',true); 
		$groups_data = $groups->populate();
		$this->template_data->set('classes', $groups_data);

		$this->template_data->set('bank_account', $bank_accounts->get());
		$this->load->view('bank_accounts/edit', $this->template_data->get_data());
	}

	public function delete($id) {
		$bank_accounts = new $this->Bank_accounts_model;
		$bank_accounts->setId($id,true,false);
		$bank_accounts->setActive(0,false,true);
		$bank_accounts->update();
		redirect('bank_accounts');
	}

	public function restore($id) {
		$bank_accounts = new $this->Bank_accounts_model;
		$bank_accounts->setId($id,true,false);
		$bank_accounts->setActive(1,false,true);
		$bank_accounts->update();
		redirect($this->input->get('next'));
	}

	public function details($id) {

		$start = date('Y-m-d', strtotime($this->input->get('start')));
		$end = date('Y-m-d', strtotime($this->input->get('end'))) ;

		$bank_accounts = new $this->Bank_accounts_model('ba');
		$bank_accounts->setId($id,true);
		$bank_accounts->set_select('*');

		$type_1 = ($this->input->get('type')) ? "rd.type='{$this->input->get('type')}' AND" : "";
		$type_2 = ($this->input->get('type')) ? "rd2.type='{$this->input->get('type')}' AND" : "";

		// beginning
		$bank_accounts->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE '.$type_1.' rd.bank_id=ba.id AND rd.report_date<\''.$start.'\') as beg_deposit');
		$bank_accounts->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE '.$type_2.' rd2.bank_id=ba.id AND rd2.report_date<\''.$start.'\') as beg_disbursement');
		// ending
		$bank_accounts->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE '.$type_1.' rd.bank_id=ba.id AND rd.report_date<=\''.$end.'\') as end_deposit');
		$bank_accounts->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE '.$type_2.' rd2.bank_id=ba.id AND rd2.report_date<=\''.$end.'\') as end_disbursement');
		$bank_accounts_data = $bank_accounts->get();

		$this->template_data->set('bank_account', $bank_accounts_data);

		$dep_type = ($this->input->get('type')) ? "dep.type='{$this->input->get('type')}' AND" : "";
		$dis_type = ($this->input->get('type')) ? "dis.type='{$this->input->get('type')}' AND" : "";

		$dep_sql = "SELECT dep.bank_id,dep.report_date,dep.description,dep.type,dep.amount,dep.id, 'dep' as item_type FROM reports_deposits dep WHERE {$dep_type} dep.bank_id= '{$id}' AND dep.report_date >= '{$start}' AND dep.report_date <= '{$end}'";
		$dis_sql = "SELECT dis.bank_id,dis.report_date,dis.description,dis.type,dis.amount,dis.id, 'dis' as item_type FROM reports_disbursements dis WHERE {$dis_type} dis.bank_id= '{$id}' AND dis.report_date >= '{$start}' AND dis.report_date <= '{$end}'";

		if( $this->input->get('q') ) {
			$q_val = $this->input->get('q');
			
			$dep_sql .= " AND (";
			$dep_sql .= "(dep.description LIKE '%{$q_val}%')";
			$dep_sql .= " OR (dep.amount = '{$q_val}')";
			$dep_sql .= ")";

			$dis_sql .= " AND (";
			$dis_sql .= "(dis.description LIKE '%{$q_val}%')";
			$dis_sql .= " OR (dis.amount = '{$q_val}')";
			$dis_sql .= ")";
		}
		
		$query = $this->db->query("{$dep_sql} UNION ALL {$dis_sql} ORDER BY report_date ASC;");
		$results = array();
		foreach ($query->result() as $row) {
			$results[] = $row;
		}

		$this->template_data->set('items', $results);
		$this->load->view('bank_accounts/details_items', $this->template_data->get_data());
		
	}

	public function details2($id, $type='deposits', $start=0) {
		$bank_accounts = new $this->Bank_accounts_model;
		$bank_accounts->setId($id,true);
		$this->template_data->set('bank_account', $bank_accounts->get());

		if( $type == 'deposits') {
			$deposits = new $this->Reports_deposits_model('d');
			$deposits->setBankId($id, true);
			$deposits->set_order('report_date', 'DESC');
			$deposits->set_start($start);
			$deposits->set_where('report_date >=', date('Y-m-d', strtotime($this->input->get('start'))) );
			$deposits->set_where('report_date <=', date('Y-m-d', strtotime($this->input->get('end'))) );
			$deposits->set_limit(20);
			$this->template_data->set('items', $deposits->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 5,
				'base_url' => site_url("bank_accounts/details/{$id}/{$type}"),
				'total_rows' => $deposits->count_all_results(),
				'per_page' => $deposits->get_limit(),
			), '?start='.urlencode($this->input->get('start')).'&end='.urlencode($this->input->get('end'))));

			$this->load->view('bank_accounts/details_deposits', $this->template_data->get_data());
		} elseif( $type == 'disbursements') {

			$disbursements = new $this->Reports_disbursements_model('d');
			$disbursements->setBankId($id, true);
			$disbursements->set_order('report_date', 'DESC');
			$disbursements->set_start($start);
			$disbursements->set_where('report_date >=', date('Y-m-d', strtotime($this->input->get('start'))) );
			$disbursements->set_where('report_date <=', date('Y-m-d', strtotime($this->input->get('end'))) );
			$disbursements->set_limit(20);
			$this->template_data->set('items', $disbursements->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 5,
				'base_url' => site_url("bank_accounts/details/{$id}/{$type}"),
				'total_rows' => $disbursements->count_all_results(),
				'per_page' => $disbursements->get_limit(),
			), '?start='.urlencode($this->input->get('start')).'&end='.urlencode($this->input->get('end'))));

			$this->load->view('bank_accounts/details_disbursements', $this->template_data->get_data());
		} 
	}

	private function _fund_accounts() {
		$funds = new $this->Funds_model;
		$funds->set_order('priority', 'ASC');
		$funds->set_limit(0);
		$funds_data = $funds->populate();
		foreach( $funds_data as $i=>$fd ) {
			$bank_accounts = new $this->Bank_accounts_model('ba');
			$bank_accounts->setFundId($fd->id,true);
			$bank_accounts->set_limit(0);
			$bank_accounts->set_order('ba.priority', 'ASC');
			$bank_accounts->set_select('ba.*');
			$funds_data[$i]->bank_accounts = $bank_accounts->populate();
		}
		$this->template_data->set('funds', $funds_data);
	}
	public function edit_item($type, $id) {

		$this->template_data->set('item_type', $type);

		$redirect_url = site_url($this->input->get('back'));
		$redirect_url .= "?next=" . $this->input->get('next');
		$redirect_url .= "&start=" . $this->input->get('start');
		$redirect_url .= "&end=" . $this->input->get('end');
		$this->template_data->set('redirect_url', $redirect_url);

		$delete_item_url = site_url("bank_accounts/delete_item/{$type}/{$id}");
		$delete_item_url .= "?next=" . $this->input->get('next');
		$delete_item_url .= "&start=" . $this->input->get('start');
		$delete_item_url .= "&end=" . $this->input->get('end');
		$delete_item_url .= "&back=" . $this->input->get('back');
		$this->template_data->set('delete_item_url', $delete_item_url);

		if( $type == 'deposit') {
			$item = new $this->Reports_deposits_model('d');
			$item->setId($id, true, false);
			if( $item->nonEmpty() ) {
				$item_data = $item->get_results();
				if( $this->input->post() ) {
					$this->form_validation->set_rules('date', 'Date', 'trim|required');
					$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
					if( $this->form_validation->run() ) {
						$item->setReportDate(date('Y-m-d', strtotime($this->input->post('date'))), false, true);
						$item->setAmount(str_replace(',', '', $this->input->post('amount')), false, true);
						$item->setDescription($this->input->post('description'), false, true);
						if( $this->input->post('bank_id') ) {
							$item->setBankId($this->input->post('bank_id'), false, true);
						}
						$item->setType($this->input->post('type'), false, true);
						$item->setCompany($this->input->post('company'), false, true);
						if( $item->update() ) {
							$dbm = new $this->Reports_disbursements_model('d');
							$dbm->setId($item_data->dbm_id,true);
							if( $dbm->nonEmpty() ) {
								$dbm->setReportDate(date('Y-m-d', strtotime($this->input->post('date'))), false, true);
								$dbm->setAmount(str_replace(',', '', $this->input->post('amount')), false, true);
								$dbm->setType($this->input->post('type'), false, true);
								$dbm->setCompany($this->input->post('company'), false, true);
								$dbm->update();
							}
						}
					} 
					if( $this->input->get('back') ) {
						redirect( $this->input->get('back') . "?next=" .$this->input->get('next') . "&start=" . $this->input->get('start') . "&end=" . $this->input->get('end') );
					}
				}
			}
			$this->_fund_accounts();
			$this->template_data->set('item', $item->get());
			$this->load->view('bank_accounts/edit_item_deposit', $this->template_data->get_data());
		} elseif( $type == 'disbursement') {
			$item = new $this->Reports_disbursements_model('d');
			$item->setId($id, true);
			if( $item->nonEmpty() ) {
				$item_data = $item->get_results();
				if( $this->input->post() ) {
					$this->form_validation->set_rules('date', 'Date', 'trim|required');
					$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
					if( $this->form_validation->run() ) {
						$item->setReportDate(date('Y-m-d', strtotime($this->input->post('date'))), false, true);
						$item->setAmount(str_replace(',', '', $this->input->post('amount')), false, true);
						$item->setDescription($this->input->post('description'), false, true);
						if( $this->input->post('bank_id') ) {
							$item->setBankId($this->input->post('bank_id'), false, true);
						}
						$item->setType($this->input->post('type'), false, true);
						$item->setCompany($this->input->post('company'), false, true);
						$item->update();

						$deposit = new $this->Reports_deposits_model;
						$deposit->setId($item_data->dp_id,true,false);
						if( $deposit->nonEmpty() ) {
							$deposit->setBankId($this->input->post('dest_id'),false,true);
							$deposit->update();
						}
					} 
					if( ($this->input->get('back')) && ($this->input->post('type')!='fund_transfer')) {
						redirect( $this->input->get('back') . "?next=" .$this->input->get('next') . "&start=" . $this->input->get('start') . "&end=" . $this->input->get('end') );
					}

				}
			}
			$item->set_select("*");
			$item->set_select("(SELECT rd.bank_id FROM  reports_deposits rd WHERE rd.id=d.dp_id) as dest_id");
			$this->_fund_accounts();
			$this->template_data->set('item', $item->get());
			$this->load->view('bank_accounts/edit_item_disbursement', $this->template_data->get_data());
		} else {
			redirect($redirect_url);
		}
	}

	public function delete_item($type, $id) {
		
		$redirect_url = site_url($this->input->get('back'));
		$redirect_url .= "?next=" . $this->input->get('next');
		$redirect_url .= "&start=" . $this->input->get('start');
		$redirect_url .= "&end=" . $this->input->get('end');

		if( $type == 'deposit') {
			$item = new $this->Reports_deposits_model;
			$item->setId($id, true);
			$item->delete();
		} elseif( $type == 'disbursement') {
			$item = new $this->Reports_disbursements_model;
			$item->setId($id, true);
			$item->delete();
		}

		redirect($redirect_url);
	}

	public function write_check($m,$d,$y,$bank_id) {

		if($this->input->post()) {
			$disbursement = new $this->Reports_disbursements_model;
			$disbursement->setBankId( $bank_id );
			$disbursement->setAmount( str_replace(",", '', $this->input->post('amount')));
			$disbursement->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ) );
			$disbursement->setDescription($this->input->post('description'));
			$disbursement->setType('check');
			if( $disbursement->insert() ) {
				$db_id = $disbursement->get_inserted_id();
				$check_items = new $this->Checks_model('c');
				$check_items->setAcctId($bank_id,true,false);
				$check_items->setCheckNumber($this->input->post('check_number'),true,false);
				$check_items->setPayeeId($this->input->post('payee_id'),false,true);
				$check_items->setDbId($db_id,false,true);
				$check_items->setCvId($this->input->post('voucher_number'),false,true);
				$check_items->setSignatory1($this->input->post('signatory1'),false,true);
				$check_items->setSignatory2($this->input->post('signatory2'),false,true);
				$check_items->setVoided(0,false,true);
				if( $check_items->nonEmpty() ) {
					$check_items->update();
					$results = $check_items->get_results();
					redirect( uri_string() );
				}
			}
		}
		
		$this->template_data->set('current_month', $m);
		$this->template_data->set('current_day', $d);
		$this->template_data->set('current_year', $y);

        $current_account = new $this->Bank_accounts_model('ba');
        $current_account->setId($bank_id,true);
        $current_account->set_select('ba.*');
        $current_account->set_select('(SELECT f.name FROM funds f WHERE f.id=ba.fund_id) as fund_name');
        $this->template_data->set('current_account', $current_account->get());

        $bank_accounts = new $this->Bank_accounts_model('ba');
        $bank_accounts->setFundCategory('disposable',true);
        $bank_accounts->set_limit(0);
        $bank_accounts->set_order('ba.fund_id', 'ASC');
        $bank_accounts->set_select('ba.*');
        $bank_accounts->set_select('(SELECT f.name FROM funds f WHERE f.id=ba.fund_id) as fund_name');
        $this->template_data->set('disposables', $bank_accounts->populate());

		$check_items = new $this->Checks_model('c');
		$check_items->setAcctId($bank_id,true,false);
		$check_items->set_limit(0);
		$check_items->set_start(0);
		$check_items->set_select('c.*');
		$check_items->set_order('c.check_number', "ASC");
		$check_items->set_where('((c.db_id IS NULL)');
		$check_items->set_where_or('(c.db_id = 0))');
		$this->template_data->set('check_items', $check_items->populate());

		$payees = new $this->Payee_model('p');
		$payees->set_limit(0);
		$payees->set_order('p.payee', 'ASC');
		$payees->setTrash(0,true);
		$this->template_data->set('payees', $payees->populate());

		$signatories1 = new $this->Signatory_model('p');
		$signatories1->setSignatory1(1,true);
		$signatories1->set_order('p.signatory', 'DESC');
		$signatories1->set_limit(0);
		$this->template_data->set('signatories1', $signatories1->populate());

		$signatories2 = new $this->Signatory_model('p');
		$signatories2->setSignatory2(1,true);
		$signatories2->set_order('p.signatory', 'DESC');
		$signatories2->set_limit(0);
		$this->template_data->set('signatories2', $signatories2->populate());

		$check_items = new $this->Checks_model('c');
		$check_items->setAcctId($bank_id,true);
		$check_items->set_select('*');
		$check_items->set_select('c.id as check_id');
		$check_items->set_join('reports_disbursements rd','rd.id=c.db_id');
		$check_items->set_order('c.check_number', 'DESC');
		$check_items->set_where('((c.db_id IS NOT NULL) AND (c.db_id <> 0))');
		$check_items->set_where('rd.type="check"');
		$this->template_data->set('latest', $check_items->populate());

		$this->load->view('bank_accounts/write_check', $this->template_data->get_data());
	}

	public function collections($type=NULL) {

		$start = date('Y-m-d', strtotime($this->input->get('start')));
		$end = date('Y-m-d', strtotime($this->input->get('end'))) ;
		$this->template_data->set('collection', ($type) ? strtoupper($type) : 'ALL');

		$bank_accounts = new $this->Reports_deposits_model('rd');
		$bank_accounts->set_select('SUM(rd.amount) as beg_deposit');
		$bank_accounts->set_where('rd.report_date<\''.$start.'\'');
		if( $type ) {
			$bank_accounts->set_where('rd.type=\''.$type.'\'');
		}
		$company = ($this->input->get('company')) ? $this->input->get('company') : 'rcbdi';
		$bank_accounts->set_where('rd.company=\''.$company.'\'');
		
		$bank_accounts->set_where('rd.bank_id=0');

		$type_sql_rd = ($type) ? " AND rd3.type='{$type}' " : '';
		$type_sql_rd2 = ($type) ? " AND rd2.type='{$type}' " : '';

		$bank_accounts->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=rd.company AND rd2.bank_id=0 '.$type_sql_rd2.' AND rd2.report_date<\''.$start.'\') as beg_disbursement');
		// ending
		$bank_accounts->set_select('(SELECT SUM(rd3.amount) FROM reports_deposits rd3 WHERE rd3.company=rd.company AND rd3.bank_id=0 '.$type_sql_rd.' AND rd3.report_date<=\''.$end.'\') as end_deposit');

		$bank_accounts->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=rd.company AND rd2.bank_id=0 '.$type_sql_rd2.' AND rd2.report_date<=\''.$end.'\') as end_disbursement');
		$bank_accounts_data = $bank_accounts->get();

		$this->template_data->set('bank_account', $bank_accounts_data);

		$type_sql_dep = ($type) ? " AND dep.type='{$type}' " : '';
		$type_sql_dis = ($type) ? " AND dis.type='{$type}' " : '';

		$dep_sql = "SELECT dep.bank_id,dep.report_date,dep.description,dep.type,dep.amount,dep.id, 'dep' as item_type, '' as deposited_to FROM reports_deposits dep WHERE dep.company= '{$company}' AND dep.bank_id= '0' {$type_sql_dep} AND dep.report_date >= '{$start}' AND dep.report_date <= '{$end}'";
		$dis_sql = "SELECT dis.bank_id,dis.report_date,dis.description,dis.type,dis.amount,dis.id, 'dis' as item_type, (SELECT CONCAT(ba.bank_name, ' (', ba.account_number, ')' ) FROM  bank_accounts ba WHERE ba.id=(SELECT rd2.bank_id FROM reports_deposits rd2 WHERE rd2.id=dis.dp_id)) as deposited_to FROM reports_disbursements dis WHERE dis.company= '{$company}' AND dis.bank_id= '0' {$type_sql_dis} AND dis.report_date >= '{$start}' AND dis.report_date <= '{$end}'";

		if( $this->input->get('q') ) {
			$q_val = $this->input->get('q');
			
			$dep_sql .= " AND (";
			$dep_sql .= "(dep.description LIKE '%{$q_val}%')";
			$dep_sql .= " OR (dep.amount = '{$q_val}')";
			$dep_sql .= ")";

			$dis_sql .= " AND (";
			$dis_sql .= "(dis.description LIKE '%{$q_val}%')";
			$dis_sql .= " OR (dis.amount = '{$q_val}')";
			$dis_sql .= ")";
		}
		
		$query = $this->db->query("{$dep_sql} UNION ALL {$dis_sql} ORDER BY report_date ASC, item_type ASC;");
		$results = array();
		foreach ($query->result() as $row) {
			$results[] = $row;
		}

		$this->template_data->set('items', $results);
		$this->load->view('bank_accounts/details_collections', $this->template_data->get_data());
		
	}

}
