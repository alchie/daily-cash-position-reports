<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_templates extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {

		$templates = new $this->Check_templates_model('ct');
		$templates->set_order('ct.name', 'ASC');
		$templates->set_start($start);
		$this->template_data->set('templates', $templates->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => site_url("check_templates/index"),
			'total_rows' => $templates->count_all_results(),
			'per_page' => $templates->get_limit(),
		)));

		$this->load->view('check_templates/check_templates', $this->template_data->get_data());
	}

	public function add() {

		if( $this->input->post('name') ) {
			$templates = new $this->Check_templates_model('ct');
			$templates->setName($this->input->post('name'),true);
			if($templates->nonEmpty()==FALSE) {
				$templates->insert();
				redirect('check_templates/edit/' . $templates->get_inserted_id());
			}
		}

		$this->load->view('check_templates/check_templates_add', $this->template_data->get_data());
	}

	public function edit($id) {
		$templates = new $this->Check_templates_model('ct');
		$templates->setId($id,true);

		if( $this->input->post() ) {
			$templates->setName($this->input->post('name'),false,true);
			$templates->setActive((($this->input->post('active'))?1:0),false,true);
			$templates->setSettings(json_encode($this->input->post('settings')),false,true);
			$templates->update();
		}

		$this->template_data->set('template', $templates->get());
		$this->load->view('check_templates/check_templates_edit', $this->template_data->get_data());
	}

	public function set_as_default($id) {
		$templates = new $this->Check_templates_model('ct');
		$templates->setDefault(0,false,true);
		$templates->set_where('ct.default=1');
		$templates->update();

		$templates2 = new $this->Check_templates_model('ct');
		$templates2->setDefault(1,false,true);
		$templates2->setId($id,true);
		$templates2->update();

		redirect("check_templates/edit/{$id}");
	}

}
