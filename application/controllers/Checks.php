<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checks extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($acct_id, $start=0) {

		$bank_account = new $this->Bank_accounts_model('b');
		$bank_account->setId($acct_id,true);
		$bank_account->set_join('funds f', 'f.id=b.fund_id');
		$bank_account->set_select('f.name as fund_name');
		$bank_account->set_select('b.*');
		$bank_account->set_select('(SELECT COUNT(*) FROM checks c JOIN reports_disbursements rd ON rd.id=c.db_id WHERE c.acct_id=b.id AND ((c.db_id IS NOT NULL) AND (c.db_id <> 0)) ) as total_assigned');
		$bank_account->set_select('(SELECT COUNT(*) FROM checks c WHERE c.acct_id=b.id AND ((c.db_id IS NULL) OR (c.db_id = 0) )) as total_unassigned');
		$bank_account->set_select('(SELECT COUNT(*) FROM checks c WHERE c.acct_id=b.id) as total_checks');
		$this->template_data->set('bank_account', $bank_account->get()); 

		$check_items = new $this->Checks_model('c');
		$check_items->setAcctId($acct_id,true);
		$check_items->set_limit(10);
		$check_items->set_start($start);
		$check_items->set_select('c.*');
		$check_items->set_join('reports_disbursements rd', 'rd.id=c.db_id');
		$check_items->set_select('rd.report_date as check_date');
		$check_items->set_select('rd.amount as amount');
		$check_items->set_select('rd.id as rd_id');
		//$check_items->set_where('(rd.id IS NOT NULL)');
		
		$check_items->set_join('payee p', 'p.id=c.payee_id');
		$check_items->set_select('p.payee as payee_name');
		
		if( $this->input->get('q') ) {
			$check_items->set_where('(rd.amount = "'.$this->input->get('q').'"', NULL, 80);
			$check_items->set_where_or('c.check_number = "'.$this->input->get('q').'"', NULL, 81);
			$check_items->set_where_or('rd.report_date LIKE "%'.$this->input->get('q').'%"', NULL, 82);
			$check_items->set_where_or('p.payee LIKE "%'.$this->input->get('q').'%")', NULL, 83);
		}

		switch($this->input->get('filter')) {
			case 'all':
			break;
			case 'unassigned':
				$check_items->set_where('((c.db_id IS NULL)');
				$check_items->set_where_or('(c.db_id = 0))');
			break;
			case 'assigned':
			default:
				$check_items->set_where('c.db_id IS NOT NULL');
				$check_items->set_where('c.db_id <> 0');
			break;
		}

		$check_items->set_order('c.check_number', "DESC");
		
		$this->template_data->set('check_items', $check_items->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => site_url("checks/index/{$acct_id}"),
				'total_rows' => $check_items->count_all_results(),
				'per_page' => $check_items->get_limit(),
			), '?filter=' . $this->input->get('filter') . '&q=' . $this->input->get('q') ));

		$this->template_data->set('filter', (($this->input->get('filter')) ? $this->input->get('filter') : 'assigned'));

		$checking_accounts = new $this->Bank_accounts_model('b');
		$checking_accounts->setAcctType('checking',true);
		$checking_accounts->set_where('b.id!=' . $acct_id);
		$checking_accounts->set_limit(0);
		$checking_accounts->set_join('funds f', 'f.id=b.fund_id');
		$checking_accounts->set_select('b.*');
		$checking_accounts->set_select('f.name as fund_name');
		$checking_accounts->set_order('f.priority', 'ASC');
		$this->template_data->set('checking_accounts', $checking_accounts->populate());

		$this->load->view('checks/checks', $this->template_data->get_data());
	}

	public function add($acct_id) {
		$this->template_data->set('acct_id', $acct_id);

		$bank_account = new $this->Bank_accounts_model('b');
		$bank_account->setId($acct_id,true);
		$bank_account->set_join('funds f', 'f.id=b.fund_id');
		$bank_account->set_select('f.name as fund_name');
		$bank_account->set_select('b.*');
		$this->template_data->set('bank_account', $bank_account->get()); 

		if( $this->input->post() ) {
			$proceed = false;
			$start_num = 0;
			$end_num = 0;

			if( $this->input->post('start') ) {
				$proceed = true;
				$start_num = $this->input->post('start');
				$end_num = $this->input->post('start');
			}
			if( $this->input->post('end') ) {
				$end_num = $this->input->post('end');
				if( $this->input->post('end') < $this->input->post('start') ) {
					$proceed = false;
				}
			}
			
			if($proceed && $start_num && $end_num) {
				for($i=$start_num;$i<=$end_num;$i++) {
							$check_items = new $this->Checks_model();
							$check_items->setAcctId($acct_id,true);
							$check_items->setCheckNumber($i,true);
							if( $check_items->nonEmpty() === false) {
								$check_items->insert();
							}
				}
			}
			if( $this->input->get('next') ) {
				redirect( $this->input->get('next') );
			} else {
				redirect("checks/index/{$acct_id}");
			}
		}

		$this->load->view('checks/checks_add', $this->template_data->get_data());
	}

	public function write($db_id) {

		$disbursement = new $this->Reports_disbursements_model('rd');
		$disbursement->setId($db_id,true);
		$disbursement->set_select("rd.*");

		$bank_account = new $this->Bank_accounts_model('b');
		$bank_account->set_where('b.id=rd.bank_id');
		$bank_account->set_select('b.bank_name');

		$disbursement->set_select("(".$bank_account->get_compiled_select().")as bank_name");

			$account_number = new $this->Bank_accounts_model('b');
			$account_number->set_where('b.id=rd.bank_id');
			$account_number->set_select('b.account_number');

		$disbursement->set_select("(".$account_number->get_compiled_select().") as account_number");

			$active_check = new $this->Checks_model('c');
			$active_check->setDbId($db_id,true);
			$active_check->setVoided(0,true);
			$active_check->set_where('c.acct_id=rd.bank_id');
			$active_check->set_select("COUNT(c.id)");

		$disbursement->set_select("(".$active_check->get_compiled_select().") as active_checks");

			$adliq = new $this->Advances_liquidation_model('al');
			$adliq->setDbId($db_id,true);
			$adliq->set_select("SUM(al.amount)");

		$disbursement->set_select("(rd.amount - (".$adliq->get_compiled_select().")) as advances_balance");

		$db_data = $disbursement->get(); 
		
		if( $this->input->get('edit_item') ) {
		$current_item = new $this->Advances_liquidation_model('al');
		$current_item->setId($this->input->get('edit_item'),true);
			if( $this->input->post('action') == 'edit_advances' ) {
				$current_item->setNameId($this->input->post('payee_id'),false,true);
				$amount = str_replace(",", "", $this->input->post('amount'));
				$amount = str_replace(" ", "", $amount);
				$current_item->setAmount( $amount,false,true );
				$current_item->setNotes($this->input->post('notes'),false,true);
				$current_item->setGroupId($this->input->post('group_id'),false,true);
				$current_item->setClassId($this->input->post('class_id'),false,true);
				$current_item->update();
			}
		$this->template_data->set('current_item', $current_item->get());
		}

		$adliq = new $this->Advances_liquidation_model('al');
		$adliq->setDbId($db_id,true);
		if(( $db_data->advances_balance > 0 ) || ( is_null($db_data->advances_balance )) ) {
			if( $this->input->post('action') == 'add_advances' ) {
				$adliq->setNameId($this->input->post('payee_id'));
				$amount = str_replace(",", "", $this->input->post('amount'));
				$amount = str_replace(" ", "", $amount);
				$adliq->setAmount( $amount );
				$adliq->setNotes($this->input->post('notes'));
				$adliq->setGroupId($this->input->post('group_id'));
				$adliq->setClassId($this->input->post('class_id'));
				$adliq->insert();
			}
		}
		$adliq->set_select('(SELECT p.payee FROM payee p WHERE p.id=al.name_id) as payee');
		$adliq->set_select('al.*');
		$adliq->set_order('al.id', 'DESC');
		$this->template_data->set('advances', $adliq->populate());

		$db_data = $disbursement->get(); 
		$this->template_data->set('db_data', $db_data);

		$check_items = new $this->Checks_model('c');
		$check_items->setAcctId($db_data->bank_id,true,false);

		if( $this->input->post('action') == 'assign_check' ) {
			$check_number = $this->input->post('check_number');
			$check_items->setCheckNumber($check_number,true,false);
			$check_items->setPayeeId($this->input->post('payee_id'),false,true);
			$check_items->setDbId($db_id,false,true);
			$check_items->setCvId($this->input->post('voucher_number'),false,true);
			$check_items->setSignatory1($this->input->post('signatory1'),false,true);
			$check_items->setSignatory2($this->input->post('signatory2'),false,true);
			$check_items->setVoided(0,false,true);
			if( $check_items->nonEmpty() ) {
				$results = $check_items->get_results();
				$check_items->update();
				//redirect("checks/print_check/{$results->id}/{$db_id}");
				redirect(uri_string());
			}
		}

		$check_items->set_limit(0);
		$check_items->set_start(0);
		$check_items->set_select('c.*');
		$check_items->set_order('c.check_number', "ASC");
		$check_items->set_where('((c.db_id IS NULL)');
		$check_items->set_where_or('(c.db_id = 0))');
		$this->template_data->set('check_items', $check_items->populate());

		$payees = new $this->Payee_model('p');
		$payees->set_limit(0);
		$payees->set_order('p.payee', 'ASC');
		$payees->setTrash(0,true);
		$this->template_data->set('payees', $payees->populate());

		$checks_assigned = new $this->Checks_model('c');
		$checks_assigned->setAcctId($db_data->bank_id,true,false);
		$checks_assigned->setDbId($db_id,true,false);
		$checks_assigned->set_join('payee p', 'p.id=c.payee_id');
		$checks_assigned->set_select('c.*');
		$checks_assigned->set_select('p.payee as payee_name');
		$checks_assigned->set_order('c.voided', 'ASC');
		$checks_assigned->set_order('c.check_number', 'DESC');
		$this->template_data->set('checks_assigned', $checks_assigned->populate());

		$signatories1 = new $this->Signatory_model('p');
		$signatories1->setSignatory1(1,true);
		$signatories1->set_order('p.signatory', 'DESC');
		$signatories1->set_limit(0);
		$this->template_data->set('signatories1', $signatories1->populate());

		$signatories2 = new $this->Signatory_model('p');
		$signatories2->setSignatory2(1,true);
		$signatories2->set_order('p.signatory', 'DESC');
		$signatories2->set_limit(0);
		$this->template_data->set('signatories2', $signatories2->populate());

		$adv_groups = new $this->Advances_groups_model;
		$adv_groups->set_order('name', 'ASC');
		$adv_groups->set_limit(0);
		$this->template_data->set('adv_groups', $adv_groups->populate());

		$other_checks = new $this->Reports_disbursements_model('rd');
		$other_checks->setBankId($db_data->bank_id,true);
			$bank_account = new $this->Bank_accounts_model('b');
			$bank_account->set_where('b.id=rd.bank_id');
			$bank_account->set_select('b.bank_name');
		$other_checks->set_select("(".$bank_account->get_compiled_select().")as bank_name");

		$other_checks->set_select("rd.*");
		$other_checks->set_where('rd.id !=', $db_id);
		$other_checks->setReportDate($db_data->report_date, true);
		$other_checks->setType('check', true);
		$other_checks->set_limit(0);
		$this->template_data->set('other_checks', $other_checks->populate());

		$this->load->view('checks/checks_write', $this->template_data->get_data());
	}

	public function fix($check_id) {
		$current_check = new $this->Checks_model('c');
		$current_check->setId($check_id,true);
		$current_check->setDbId(0,false,true);
		$current_check->update();
		redirect($this->input->get('next'));
	}

	public function delete($check_id) {
		$current_check = new $this->Checks_model();
		$current_check->setId($check_id,true);
		$current_check->set_where("((db_id IS NULL) OR (db_id = 0))");
		$current_check->delete();
		redirect( site_url($this->input->get('next')) . "?filter=unassigned" );
	}

	public function edit($check_id, $db_id) {

		$disbursement = new $this->Reports_disbursements_model('rd');
		$disbursement->setId($db_id,true);
		$disbursement->set_select("rd.*");

		$bank_account = new $this->Bank_accounts_model('b');
		$bank_account->set_where('b.id=rd.bank_id');
		$bank_account->set_select('b.bank_name');

		$disbursement->set_select("(".$bank_account->get_compiled_select().")as bank_name");

			$account_number = new $this->Bank_accounts_model('b');
			$account_number->set_where('b.id=rd.bank_id');
			$account_number->set_select('b.account_number');

		$disbursement->set_select("(".$account_number->get_compiled_select().") as account_number");

			$active_check = new $this->Checks_model('c');
			$active_check->setDbId($db_id,true);
			$active_check->setVoided(0,true);
			$active_check->set_where('c.acct_id=rd.bank_id');
			$active_check->set_select("COUNT(c.id)");

		$disbursement->set_select("(".$active_check->get_compiled_select().") as active_checks");

		$db_data = $disbursement->get();
		$this->template_data->set('db_data', $db_data);

		$current_check = new $this->Checks_model('c');
		$current_check->setId($check_id,true);

		if( $this->input->post() ) {
			$current_check->setCvId($this->input->post('voucher_number'),false,true);
			$current_check->setPayeeId($this->input->post('payee_id'),false,true);
			$current_check->setSignatory1($this->input->post('signatory1'),false,true);
			$current_check->setSignatory2($this->input->post('signatory2'),false,true);
			$current_check->update();
			redirect( $this->input->get('next') );
		}


		$current_check->set_join('payee p', 'p.id=c.payee_id');
		$current_check->set_select('c.*');
		$current_check->set_select('p.payee as payee_name');
		$current_check->set_order('c.voided', 'ASC');
		$current_check->set_order('c.check_number', 'DESC');
		$this->template_data->set('current_check', $current_check->get());

		$payees = new $this->Payee_model('p');
		$payees->set_limit(0);
		$payees->set_order('p.payee', 'ASC');
		$payees->setTrash(0,true);
		$this->template_data->set('payees', $payees->populate());

		$signatories1 = new $this->Signatory_model('p');
		$signatories1->setSignatory1(1,true);
		$signatories1->set_order('p.signatory', 'DESC');
		$signatories1->set_limit(0);
		$this->template_data->set('signatories1', $signatories1->populate());

		$signatories2 = new $this->Signatory_model('p');
		$signatories2->setSignatory2(1,true);
		$signatories2->set_order('p.signatory', 'DESC');
		$signatories2->set_limit(0);
		$this->template_data->set('signatories2', $signatories2->populate());

		$this->load->view('checks/checks_edit', $this->template_data->get_data());
	}

	public function void($check_id, $db_id) {
		$check = new $this->Checks_model('c');
		$check->setId($check_id, true, false);
		$check->setDbId($db_id, true, false);
		$check->setVoided(1,false,true);
		$check->update();
		redirect("checks/write/{$db_id}");
	}

	public function activate($check_id, $db_id) {
		$check = new $this->Checks_model('c');
		$check->setId($check_id, true, false);
		$check->setDbId($db_id, true, false);
		$check->setVoided(0,false,true);
		$check->update();
		redirect("checks/write/{$db_id}");
	}

	public function remove_assignment($check_id, $db_id) {
		$check = new $this->Checks_model('c');
		$check->setId($check_id, true, false);
		$check->setDbId(0, false, true);
		$check->setCvId(0, false, true);
		$check->setPayeeId(0, false, true);
		$check->setVoided(0,false,true);
		$check->update();
		redirect("checks/write/{$db_id}");
	}

	public function unliquidated($adv_id) {
		$adliq = new $this->Advances_liquidation_model();
		$adliq->setId($adv_id,true,false);
		$adliq->setLiquidated(0,false,true);
		$adliq->update();
		redirect($this->input->get('next'), 'location');
	}

	public function remove_advances($adv_id) {
		$adliq = new $this->Advances_liquidation_model();
		$adliq->setId($adv_id,true);
		$adliq->delete();
		redirect($this->input->get('next'), 'location');
	}

	public function print_check($check_id, $db_id) {

		$this->load->helper('number2word');

		if( $this->input->get('template') ) {
			$this->session->set_userdata('print_check_template', $this->input->get('template'));
			redirect( uri_string() );
		}

		if( $this->input->get('hide_signatories') ) {
			$this->session->set_userdata('hidden_signatories', 1 );
			redirect( uri_string() );
		}

		if( $this->input->get('show_signatories') ) {
			$this->session->set_userdata('hidden_signatories', 0 );
			redirect( uri_string() );
		}

		$template = new $this->Check_templates_model('ct');
		if( $this->session->userdata('print_check_template') ) {
			$template->setId($this->session->userdata('print_check_template'),true);
		} else {
			$template->setDefault(1,true);
		}
		$this->template_data->set('template', $template->get());

		$check = new $this->Checks_model('c');
		$check->setId($check_id, true);
		$check->setDbId($db_id, true);
		$check->set_select('c.*');

		$check->set_join('reports_disbursements rd', 'rd.id=c.db_id');
		$check->set_select('rd.report_date as check_date');
		$check->set_select('rd.amount as amount');
		
		$check->set_join('payee p', 'p.id=c.payee_id');
		$check->set_select('p.payee as payee_name');
		$check->set_select('p.two_lines as two_lines');

		$check->set_select('(SELECT s.signatory FROM signatory s WHERE c.signatory1=s.id LIMIT 1) as signatory1');
		$check->set_select('(SELECT s.signatory FROM signatory s WHERE c.signatory2=s.id LIMIT 1) as signatory2');
		
		$check_data = $check->get();
		$this->template_data->set('check_data', $check_data);

		$templates = new $this->Check_templates_model('ct');
		$templates->set_limit(0);
		$this->template_data->set('templates', $templates->populate());

        $bank_accounts = new $this->Bank_accounts_model('ba');
        $bank_accounts->setFundCategory('disposable',true);
        $bank_accounts->set_limit(0);
        $bank_accounts->set_order('ba.fund_id', 'ASC');
        $bank_accounts->set_select('ba.*');
        $bank_accounts->set_select('(SELECT f.name FROM funds f WHERE f.id=ba.fund_id) as fund_name');
        $this->template_data->set('disposables', $bank_accounts->populate());

        $bank_id = ($this->input->get('bank_id')) ? $this->input->get('bank_id') : $check_data->acct_id;
		$check_items = new $this->Checks_model('c');
		$check_items->setAcctId($bank_id,true);
		$check_items->set_select('*');
		$check_items->set_select('c.id as check_id');
		$check_items->set_join('reports_disbursements rd','rd.id=c.db_id');
		$check_items->set_order('c.check_number', 'DESC');
		$check_items->set_where('((c.db_id IS NOT NULL) AND (c.db_id <> 0))');
		$check_items->set_where('rd.type="check"');
		$this->template_data->set('latest', $check_items->populate());

		$this->load->view('checks/checks_print', $this->template_data->get_data());
	}
}
