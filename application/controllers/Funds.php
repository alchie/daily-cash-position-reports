<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funds extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($category=false) {

		$this->template_data->set('method', 'index');

		$funds = new $this->Funds_model;
		$funds->set_select("*");
		$funds->set_select("(SELECT name FROM fund_groups WHERE id=funds.group) as group_name");
		$funds->set_select("(SELECT COUNT(*) FROM bank_accounts WHERE fund_id=funds.id) as bank_accounts_count");
		$funds->set_order('priority', 'ASC');
		$funds->set_limit(0);
		$funds_data = $funds->populate();
		
		$this->template_data->set('category', $category);

		foreach( $funds_data as $i=>$fd ) {
			$bank_accounts = new $this->Bank_accounts_model('b');
			$bank_accounts->setFundId($fd->id,true);
			$bank_accounts->setActive(1,true);
			if($category) {
				if( $category == 'checking_accounts') {
					$bank_accounts->setAcctType('checking',true);
				} else {
					$bank_accounts->setFundCategory($category,true);
				}
			}
			$bank_accounts->set_limit(0);
			$bank_accounts->set_order('b.priority', 'ASC');
			$bank_accounts->set_select("b.*");
			$bank_accounts->set_select("(SELECT g.name FROM fund_groups g WHERE g.id=b.class) as class_name");
			//$funds_data[$i]->bank_accounts_count = $bank_accounts->count_all_results();
			$funds_data[$i]->bank_accounts = $bank_accounts->populate();
		}
		$this->template_data->set('funds', $funds_data);
		$this->load->view('funds/index', $this->template_data->get_data());
	}

	public function trash($category=false) {

		$this->template_data->set('method', 'trash');

		$funds = new $this->Funds_model;
		$funds->set_select("*");
		$funds->set_select("(SELECT name FROM fund_groups WHERE id=funds.group) as group_name");
		$funds->set_select("(SELECT COUNT(*) FROM bank_accounts WHERE fund_id=funds.id) as bank_accounts_count");
		$funds->set_order('priority', 'ASC');
		$funds->set_limit(0);
		$funds_data = $funds->populate();
		
		$this->template_data->set('category', $category);

		foreach( $funds_data as $i=>$fd ) {
			$bank_accounts = new $this->Bank_accounts_model('b');
			$bank_accounts->setFundId($fd->id,true);
			$bank_accounts->setActive(0,true);
			if($category) {
				if( $category == 'checking_accounts') {
					$bank_accounts->setAcctType('checking',true);
				} else {
					$bank_accounts->setFundCategory($category,true);
				}
			}
			$bank_accounts->set_limit(0);
			$bank_accounts->set_order('b.priority', 'ASC');
			$bank_accounts->set_select("b.*");
			$bank_accounts->set_select("(SELECT g.name FROM fund_groups g WHERE g.id=b.class) as class_name");
			//$funds_data[$i]->bank_accounts_count = $bank_accounts->count_all_results();
			$funds_data[$i]->bank_accounts = $bank_accounts->populate();
		}
		$this->template_data->set('funds', $funds_data);
		$this->load->view('funds/trash', $this->template_data->get_data());
	}

	public function add() {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Fund Name', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$funds = new $this->Funds_model;
				$funds->setName($this->input->post('name')); 
				$funds->setPriority($this->input->post('priority')); 
				$funds->setGroup($this->input->post('group')); 
				$funds->insert();
				redirect('funds');
			}
		}

		$groups = new $this->Fund_groups_model;
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		$groups->setType('group',true); 
		$groups_data = $groups->populate();
		$this->template_data->set('groups', $groups_data);

		$this->load->view('funds/add', $this->template_data->get_data());
	}

	public function edit($id) {
		$funds = new $this->Funds_model;
		$funds->setId($id,true,false);
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Fund Name', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$funds->setName($this->input->post('name')); 
				$funds->setPriority($this->input->post('priority')); 
				$funds->setGroup($this->input->post('group')); 
				if( $funds->nonEmpty() ) {
					$funds->update();
				} 
				redirect('funds');
			}
		}
		$this->template_data->set('fund', $funds->get());

		$groups = new $this->Fund_groups_model;
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		$groups->setType('group',true); 
		$groups_data = $groups->populate();
		$this->template_data->set('groups', $groups_data);

		$this->load->view('funds/edit', $this->template_data->get_data());
	}

	public function delete($id) {
		$funds = new $this->Funds_model;
		$funds->setId($id,true,false);
		$funds->delete();
		redirect('funds');
	}

	public function groups() {
		$groups = new $this->Fund_groups_model;
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		$groups_data = $groups->populate();

		$this->template_data->set('groups', $groups_data);
		$this->load->view('fund_groups/index', $this->template_data->get_data());
	}

	public function groups_add() {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Fund Name', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$groups = new $this->Fund_groups_model;
				$groups->setName($this->input->post('name')); 
				$groups->setPriority($this->input->post('priority')); 
				$groups->setType($this->input->post('type')); 
				$groups->insert();
				redirect('funds/groups');
			}
		}
		$this->load->view('fund_groups/add', $this->template_data->get_data());
	}

	public function groups_edit($id) {
		$groups = new $this->Fund_groups_model;
		$groups->setId($id,true,false);
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Group Name', 'trim|required');
			if( $this->form_validation->run() != FALSE) {
				$groups->setName($this->input->post('name'),false,true); 
				$groups->setPriority($this->input->post('priority'),false,true); 
				$groups->setType($this->input->post('type'),false,true); 
				if( $groups->nonEmpty() ) {
					$groups->update();
				} 
				redirect('funds/groups');
			}
		}
		$this->template_data->set('group', $groups->get());
		$this->load->view('fund_groups/edit', $this->template_data->get_data());
	}

	public function groups_delete($id) {
		$groups = new $this->Fund_groups_model;
		$groups->setId($id,true);
		$groups->delete();
		redirect('funds/groups');
	}

}
