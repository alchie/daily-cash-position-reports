<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payee extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {

		$payees = new $this->Payee_model('p');
		$payees->set_order('p.payee', 'ASC');
		$payees->set_start($start);
		$payees->set_limit(10);
		$payees->setTrash(0,true);

		if( $this->input->get('q') ) {
			$payees->set_where('p.payee LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('payees', $payees->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => site_url("payee/index"),
			'total_rows' => $payees->count_all_results(),
			'per_page' => $payees->get_limit(),
		), "?q=" . $this->input->get('q') ));

		$this->load->view('payee/payee', $this->template_data->get_data());
	}

	public function trash($start=0) {

		$payees = new $this->Payee_model('p');
		$payees->set_order('p.payee', 'ASC');
		$payees->set_start($start);
		$payees->set_limit(10);
		$payees->setTrash(1,true);

		if( $this->input->get('q') ) {
			$payees->set_where('p.payee LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('payees', $payees->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => site_url("payee/trash"),
			'total_rows' => $payees->count_all_results(),
			'per_page' => $payees->get_limit(),
		), "?q=" . $this->input->get('q') ));

		$this->template_data->set('trash_page', true);
		$this->load->view('payee/payee', $this->template_data->get_data());
	}

	private function _add_single() {
		if( $this->input->post('name') ) {
			$payees = new $this->Payee_model('p');
			$payees->setPayee($this->input->post('name'),true);
			if($payees->nonEmpty()==FALSE) {
				$payees->insert();
				if($this->input->get('next')) {
					redirect( site_url( $this->input->get('next') ) . "?payee_id=" . $payees->get_inserted_id() );
				} else {
					redirect('payee');
				}
			}
		}
	}

	private function _add_multiple() {
		if( $this->input->post('name') ) {
			$names = explode("\n", $this->input->post('name') );
			if($names) {
				foreach($names as $name) {
					$name = trim($name);
					if( $name != '' ) {
						$payees = new $this->Payee_model('p');
						$payees->setPayee($name,true);
						if($payees->nonEmpty()==FALSE) {
							$payees->insert();
						}
					}
				}
			}
			redirect('payee');
		}
	}

	public function add() {
		if( $this->input->get('multiple') ) {
			$this->_add_multiple();
		} else {
			$this->_add_single();
		}
		$this->load->view('payee/payee_add', $this->template_data->get_data());
	}

	public function edit($id) {
		$payees = new $this->Payee_model('p');
		$payees->setId($id,true);

		if( $this->input->post('name') ) {
			$payees->setPayee($this->input->post('name'),false,true);
			$payees->update();
		}

		$this->template_data->set('payee', $payees->get());
		$this->load->view('payee/payee_edit', $this->template_data->get_data());
	}

	public function activate($id) {
		$payees = new $this->Payee_model('p');
		$payees->setId($id,true);
		$payees->setTrash(0,false,true);
		$payees->update();
		redirect( site_url( $this->input->get('next') ) );
	}

	public function deactivate($id) {
		$payees = new $this->Payee_model('p');
		$payees->setId($id,true);
		$payees->setTrash(1,false,true);
		$payees->update();
		redirect( site_url( $this->input->get('next') ) );
	}
}
