<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($m=false,$y=false) {
		
		if($m) {
			$this->reportsdb->setMonth($m);
		}

		if($y) {
			$this->reportsdb->setYear($y);
		}

		$this->template_data->set('reports', $this->reportsdb->init());

		$this->template_data->set('years', $this->reportsdb->getMaxMinYear());

		$this->load->view('reports/index', $this->template_data->get_data());
	}

	public function _view($m,$d,$y,$cat=false) {
		$current_date = date("Y-m-d", strtotime( $m."/".$d."/".$y ) );
		$this->template_data->set('current_date', $current_date);

		$filters = $this->input->get('filters');

		$this->reportsdb->setMonth($m);
		$this->reportsdb->setDay($d);
		$this->reportsdb->setYear($y);
		$this->template_data->set('reports', $this->reportsdb->init());

		$this->template_data->set('years', $this->reportsdb->getMaxMinYear());

		if( $cat == 'disposable') {

		$report = new $this->Reports_deposits_model;

			// RCBDI
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'rcbdi\' AND rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date<\''.$current_date.'\') as beg_cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'rcbdi\' AND rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date<\''.$current_date.'\') as beg_check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'rcbdi\' AND rd.bank_id=0 AND rd.report_date<\''.$current_date.'\') as beg_deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'rcbdi\' AND rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date<\''.$current_date.'\') as beg_cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'rcbdi\' AND rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date<\''.$current_date.'\') as beg_check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'rcbdi\' AND rd2.bank_id=0 AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
			
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'rcbdi\' AND rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date=\''.$current_date.'\') as cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'rcbdi\' AND  rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date=\''.$current_date.'\') as check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'rcbdi\' AND  rd.bank_id=0 AND rd.report_date=\''.$current_date.'\') as deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'rcbdi\' AND rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date=\''.$current_date.'\') as cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'rcbdi\' AND rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date=\''.$current_date.'\') as check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'rcbdi\' AND rd2.bank_id=0 AND rd2.report_date=\''.$current_date.'\') as disbursement');


			// ADSAI
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'adsai\' AND rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date<\''.$current_date.'\') as beg_cash_deposit_adsai');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'adsai\' AND rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date<\''.$current_date.'\') as beg_check_deposit_adsai');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'adsai\' AND rd.bank_id=0 AND rd.report_date<\''.$current_date.'\') as beg_deposit_adsai');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'adsai\' AND rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date<\''.$current_date.'\') as beg_cash_disbursement_adsai');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'adsai\' AND rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date<\''.$current_date.'\') as beg_check_disbursement_adsai');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'adsai\' AND rd2.bank_id=0 AND rd2.report_date<\''.$current_date.'\') as beg_disbursement_adsai');
			
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'adsai\' AND rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date=\''.$current_date.'\') as cash_deposit_adsai');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'adsai\' AND  rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date=\''.$current_date.'\') as check_deposit_adsai');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.company=\'adsai\' AND  rd.bank_id=0 AND rd.report_date=\''.$current_date.'\') as deposit_adsai');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'adsai\' AND rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date=\''.$current_date.'\') as cash_disbursement_adsai');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'adsai\' AND rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date=\''.$current_date.'\') as check_disbursement_adsai');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.company=\'adsai\' AND rd2.bank_id=0 AND rd2.report_date=\''.$current_date.'\') as disbursement_adsai');

		$dreport = $report->get();
		$this->template_data->set('current_report', $dreport);

		}

		$funds = new $this->Funds_model;
		$funds->set_order('priority', 'ASC');
		$funds->set_limit(0);

		if( ($filters) && isset($filters['group']) ) {
			$funds->set_where_in('group', $filters['group']);
		}

		$funds_data = $funds->populate();

		foreach( $funds_data as $i=>$fd ) {
			$bank_accounts = new $this->Bank_accounts_model('ba');
			$bank_accounts->setFundId($fd->id,true);
			if( $cat ) {
				$bank_accounts->setFundCategory($cat,true);
			}
			$bank_accounts->set_limit(0);
			$bank_accounts->set_order('ba.priority', 'ASC');
			$bank_accounts->set_select('ba.*');
			// beginning
			$bank_accounts->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=ba.id AND rd.report_date<\''.$current_date.'\') as beg_deposit');
			$bank_accounts->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=ba.id AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
			//$bank_accounts->set_select('(SELECT(IF(beg_deposit,beg_deposit,0) - IF(beg_disbursement,beg_disbursement,0))) as beg_balance');
			
			// current balances
			$bank_accounts->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=ba.id AND rd.report_date=\''.$current_date.'\') as deposit');
			$bank_accounts->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=ba.id AND rd2.report_date=\''.$current_date.'\') as disbursement');

			if( ($filters) && isset($filters['class']) ) {
				$bank_accounts->set_where_in('class', $filters['class']);
			}

			if( ($filters) && isset($filters['category']) ) {
				$bank_accounts->set_where_in('fund_category', $filters['category']);
			}

			$bank_accounts->set_select("(SELECT g.name FROM fund_groups g WHERE g.id=ba.class) as class_name");
			$funds_data[$i]->bank_accounts_count = $bank_accounts->count_all_results();
			$funds_data[$i]->bank_accounts = $bank_accounts->populate();
		}

		$this->template_data->set('funds', $funds_data);

		$this->template_data->set('current_month', $m);
		$this->template_data->set('current_day', $d);
		$this->template_data->set('current_year', $y);

		$this->template_data->set('cat', $cat);

	}

	public function _summary($m,$d,$y) {
		
	}

	public function select($m,$d,$y) {
		$this->session->set_userdata('report_date', "{$y}-{$m}-{$d}");
		$f = ($this->input->get('f')) ? $this->input->get('f') : 'view';
		redirect("reports/{$f}/{$m}/{$d}/{$y}");
	}

	public function view($m,$d,$y) {
		$this->_view($m,$d,$y,'disposable');
		$this->load->view('reports/view', $this->template_data->get_data());
	}

	public function disposable($m,$d,$y) {
		$this->view($m,$d,$y);
	}

	public function dollar($m,$d,$y) {
		$this->_view($m,$d,$y,'dollar');
		$this->load->view('reports/dollar', $this->template_data->get_data());
	}

	public function time_deposits($m,$d,$y) {
		$this->_view($m,$d,$y,'time_deposit');
		$this->template_data->set('cat', 'time_deposits');
		$this->load->view('reports/time_deposits', $this->template_data->get_data());
	}

	public function long_term($m,$d,$y) {
		$this->_view($m,$d,$y,'long_term');
		$this->load->view('reports/long_term', $this->template_data->get_data());
	}

	public function all_accounts($m,$d,$y) {
		$this->_view($m,$d,$y,false);
		$this->template_data->set('cat', 'all_accounts');
		$this->load->view('reports/all_accounts', $this->template_data->get_data());
	}

	public function summary($m,$d,$y) {
		$this->template_data->set('cat', 'summary');
		$current_date = date("Y-m-d", strtotime( $m."/".$d."/".$y ) );
		$this->template_data->set('current_date', $current_date);

		$this->reportsdb->setMonth($m);
		$this->reportsdb->setDay($d);
		$this->reportsdb->setYear($y);
		$this->template_data->set('reports', $this->reportsdb->init());

		$this->template_data->set('years', $this->reportsdb->getMaxMinYear());

		$funds = new $this->Funds_model('f');
		$funds->set_order('priority', 'ASC');
		$funds->set_select('f.*');
		
		// BEGINNING BALANCES
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date<\''.$current_date.'\') as beg_deposit');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
		//$funds->set_select('(SELECT(IF(beg_deposit,beg_deposit,0) - IF(beg_disbursement,beg_disbursement,0))) as beg_balance');

		// CURRENT BALANCES
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\') as deposit');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date=\''.$current_date.'\') as disbursement');

		$funds->set_limit(0);
		$funds_data = $funds->populate();
		//print_r( $funds_data );

		$this->template_data->set('funds', $funds_data);

		$this->template_data->set('current_month', $m);
		$this->template_data->set('current_day', $d);
		$this->template_data->set('current_year', $y);


			$report = new $this->Reports_deposits_model;

			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date<\''.$current_date.'\') as beg_cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date<\''.$current_date.'\') as beg_check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date<\''.$current_date.'\') as beg_deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date<\''.$current_date.'\') as beg_cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date<\''.$current_date.'\') as beg_check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
			
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date=\''.$current_date.'\') as cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date=\''.$current_date.'\') as check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date=\''.$current_date.'\') as deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date=\''.$current_date.'\') as cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date=\''.$current_date.'\') as check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date=\''.$current_date.'\') as disbursement');

			$dreport = $report->get();
			$this->template_data->set('current_report', $dreport);

		$this->load->view('reports/summary', $this->template_data->get_data());
	}

	public function print_cashier($m,$d,$y) {
		$current_date = date("Y-m-d", strtotime( $m."/".$d."/".$y ) );
		$this->template_data->set('current_date', $current_date);

		$this->reportsdb->setMonth($m);
		$this->reportsdb->setDay($d);
		$this->reportsdb->setYear($y);
		$this->template_data->set('reports', $this->reportsdb->init());

		$report = new $this->Reports_deposits_model;
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date<\''.$current_date.'\') as beg_cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date<\''.$current_date.'\') as beg_check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date<\''.$current_date.'\') as beg_deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date<\''.$current_date.'\') as beg_cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date<\''.$current_date.'\') as beg_check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
			
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date=\''.$current_date.'\') as cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date=\''.$current_date.'\') as check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date=\''.$current_date.'\') as deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date=\''.$current_date.'\') as cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date=\''.$current_date.'\') as check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date=\''.$current_date.'\') as disbursement');

		$dreport = $report->get();
		$this->template_data->set('current_report', $dreport);

		$funds = new $this->Funds_model('f');
		$funds->set_order('priority', 'ASC');
		$funds->set_select('f.*');
		
		// count bank accounts
		$funds->set_where('(SELECT COUNT(*) FROM bank_accounts ba WHERE ba.fund_id=f.id AND ba.fund_category=\'disposable\') > 0');

		// BEGINNING BALANCES
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date<\''.$current_date.'\' AND ba.fund_category=\'disposable\') as beg_deposit');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date<\''.$current_date.'\' AND ba.fund_category=\'disposable\') as beg_disbursement');
		//$funds->set_select('(SELECT(IF(beg_deposit,beg_deposit,0) - IF(beg_disbursement,beg_disbursement,0))) as beg_balance');

		// CURRENT BALANCES
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\' AND rd.type=\'cash\' AND ba.fund_category=\'disposable\') as cash_deposit');
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\' AND rd.type=\'check\' AND ba.fund_category=\'disposable\') as check_deposit');
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\' AND rd.type=\'fund_transfer\' AND ba.fund_category=\'disposable\') as fund_transfers');
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\' AND rd.type=\'adj\' AND ba.fund_category=\'disposable\') as adjustments');
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\' AND ba.fund_category=\'disposable\') as deposit');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date=\''.$current_date.'\' AND ba.fund_category=\'disposable\') as disbursement');

		$funds->set_limit(0);
		$funds_data = $funds->populate();

		$this->template_data->set('funds', $funds_data);

		$this->template_data->set('current_month', $m);
		$this->template_data->set('current_day', $d);
		$this->template_data->set('current_year', $y);

		$this->load->view('reports/print_cashier', $this->template_data->get_data());
	}

	public function print_summary($m,$d,$y) {
		$current_date = date("Y-m-d", strtotime( $m."/".$d."/".$y ) );
		$this->template_data->set('current_date', $current_date);

		$filters = $this->input->get('filters');

		$this->reportsdb->setMonth($m);
		$this->reportsdb->setDay($d);
		$this->reportsdb->setYear($y);
		$this->template_data->set('reports', $this->reportsdb->init());

		$funds = new $this->Funds_model('f');
		$funds->set_order('priority', 'ASC');
		$funds->set_select('f.*');

		if( ($filters) && isset($filters['group']) ) {
			$funds->set_where_in('group', $filters['group']);
		}

		// BEGINNING BALANCES
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date<\''.$current_date.'\' AND ba.fund_category=\'disposable\') as beg_deposit');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date<\''.$current_date.'\' AND ba.fund_category=\'disposable\') as beg_disbursement');
		//$funds->set_select('(SELECT(IF(beg_deposit,beg_deposit,0) - IF(beg_disbursement,beg_disbursement,0))) as beg_balance');

		// CURRENT BALANCES
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date=\''.$current_date.'\' AND ba.fund_category=\'disposable\') as deposit');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date=\''.$current_date.'\' AND ba.fund_category=\'disposable\') as disbursement');

		// time deposits 
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date<=\''.$current_date.'\' AND ba.fund_category=\'time_deposit\') as time_deposit_debits');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date<=\''.$current_date.'\' AND ba.fund_category=\'time_deposit\') as time_deposit_credits');
		//$funds->set_select('(SELECT(IF(time_deposit_debits,time_deposit_debits,0) - IF(time_deposit_credits,time_deposit_credits,0))) as total_time_deposit');

		// dollar
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date<=\''.$current_date.'\' AND ba.fund_category=\'dollar\') as dollar_debits');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date<=\''.$current_date.'\' AND ba.fund_category=\'dollar\') as dollar_credits');
		//$funds->set_select('(SELECT(IF(dollar_debits,dollar_debits,0) - IF(dollar_credits,dollar_credits,0))) as total_dollar');

		// dollar
		$funds->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd JOIN bank_accounts ba ON ba.id = rd.bank_id WHERE ba.fund_id=f.id AND rd.report_date<=\''.$current_date.'\' AND ba.fund_category=\'long_term\') as long_term_debits');
		$funds->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 JOIN bank_accounts ba ON ba.id = rd2.bank_id WHERE ba.fund_id=f.id AND rd2.report_date<=\''.$current_date.'\' AND ba.fund_category=\'long_term\') as long_term_credits');
		//$funds->set_select('(SELECT(IF(long_term_debits,long_term_debits,0) - IF(long_term_credits,long_term_credits,0))) as total_long_term');

		$funds->set_limit(0);
		$funds_data = $funds->populate();
		//print_r( $funds_data );

		$this->template_data->set('funds', $funds_data);

		$this->template_data->set('current_month', $m);
		$this->template_data->set('current_day', $d);
		$this->template_data->set('current_year', $y);


			$report = new $this->Reports_deposits_model;

			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date<\''.$current_date.'\') as beg_cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date<\''.$current_date.'\') as beg_check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date<\''.$current_date.'\') as beg_deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date<\''.$current_date.'\') as beg_cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date<\''.$current_date.'\') as beg_check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
			
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date=\''.$current_date.'\') as cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date=\''.$current_date.'\') as check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date=\''.$current_date.'\') as deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date=\''.$current_date.'\') as cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date=\''.$current_date.'\') as check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date=\''.$current_date.'\') as disbursement');

			$dreport = $report->get();
			$this->template_data->set('current_report', $dreport);

		$groups = new $this->Fund_groups_model;
		$groups->setType('group', true);
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		$groups_data = $groups->populate();
		$this->template_data->set('groups', $groups_data);

		$this->load->view('reports/print_summary', $this->template_data->get_data());
	}

	public function print_detailed($m,$d,$y) {

		$current_date = date("Y-m-d", strtotime( $m."/".$d."/".$y ) );
		$this->template_data->set('current_date', $current_date);

		$this->reportsdb->setMonth($m);
		$this->reportsdb->setDay($d);
		$this->reportsdb->setYear($y);
		$this->template_data->set('reports', $this->reportsdb->init());

		$this->template_data->set('years', $this->reportsdb->getMaxMinYear());
		
		$report = new $this->Reports_deposits_model;

			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date<\''.$current_date.'\') as beg_cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date<\''.$current_date.'\') as beg_check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date<\''.$current_date.'\') as beg_deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date<\''.$current_date.'\') as beg_cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date<\''.$current_date.'\') as beg_check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date<\''.$current_date.'\') as beg_disbursement');
			
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'cash\' AND rd.report_date=\''.$current_date.'\') as cash_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.type=\'check\' AND rd.report_date=\''.$current_date.'\') as check_deposit');
			$report->set_select('(SELECT SUM(rd.amount) FROM reports_deposits rd WHERE rd.bank_id=0 AND rd.report_date=\''.$current_date.'\') as deposit');

			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'cash\' AND rd2.report_date=\''.$current_date.'\') as cash_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.type=\'check\' AND rd2.report_date=\''.$current_date.'\') as check_disbursement');
			$report->set_select('(SELECT SUM(rd2.amount) FROM reports_disbursements rd2 WHERE rd2.bank_id=0 AND rd2.report_date=\''.$current_date.'\') as disbursement');

		$dreport = $report->get();
		$this->template_data->set('current_report', $dreport);

		$this->_view($m,$d,$y,false);
		$this->load->view('reports/print_detailed', $this->template_data->get_data());
	}

	public function add($m,$d,$y) {
		if( $this->input->post() ) {
				$report = new $this->Reports_model;
				$report->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ), true );
				if( $this->input->post('beginning') ) {
					$report->setBeginning( date("Y-m-d", strtotime( $this->input->post('beginning') ) ) );
				}				
				if( $report->nonEmpty() === false ) {
					$report->insert();
					redirect("reports/view/{$m}/{$d}/{$y}");
				}
		}
		$this->load->view('reports/add', $this->template_data->get_data());
	}

	public function edit($m,$d,$y) {
		$report = new $this->Reports_model;
		$report->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ), true );
		if( $this->input->post() ) {
				if( $this->input->post('beginning') ) {
					$report->setBeginning( date("Y-m-d", strtotime( $this->input->post('beginning') ) ) );
				}				
				if( $report->nonEmpty() ) {
					$report->set_exclude(array('report_date', 'id'));
					$report->update();
					redirect("reports/view/{$m}/{$d}/{$y}");
				}
		}
		$this->template_data->set('current_report', $report->get());
		$this->load->view('reports/edit', $this->template_data->get_data());
	}

	public function add_deposit($m,$d,$y) {
/*
		if( $this->input->post('type') == 'fund_transfer' ) {
			if( $this->input->post('source_id') == '' ) {
				redirect( $redirect_url . "?error=Fund Transfer Error! No Source Fund" );
			}
			if( $this->input->post('source_id') == $this->input->post('bankid') ) {
				redirect( $redirect_url . "?error=Fund Transfer Error! Source Fund should not be equal to Destination Fund!" );
			}
		}
*/
		//$redirect_url = ($this->agent->referrer()) ? $this->agent->referrer() : site_url("reports/view/{$m}/{$d}/{$y}");
		$redirect_url = site_url("reports/view/{$m}/{$d}/{$y}");

		if($this->input->post()) {
			
			$deposit = new $this->Reports_deposits_model;
			$deposit->setBankId($this->input->post('bankid'));
			$deposit->setAmount( str_replace(",", '', $this->input->post('amount')));
			$deposit->setType($this->input->post('type'));
			$deposit->setCompany($this->input->post('company'));
			$deposit->setDescription($this->input->post('description'));
			$deposit->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ) );
			
			$dbm_id = false;
			if( ($this->input->post('bankid') != '0') && ($this->input->post('type') != 'adj' ) ) {
				$source = 0; //($this->input->post('source_id')) ? $this->input->post('source_id') : 0;
				$disbursement = new $this->Reports_disbursements_model;
				$disbursement->setBankId($source);
				$disbursement->setAmount( str_replace(",", '', $this->input->post('amount')));
				$disbursement->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ) );
				$disbursement->setType( $this->input->post('type') );
				$disbursement->setCompany($this->input->post('company'));
				$disbursement->setDescription( $this->input->post('description') );
				if( $disbursement->insert() ) {
					$dbm_id = $disbursement->getId();
					$deposit->setDbmId($dbm_id);
				}
			}

			if( $deposit->insert() ) {
				if( $dbm_id ) {
					$disbursement = new $this->Reports_disbursements_model;
					$disbursement->setId($dbm_id,true,false);
					if( $disbursement->nonEmpty() ) {
						$disbursement->setDpId($deposit->getId(),false,true);
						$disbursement->update();
					}
				}
			}

		}

		if( $this->input->get('next') ) {
			redirect( $this->input->get('next') );
		} else {
			redirect( $redirect_url );
		}
		
	}

	public function delete_deposit($m,$d,$y) {
		if($this->input->get('id')) {
			$deposit = new $this->Reports_deposits_model;
			$deposit->setId($this->input->get('id'),true);
			$deposit->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ), true );
			if( $deposit->nonEmpty() ) {
				
				$results = $deposit->getResults();
			
				$disbursement = new $this->Reports_disbursements_model;
				$disbursement->setId($results->dbm_id,true);
				$disbursement->delete();
				$deposit->delete();

			}
		}
		if( $this->input->get('next') ) {
			redirect( $this->input->get('next') );
		} else {
			redirect( "reports/view/{$m}/{$d}/{$y}" );
		}
	}

	public function add_disbursement($m,$d,$y) {
		if($this->input->post()) {

			if( $this->input->post('type') == 'fund_transfer' ) {
				if( $this->input->post('dest_id') == '' ) {
					redirect( $redirect_url . "?error=Fund Transfer Error! No Source Fund" );
				}
				if( $this->input->post('dest_id') == $this->input->post('bankid') ) {
					redirect( $redirect_url . "?error=Fund Transfer Error! Source Fund should not be equal to Destination Fund!" );
				}
			}

			$disbursement = new $this->Reports_disbursements_model;
			$disbursement->setBankId($this->input->post('bankid'));
			$disbursement->setAmount( str_replace(",", '', $this->input->post('amount')));
			$disbursement->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ) );
			$disbursement->setDescription($this->input->post('description'));
			$disbursement->setType($this->input->post('type'));
			$disbursement->setCompany($this->input->post('company'));

			$dp_id = false;
			if( ($this->input->post('bankid') != '0') && ($this->input->post('type') != 'adj' ) ) {
				$source = ($this->input->post('dest_id')) ? $this->input->post('dest_id') : 0;
				$deposit = new $this->Reports_deposits_model;
				$deposit->setBankId($source);
				$deposit->setAmount( str_replace(",", '', $this->input->post('amount')));
				$deposit->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ) );
				$deposit->setType( $this->input->post('type') );
				$deposit->setCompany($this->input->post('company'));
				$deposit->setDescription( $this->input->post('description') );
				if( $deposit->insert() ) {
					$dp_id = $deposit->get_inserted_id();
					$disbursement->setDpId($dp_id);
				}
			}
			if( $disbursement->insert() ) {
				if( $dp_id ) {
					$deposit = new $this->Reports_deposits_model;
					$deposit->setId($dp_id,true,false);
					if( $deposit->nonEmpty() ) {
						$deposit->setDbmId($disbursement->get_inserted_id(),false,true);
						$deposit->update();
					}
				}
			}

			if(($this->input->post('type') == 'check')||($this->input->post('type') == 'fund_transfer')) {
				if( $this->input->post('write_check') ) {
					redirect( "checks/write/" . $disbursement->get_inserted_id() );
				}
			}
		}

		if( $this->input->get('next') ) {
			redirect( $this->input->get('next') );
		} else {
			redirect( "reports/view/{$m}/{$d}/{$y}" );
		}
	}

	public function delete_disbursement($m,$d,$y) {
		if($this->input->get('id')) {
			$disbursement = new $this->Reports_disbursements_model;
			$disbursement->setId($this->input->get('id'),true);
			$disbursement->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ), true );
			
			if( $disbursement->nonEmpty() ) {
				$results = $disbursement->getResults();
				$disbursement->delete();

				$deposit = new $this->Reports_deposits_model;
				$deposit->setId($results->dp_id,true);
				$deposit->delete();
			}
		}
		if( $this->input->get('next') ) {
			redirect( $this->input->get('next') );
		} else {
			redirect( "reports/view/{$m}/{$d}/{$y}" );
		}
	}

	public function existing($m,$d,$y) {
		if($this->input->get()) {
			$bankid = ($this->input->get('bankid')) ? $this->input->get('bankid') : 0;
			switch($this->input->get('what')) {
				case 'deposit':
					$deposit = new $this->Reports_deposits_model;
					$deposit->setBankId($bankid,true);
					$deposit->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ), true );
					$deposit->set_select("*");
					$deposit->set_limit(0);
					$url = site_url("bank_accounts/edit_item/deposit");
					$deposit->set_select("CONCAT('{$url}/',reports_deposits.id,'?back=reports/view/{$m}/{$d}/{$y}') as edit_uri");
					echo json_encode($deposit->populate());
				break;
				case 'disbursement':
					$disbursement = new $this->Reports_disbursements_model;
					$disbursement->setBankId($bankid,true);
					$disbursement->setReportDate( date("Y-m-d", strtotime( $m."/".$d."/".$y ) ), true );
					$disbursement->set_select("*");
					$disbursement->set_limit(0);
					$url = site_url("bank_accounts/edit_item/disbursement");
					$disbursement->set_select("CONCAT('{$url}/',reports_disbursements.id,'?back=reports/view/{$m}/{$d}/{$y}') as edit_uri");
					$write_check = site_url("checks/write/");
					$disbursement->set_select("CONCAT('{$write_check}/',reports_disbursements.id) as write_check");
					$disbursement->set_select("(SELECT CONCAT(ba.bank_name, ' (', ba.account_number, ')' ) FROM  bank_accounts ba WHERE ba.id=(SELECT rd2.bank_id FROM reports_deposits rd2 WHERE rd2.id=reports_disbursements.dp_id)) as deposited_to");
					echo json_encode($disbursement->populate());
				break;
			}
		}
	}

	public function filters($m,$d,$y) {
		
		$this->template_data->set('month', $m);
		$this->template_data->set('day', $d);
		$this->template_data->set('year', $y);

		$groups = new $this->Fund_groups_model;
		$groups->set_order('priority', 'ASC');
		$groups->set_limit(0);
		//$groups->setType('class',true); 
		$groups_data = $groups->populate();
		$this->template_data->set('groups', $groups_data);

		$this->load->view('reports/filters', $this->template_data->get_data());
	}

}
