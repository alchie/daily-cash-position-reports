<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signatory extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {

		$signatories = new $this->Signatory_model('p');
		$signatories->set_order('p.signatory', 'ASC');
		$signatories->set_start($start);
		$this->template_data->set('signatories', $signatories->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => site_url("signatory/index"),
			'total_rows' => $signatories->count_all_results(),
			'per_page' => $signatories->get_limit(),
		)));

		$this->load->view('signatory/signatory', $this->template_data->get_data());
	}

	public function add() {

		if( $this->input->post('name') ) {
			$signatories = new $this->Signatory_model('p');
			$signatories->setSignatory($this->input->post('name'),true);
			if($signatories->nonEmpty()==FALSE) {
				$signatories->insert();
				if($this->input->get('next')) {
					redirect( site_url( $this->input->get('next') ) . "?signatory_id=" . $signatories->get_inserted_id() );
				} else {
					redirect('signatory');
				}
			}
		}

		$this->load->view('signatory/signatory_add', $this->template_data->get_data());
	}

	public function edit($id) {
		$signatories = new $this->Signatory_model('p');
		$signatories->setId($id,true);

		if( $this->input->post('name') ) {
			$signatories->setSignatory($this->input->post('name'),false,true);
			$signatories->setPriority($this->input->post('priority'),false,true);
			$signatories->setSignatory1((($this->input->post('signatory1'))?1:0),false,true);
			$signatories->setSignatory2((($this->input->post('signatory2'))?1:0),false,true);
			$signatories->update();
		}

		$this->template_data->set('signatory', $signatories->get());
		$this->load->view('signatory/signatory_edit', $this->template_data->get_data());
	}

}
