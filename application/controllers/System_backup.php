<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System_backup extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Backup');
		$this->template_data->set('current_uri', 'system_backup');
	}

	public function index() {

		$dir    = 'backups';
		$files = array_diff(scandir($dir), array('..', '.', '.htaccess', 'index.html'));
		arsort($files);
		$this->template_data->set('backup_files', $files);

		$this->load->view('system/backup', $this->template_data->get_data());

		global $autoload;
		print_r($autoload);
	}

	public function download($file)
	{
		$file_dir = "backups/" . $file;
		if (file_exists($file_dir)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file_dir).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file_dir));
		    readfile($file_dir);
		    exit;
		}
		redirect("system_backup");
	}

	public function delete($file)
	{
		$file_dir = "backups/" . $file;
		if (file_exists($file_dir)) {
			unlink($file_dir);
		}
		redirect("system_backup");
	}

	private function _models() {
		$models = array();
		$models['account_sessions'] = 'Account_sessions_model';
		$models['bank_accounts'] = 'Bank_accounts_model';
		$models['check_templates'] = 'Check_templates_model';
		$models['checks'] = 'Checks_model';
		$models['fund_groups'] = 'Fund_groups_model';
		$models['funds'] = 'Funds_model';
		$models['payee'] = 'Payee_model';
		$models['reports'] = 'Reports_model';
		$models['reports_deposits'] = 'Reports_deposits_model';
		$models['reports_disbursements'] = 'Reports_disbursements_model';
		$models['signatory'] = 'Signatory_model';
		$models['user_accounts'] = 'User_accounts_model';
		return $models;
	}

	public function verify() {

		$models = $this->_models();
		$models_obj = array();
		$missing_tables = array();
		foreach($models as $i=>$model) {
			
			$obj = new $this->$model;
			if( !in_array($obj->get_table_name(), $this->db->list_tables())) {
				$missing_tables[$obj->get_table_name()] = $obj->get_table_fields();
				continue;
			}

			$table_columns = $this->db->query('SHOW COLUMNS FROM '. $obj->get_table_name());
			$models_obj[$i] = (object) array(
				'model_name' => $model,
				'table_name'=> $obj->get_table_name(),
				'fields' => $obj->get_table_fields(),
				'table_options' => $obj->get_table_options(),
				'table_columns' => ((isset($table_columns)) && ($table_columns)) ? $table_columns->result() : false,
			);
		}

		$this->template_data->set('models', $models_obj);
		$this->template_data->set('missing_tables', $missing_tables);

		$this->load->view('system/verify', $this->template_data->get_data());

	}

	public function remove_field($table_name, $field_name) {
		$this->db->query("ALTER TABLE `{$table_name}` DROP `{$field_name}`;");
		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function fix_key($table_name, $field_name, $field_type) {
		$field_type = urldecode( $field_type );
		if( $field_type == 'PRI') {
			$this->db->query("ALTER TABLE `{$table_name}` ADD PRIMARY KEY(`{$field_name}`);");
		}
		elseif( $field_type == 'MUL') {
			$this->db->query("ALTER TABLE `{$table_name}` ADD INDEX(`$field_name`);");
		}
		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function fix_type($table_name, $field_name, $field_type) {
		$field_type = urldecode( $field_type );
		$this->db->query("ALTER TABLE `{$table_name}` CHANGE `{$field_name}` `{$field_name}` {$field_type};");
		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function fix_extra($table_name, $field_name, $field_type, $value) {
		$field_type = urldecode( $field_type );
		if( $value == 'auto_increment') {
			$this->db->query("ALTER TABLE `{$table_name}` CHANGE `{$field_name}` `{$field_name}` {$field_type} NOT NULL AUTO_INCREMENT;");
		}
		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function fix_default($table_name, $field_name, $field_type, $value) {
		$field_type = urldecode( $field_type );
		$this->db->query("ALTER TABLE `{$table_name}` CHANGE `{$field_name}` `{$field_name}` {$field_type} NOT NULL DEFAULT '{$value}';");
		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function fix_null($table_name, $field_name, $field_type, $value) {
		$new_value = 'NOT NULL';
		if( $value == 'YES' ) {
			$new_value = 'NULL';
		}
		$field_type = urldecode( $field_type );
		$this->db->query("ALTER TABLE `{$table_name}` CHANGE `{$field_name}` `{$field_name}` {$field_type} {$new_value};");

		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function add_column($table_name, $field_name) {
		$models = $this->_models();
		if(isset($models[$table_name])) {
			$model_class = $models[$table_name];
			$table = new $this->$model_class;
			$table->add_table_column($field_name);
		}

		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

	public function add_table($table_name) {
		$models = $this->_models();
		
		if( isset( $models[$table_name] ) ) {
			$this->db->query("CREATE TABLE IF NOT EXISTS `{$table_name}` (`temporary_column_remove_this` int(1) NULL);");
			$obj = new $this->$models[$table_name];
			foreach( $obj->get_table_fields() as $field ) {
				$obj->add_table_column($field);
			}
			$this->db->query("ALTER TABLE `{$table_name}` DROP `temporary_column_remove_this`;");
		}

		redirect( site_url("system_backup/verify") . "?table=" . $table_name );
	}

}
