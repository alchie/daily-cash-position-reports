<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($m=false,$y=false) {
		
		if($m) {
			$this->reportsdb->setMonth($m);
		}

		if($y) {
			$this->reportsdb->setYear($y);
		}

		$this->template_data->set('reports', $this->reportsdb->init());

		$this->template_data->set('years', $this->reportsdb->getMaxMinYear());
		
		$this->load->view('reports/index', $this->template_data->get_data());
	}

}
