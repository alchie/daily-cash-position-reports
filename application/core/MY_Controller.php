<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                
                if( ! $this->session->loggedIn || ! isset($this->session->loggedIn) ) {
                	$this->session->sess_destroy();
                	redirect('account/login');
                }

                $this->template_data->set('session_auth', $this->session->session_auth);

                $bank_accounts = new $this->Bank_accounts_model('ba');
                $bank_accounts->setFundCategory('disposable',true);
                $bank_accounts->set_limit(0);
                $bank_accounts->set_order('ba.fund_id', 'ASC');
                $bank_accounts->set_select('ba.*');
                $bank_accounts->set_select('(SELECT f.name FROM funds f WHERE f.id=ba.fund_id) as fund_name');
                $this->template_data->set('disposables', $bank_accounts->populate());
        }

        public function _isAuth($module, $action='view', $uri=false) {
        	
                $auth = false;

                if( isset( $this->session->session_auth ) ) {
                	if( isset($this->session->session_auth->$module) ) {
                		if( isset($this->session->session_auth->$module->$action) ) {
                			$auth = (bool) $this->session->session_auth->$module->$action;
                		}
                	} 
                }

        	if( $auth ) {
                        
                        return $auth;

                } else {
                        
                        if( $uri == '') {

                                $uri = 'welcome';

                        }
        		redirect( $uri, 'refresh' );
        	}

        	

        }
}