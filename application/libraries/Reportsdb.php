<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Reportsdb {
    
    var $month;
    var $day;
    var $year;
    var $monthDays;
    var $monthName;
    var $currentDate;
    var $reports = array();

    public function __construct()
    {
       $this->month = date('m');
       $this->day = date('d');
       $this->year = date('Y');
       $this->monthDays = date('t');
       $this->monthName = date('F');
       $this->currentDate = date('Y-m-d');
       $this->next_month = date('Y-m-d', strtotime(date('Y-m-d')." +1 month"));
       $this->previous_month = date('Y-m-d', strtotime(date('Y-m-d')." -1 month"));
    }
    
    public function setMonth($m) {
        $this->month = $m;
        //$this->monthDays = cal_days_in_month(0, $this->month, $this->year);
        $this->_setDateValues();
    }

    public function setDay($d) {
        $this->day = $d;
        $this->_setDateValues();
    }

    public function setYear($y) {
        $this->year = $y;
        $this->_setDateValues();
    }

    public function setDate($d) {
        $this->month = date('m', strtotime($d));
        $this->day = date('d', strtotime($d));
        $this->year = date('Y', strtotime($d));
        $this->_setDateValues();
    }

    private function _setDateValues() {
        $this->monthDays = date('t', strtotime($this->month."/".$this->day."/".$this->year));
        $this->monthName = date('F', strtotime($this->month."/".$this->day."/".$this->year));
        $this->currentDate = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year));
        
        $this->next_month = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." +1 month"));
        $this->previous_month = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." -1 month"));

        $this->next_day = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." +1 day"));
        $this->previous_day = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." -1 day"));
    }

    public function getReports() {
        $ci = get_instance();
        $ci->load->model('Reports_deposits_model');
        $ci->load->model('Reports_disbursements_model');
        for($i=1;$i<=$this->monthDays;$i++) {
            $cdate = $this->year . '-' . $this->month . '-' . $i;
            $deposit = new $ci->Reports_deposits_model;
            $deposit->setReportDate($cdate,true);
            //$reports->set_select('(SELECT COUNT(*) FROM reports_deposits WHERE report_date=\''.$cdate.'\') as deposit');
            //$reports->set_select('(SELECT COUNT(*) FROM reports_disbursements WHERE report_date=\''.$cdate.'\') as disburse');

            $disbursement = new $ci->Reports_disbursements_model;
            $disbursement->setReportDate($cdate,true);
            $results = array(
                'deposit' => $deposit->count_all_results(),
                'disburse' => $disbursement->count_all_results()
                );
            $this->reports[$cdate] = (object) $results;
        }
    }

    public function getMaxMinYear() {
        $ci = get_instance();
        $ci->load->model('Reports_deposits_model');
        $reports = new $ci->Reports_deposits_model;
        $reports->set_select("(SELECT MAX(report_date) FROM  `reports_deposits`) as dep_max_date");
        $reports->set_select("(SELECT MIN(report_date) FROM  `reports_deposits`) as dep_min_date");
        $reports->set_select("(SELECT MAX(report_date) FROM  `reports_disbursements`) as dis_max_date");
        $reports->set_select("(SELECT MIN(report_date) FROM  `reports_disbursements`) as dis_min_date");
        $reports_data = $reports->get();

        return array(
            'min_year' => ( date( 'Y', strtotime($reports_data->dep_min_date) ) > date( 'Y', strtotime($reports_data->dis_min_date) ) ) ? date( 'Y', strtotime($reports_data->dis_min_date) ) : date( 'Y', strtotime($reports_data->dep_min_date) ),
            'max_year' => ( date( 'Y', strtotime($reports_data->dep_max_date) ) > date( 'Y', strtotime($reports_data->dis_max_date) ) ) ? date( 'Y', strtotime($reports_data->dep_max_date) ) : date( 'Y', strtotime($reports_data->dis_max_date) )
            );
    }

    public function init() {
        $this->getReports();
        return $this;
    }
}

/* End of file Global_variables.php */