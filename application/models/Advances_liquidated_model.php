<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Advances_liquidated_model Class
 *
 * Manipulates `advances_liquidated` table on database

CREATE TABLE `advances_liquidated` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `adv_id` int(20) NOT NULL,
  `cv_id` int(20) NOT NULL,
  `cv_date` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `num` varchar(50) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cv_id` (`cv_id`)
);

ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `adv_id` int(20) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `cv_id` int(20) NOT NULL   UNIQUE KEY;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `cv_date` date NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `amount` decimal(20,5) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `num` varchar(50) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `notes` text NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Advances_liquidated_model extends MY_Model {

	protected $id;
	protected $adv_id;
	protected $cv_id;
	protected $cv_date;
	protected $amount;
	protected $num;
	protected $notes;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'advances_liquidated';
		$this->_short_name = 'advances_liquidated';
		$this->_fields = array("id","adv_id","cv_id","cv_date","amount","num","notes");
		$this->_required = array("adv_id","cv_id","cv_date","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: adv_id -------------------------------------- 

	/** 
	* Sets a value to `adv_id` variable
	* @access public
	*/

	public function setAdvId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('adv_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `adv_id` variable
	* @access public
	*/

	public function getAdvId() {
		return $this->adv_id;
	}
	
// ------------------------------ End Field: adv_id --------------------------------------


// ---------------------------- Start Field: cv_id -------------------------------------- 

	/** 
	* Sets a value to `cv_id` variable
	* @access public
	*/

	public function setCvId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('cv_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `cv_id` variable
	* @access public
	*/

	public function getCvId() {
		return $this->cv_id;
	}
	
// ------------------------------ End Field: cv_id --------------------------------------


// ---------------------------- Start Field: cv_date -------------------------------------- 

	/** 
	* Sets a value to `cv_date` variable
	* @access public
	*/

	public function setCvDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('cv_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `cv_date` variable
	* @access public
	*/

	public function getCvDate() {
		return $this->cv_date;
	}
	
// ------------------------------ End Field: cv_date --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->amount;
	}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: num -------------------------------------- 

	/** 
	* Sets a value to `num` variable
	* @access public
	*/

	public function setNum($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('num', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `num` variable
	* @access public
	*/

	public function getNum() {
		return $this->num;
	}
	
// ------------------------------ End Field: num --------------------------------------


// ---------------------------- Start Field: notes -------------------------------------- 

	/** 
	* Sets a value to `notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->notes;
	}
	
// ------------------------------ End Field: notes --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'adv_id' => (object) array(
										'Field'=>'adv_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'cv_id' => (object) array(
										'Field'=>'cv_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'UNI',
										'Default'=>'',
										'Extra'=>''
									),

			'cv_date' => (object) array(
										'Field'=>'cv_date',
										'Type'=>'date',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'amount' => (object) array(
										'Field'=>'amount',
										'Type'=>'decimal(20,5)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'num' => (object) array(
										'Field'=>'num',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'notes' => (object) array(
										'Field'=>'notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'adv_id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `adv_id` int(20) NOT NULL   ;",
			'cv_id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `cv_id` int(20) NOT NULL   UNIQUE KEY;",
			'cv_date' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `cv_date` date NOT NULL   ;",
			'amount' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `amount` decimal(20,5) NOT NULL   ;",
			'num' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `num` varchar(50) NULL   ;",
			'notes' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidated` ADD  `notes` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Advances_liquidated_model.php */
/* Location: ./application/models/Advances_liquidated_model.php */
