<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Advances_liquidation_model Class
 *
 * Manipulates `advances_liquidation` table on database

CREATE TABLE `advances_liquidation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `db_id` int(20) NOT NULL,
  `name_id` int(20) NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `liquidated` int(1) NOT NULL DEFAULT '0',
  `notes` text,
  `group_id` int(20) DEFAULT NULL,
  `class_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `db_id` int(20) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `name_id` int(20) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `amount` decimal(20,5) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `liquidated` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `notes` text NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `group_id` int(20) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `class_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Advances_liquidation_model extends MY_Model {

	protected $id;
	protected $db_id;
	protected $name_id;
	protected $amount;
	protected $liquidated;
	protected $notes;
	protected $group_id;
	protected $class_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'advances_liquidation';
		$this->_short_name = 'advances_liquidation';
		$this->_fields = array("id","db_id","name_id","amount","liquidated","notes","group_id","class_id");
		$this->_required = array("db_id","name_id","amount","liquidated");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: db_id -------------------------------------- 

	/** 
	* Sets a value to `db_id` variable
	* @access public
	*/

	public function setDbId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('db_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `db_id` variable
	* @access public
	*/

	public function getDbId() {
		return $this->db_id;
	}
	
// ------------------------------ End Field: db_id --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}
	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->amount;
	}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: liquidated -------------------------------------- 

	/** 
	* Sets a value to `liquidated` variable
	* @access public
	*/

	public function setLiquidated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('liquidated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `liquidated` variable
	* @access public
	*/

	public function getLiquidated() {
		return $this->liquidated;
	}
	
// ------------------------------ End Field: liquidated --------------------------------------


// ---------------------------- Start Field: notes -------------------------------------- 

	/** 
	* Sets a value to `notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->notes;
	}
	
// ------------------------------ End Field: notes --------------------------------------


// ---------------------------- Start Field: group_id -------------------------------------- 

	/** 
	* Sets a value to `group_id` variable
	* @access public
	*/

	public function setGroupId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group_id` variable
	* @access public
	*/

	public function getGroupId() {
		return $this->group_id;
	}
	
// ------------------------------ End Field: group_id --------------------------------------


// ---------------------------- Start Field: class_id -------------------------------------- 

	/** 
	* Sets a value to `class_id` variable
	* @access public
	*/

	public function setClassId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('class_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `class_id` variable
	* @access public
	*/

	public function getClassId() {
		return $this->class_id;
	}
	
// ------------------------------ End Field: class_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'db_id' => (object) array(
										'Field'=>'db_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'amount' => (object) array(
										'Field'=>'amount',
										'Type'=>'decimal(20,5)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'liquidated' => (object) array(
										'Field'=>'liquidated',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'notes' => (object) array(
										'Field'=>'notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'group_id' => (object) array(
										'Field'=>'group_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'class_id' => (object) array(
										'Field'=>'class_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'db_id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `db_id` int(20) NOT NULL   ;",
			'name_id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `name_id` int(20) NOT NULL   ;",
			'amount' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `amount` decimal(20,5) NOT NULL   ;",
			'liquidated' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `liquidated` int(1) NOT NULL   DEFAULT '0';",
			'notes' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `notes` text NULL   ;",
			'group_id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `group_id` int(20) NULL   ;",
			'class_id' => "ALTER TABLE  `{$this->_db->database}`.`advances_liquidation` ADD  `class_id` int(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Advances_liquidation_model.php */
/* Location: ./application/models/Advances_liquidation_model.php */
