<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Bank_accounts_model Class
 *
 * Manipulates `bank_accounts` table on database

CREATE TABLE `bank_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fund_id` int(10) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `fund_category` varchar(100) DEFAULT 'disposable',
  `priority` int(2) NOT NULL DEFAULT '0',
  `class` varchar(50) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `memo` text,
  `acct_type` varchar(20) NOT NULL DEFAULT 'checking',
  `short_name` varchar(10) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `fund_id` int(10) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `bank_name` varchar(100) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `account_number` varchar(100) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `fund_category` varchar(100) NULL   DEFAULT 'disposable';
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `priority` int(2) NOT NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `class` varchar(50) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `branch` varchar(100) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `memo` text NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `acct_type` varchar(20) NOT NULL   DEFAULT 'checking';
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `short_name` varchar(10) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `active` int(1) NOT NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Bank_accounts_model extends MY_Model {

	protected $id;
	protected $fund_id;
	protected $bank_name;
	protected $account_number;
	protected $fund_category;
	protected $priority;
	protected $class;
	protected $branch;
	protected $memo;
	protected $acct_type;
	protected $short_name;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'bank_accounts';
		$this->_short_name = 'bank_accounts';
		$this->_fields = array("id","fund_id","bank_name","account_number","fund_category","priority","class","branch","memo","acct_type","short_name","active");
		$this->_required = array("fund_id","bank_name","account_number","priority","acct_type","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: fund_id -------------------------------------- 

	/** 
	* Sets a value to `fund_id` variable
	* @access public
	*/

	public function setFundId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('fund_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `fund_id` variable
	* @access public
	*/

	public function getFundId() {
		return $this->fund_id;
	}
	
// ------------------------------ End Field: fund_id --------------------------------------


// ---------------------------- Start Field: bank_name -------------------------------------- 

	/** 
	* Sets a value to `bank_name` variable
	* @access public
	*/

	public function setBankName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('bank_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `bank_name` variable
	* @access public
	*/

	public function getBankName() {
		return $this->bank_name;
	}
	
// ------------------------------ End Field: bank_name --------------------------------------


// ---------------------------- Start Field: account_number -------------------------------------- 

	/** 
	* Sets a value to `account_number` variable
	* @access public
	*/

	public function setAccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('account_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `account_number` variable
	* @access public
	*/

	public function getAccountNumber() {
		return $this->account_number;
	}
	
// ------------------------------ End Field: account_number --------------------------------------


// ---------------------------- Start Field: fund_category -------------------------------------- 

	/** 
	* Sets a value to `fund_category` variable
	* @access public
	*/

	public function setFundCategory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('fund_category', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `fund_category` variable
	* @access public
	*/

	public function getFundCategory() {
		return $this->fund_category;
	}
	
// ------------------------------ End Field: fund_category --------------------------------------


// ---------------------------- Start Field: priority -------------------------------------- 

	/** 
	* Sets a value to `priority` variable
	* @access public
	*/

	public function setPriority($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('priority', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `priority` variable
	* @access public
	*/

	public function getPriority() {
		return $this->priority;
	}
	
// ------------------------------ End Field: priority --------------------------------------


// ---------------------------- Start Field: class -------------------------------------- 

	/** 
	* Sets a value to `class` variable
	* @access public
	*/

	public function setClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `class` variable
	* @access public
	*/

	public function getClass() {
		return $this->class;
	}
	
// ------------------------------ End Field: class --------------------------------------


// ---------------------------- Start Field: branch -------------------------------------- 

	/** 
	* Sets a value to `branch` variable
	* @access public
	*/

	public function setBranch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('branch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `branch` variable
	* @access public
	*/

	public function getBranch() {
		return $this->branch;
	}
	
// ------------------------------ End Field: branch --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->memo;
	}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: acct_type -------------------------------------- 

	/** 
	* Sets a value to `acct_type` variable
	* @access public
	*/

	public function setAcctType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('acct_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `acct_type` variable
	* @access public
	*/

	public function getAcctType() {
		return $this->acct_type;
	}
	
// ------------------------------ End Field: acct_type --------------------------------------


// ---------------------------- Start Field: short_name -------------------------------------- 

	/** 
	* Sets a value to `short_name` variable
	* @access public
	*/

	public function setShortName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('short_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `short_name` variable
	* @access public
	*/

	public function getShortName() {
		return $this->short_name;
	}
	
// ------------------------------ End Field: short_name --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}
	
// ------------------------------ End Field: active --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'fund_id' => (object) array(
										'Field'=>'fund_id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'bank_name' => (object) array(
										'Field'=>'bank_name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'account_number' => (object) array(
										'Field'=>'account_number',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'fund_category' => (object) array(
										'Field'=>'fund_category',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'disposable',
										'Extra'=>''
									),

			'priority' => (object) array(
										'Field'=>'priority',
										'Type'=>'int(2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'class' => (object) array(
										'Field'=>'class',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'branch' => (object) array(
										'Field'=>'branch',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'memo' => (object) array(
										'Field'=>'memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'acct_type' => (object) array(
										'Field'=>'acct_type',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'checking',
										'Extra'=>''
									),

			'short_name' => (object) array(
										'Field'=>'short_name',
										'Type'=>'varchar(10)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'fund_id' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `fund_id` int(10) NOT NULL   ;",
			'bank_name' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `bank_name` varchar(100) NOT NULL   ;",
			'account_number' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `account_number` varchar(100) NOT NULL   ;",
			'fund_category' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `fund_category` varchar(100) NULL   DEFAULT 'disposable';",
			'priority' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `priority` int(2) NOT NULL   DEFAULT '0';",
			'class' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `class` varchar(50) NULL   ;",
			'branch' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `branch` varchar(100) NULL   ;",
			'memo' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `memo` text NULL   ;",
			'acct_type' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `acct_type` varchar(20) NOT NULL   DEFAULT 'checking';",
			'short_name' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `short_name` varchar(10) NULL   ;",
			'active' => "ALTER TABLE  `{$this->_db->database}`.`bank_accounts` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Bank_accounts_model.php */
/* Location: ./application/models/Bank_accounts_model.php */
