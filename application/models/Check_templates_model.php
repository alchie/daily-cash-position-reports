<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Check_templates_model Class
 *
 * Manipulates `check_templates` table on database

CREATE TABLE `check_templates` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `settings` text,
  `active` int(1) NOT NULL DEFAULT '1',
  `default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `name` varchar(100) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `settings` text NULL   ;
ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `active` int(1) NOT NULL   DEFAULT '1';
ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `default` int(1) NOT NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Check_templates_model extends MY_Model {

	protected $id;
	protected $name;
	protected $settings;
	protected $active;
	protected $default;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'check_templates';
		$this->_short_name = 'check_templates';
		$this->_fields = array("id","name","settings","active","default");
		$this->_required = array("name","active","default");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: settings -------------------------------------- 

	/** 
	* Sets a value to `settings` variable
	* @access public
	*/

	public function setSettings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('settings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `settings` variable
	* @access public
	*/

	public function getSettings() {
		return $this->settings;
	}
	
// ------------------------------ End Field: settings --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: default -------------------------------------- 

	/** 
	* Sets a value to `default` variable
	* @access public
	*/

	public function setDefault($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('default', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `default` variable
	* @access public
	*/

	public function getDefault() {
		return $this->default;
	}
	
// ------------------------------ End Field: default --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'settings' => (object) array(
										'Field'=>'settings',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'default' => (object) array(
										'Field'=>'default',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'name' => "ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `name` varchar(100) NOT NULL   ;",
			'settings' => "ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `settings` text NULL   ;",
			'active' => "ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
			'default' => "ALTER TABLE  `{$this->_db->database}`.`check_templates` ADD  `default` int(1) NOT NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Check_templates_model.php */
/* Location: ./application/models/Check_templates_model.php */
