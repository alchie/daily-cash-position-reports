<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Checks_model Class
 *
 * Manipulates `checks` table on database

CREATE TABLE `checks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `signatory1` int(20) DEFAULT NULL,
  `signatory2` int(20) DEFAULT NULL,
  `acct_id` int(20) NOT NULL,
  `check_number` int(20) NOT NULL,
  `payee_id` int(20) DEFAULT NULL,
  `db_id` int(20) DEFAULT NULL,
  `signatory` varchar(100) DEFAULT NULL,
  `presigned` int(20) NOT NULL DEFAULT '0',
  `voided` int(1) NOT NULL DEFAULT '0',
  `cv_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `signatory1` int(20) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `signatory2` int(20) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `acct_id` int(20) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `check_number` int(20) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `payee_id` int(20) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `db_id` int(20) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `signatory` varchar(100) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `presigned` int(20) NOT NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `voided` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `cv_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Checks_model extends MY_Model {

	protected $id;
	protected $signatory1;
	protected $signatory2;
	protected $acct_id;
	protected $check_number;
	protected $payee_id;
	protected $db_id;
	protected $signatory;
	protected $presigned;
	protected $voided;
	protected $cv_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'checks';
		$this->_short_name = 'checks';
		$this->_fields = array("id","signatory1","signatory2","acct_id","check_number","payee_id","db_id","signatory","presigned","voided","cv_id");
		$this->_required = array("acct_id","check_number","presigned","voided");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: signatory1 -------------------------------------- 

	/** 
	* Sets a value to `signatory1` variable
	* @access public
	*/

	public function setSignatory1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('signatory1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `signatory1` variable
	* @access public
	*/

	public function getSignatory1() {
		return $this->signatory1;
	}
	
// ------------------------------ End Field: signatory1 --------------------------------------


// ---------------------------- Start Field: signatory2 -------------------------------------- 

	/** 
	* Sets a value to `signatory2` variable
	* @access public
	*/

	public function setSignatory2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('signatory2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `signatory2` variable
	* @access public
	*/

	public function getSignatory2() {
		return $this->signatory2;
	}
	
// ------------------------------ End Field: signatory2 --------------------------------------


// ---------------------------- Start Field: acct_id -------------------------------------- 

	/** 
	* Sets a value to `acct_id` variable
	* @access public
	*/

	public function setAcctId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('acct_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `acct_id` variable
	* @access public
	*/

	public function getAcctId() {
		return $this->acct_id;
	}
	
// ------------------------------ End Field: acct_id --------------------------------------


// ---------------------------- Start Field: check_number -------------------------------------- 

	/** 
	* Sets a value to `check_number` variable
	* @access public
	*/

	public function setCheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('check_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `check_number` variable
	* @access public
	*/

	public function getCheckNumber() {
		return $this->check_number;
	}
	
// ------------------------------ End Field: check_number --------------------------------------


// ---------------------------- Start Field: payee_id -------------------------------------- 

	/** 
	* Sets a value to `payee_id` variable
	* @access public
	*/

	public function setPayeeId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('payee_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `payee_id` variable
	* @access public
	*/

	public function getPayeeId() {
		return $this->payee_id;
	}
	
// ------------------------------ End Field: payee_id --------------------------------------


// ---------------------------- Start Field: db_id -------------------------------------- 

	/** 
	* Sets a value to `db_id` variable
	* @access public
	*/

	public function setDbId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('db_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `db_id` variable
	* @access public
	*/

	public function getDbId() {
		return $this->db_id;
	}
	
// ------------------------------ End Field: db_id --------------------------------------


// ---------------------------- Start Field: signatory -------------------------------------- 

	/** 
	* Sets a value to `signatory` variable
	* @access public
	*/

	public function setSignatory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('signatory', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `signatory` variable
	* @access public
	*/

	public function getSignatory() {
		return $this->signatory;
	}
	
// ------------------------------ End Field: signatory --------------------------------------


// ---------------------------- Start Field: presigned -------------------------------------- 

	/** 
	* Sets a value to `presigned` variable
	* @access public
	*/

	public function setPresigned($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('presigned', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `presigned` variable
	* @access public
	*/

	public function getPresigned() {
		return $this->presigned;
	}
	
// ------------------------------ End Field: presigned --------------------------------------


// ---------------------------- Start Field: voided -------------------------------------- 

	/** 
	* Sets a value to `voided` variable
	* @access public
	*/

	public function setVoided($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('voided', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `voided` variable
	* @access public
	*/

	public function getVoided() {
		return $this->voided;
	}
	
// ------------------------------ End Field: voided --------------------------------------


// ---------------------------- Start Field: cv_id -------------------------------------- 

	/** 
	* Sets a value to `cv_id` variable
	* @access public
	*/

	public function setCvId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('cv_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `cv_id` variable
	* @access public
	*/

	public function getCvId() {
		return $this->cv_id;
	}
	
// ------------------------------ End Field: cv_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'signatory1' => (object) array(
										'Field'=>'signatory1',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'signatory2' => (object) array(
										'Field'=>'signatory2',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'acct_id' => (object) array(
										'Field'=>'acct_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'check_number' => (object) array(
										'Field'=>'check_number',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'payee_id' => (object) array(
										'Field'=>'payee_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'db_id' => (object) array(
										'Field'=>'db_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'signatory' => (object) array(
										'Field'=>'signatory',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'presigned' => (object) array(
										'Field'=>'presigned',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'voided' => (object) array(
										'Field'=>'voided',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'cv_id' => (object) array(
										'Field'=>'cv_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'signatory1' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `signatory1` int(20) NULL   ;",
			'signatory2' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `signatory2` int(20) NULL   ;",
			'acct_id' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `acct_id` int(20) NOT NULL   ;",
			'check_number' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `check_number` int(20) NOT NULL   ;",
			'payee_id' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `payee_id` int(20) NULL   ;",
			'db_id' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `db_id` int(20) NULL   ;",
			'signatory' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `signatory` varchar(100) NULL   ;",
			'presigned' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `presigned` int(20) NOT NULL   DEFAULT '0';",
			'voided' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `voided` int(1) NOT NULL   DEFAULT '0';",
			'cv_id' => "ALTER TABLE  `{$this->_db->database}`.`checks` ADD  `cv_id` int(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Checks_model.php */
/* Location: ./application/models/Checks_model.php */
