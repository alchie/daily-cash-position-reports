<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Fund_groups_model Class
 *
 * Manipulates `fund_groups` table on database

CREATE TABLE `fund_groups` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `priority` int(3) DEFAULT '0',
  `type` varchar(50) DEFAULT 'group',
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `name` varchar(200) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `priority` int(3) NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `type` varchar(50) NULL   DEFAULT 'group';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Fund_groups_model extends MY_Model {

	protected $id;
	protected $name;
	protected $priority;
	protected $type;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'fund_groups';
		$this->_short_name = 'fund_groups';
		$this->_fields = array("id","name","priority","type");
		$this->_required = array("name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: priority -------------------------------------- 

	/** 
	* Sets a value to `priority` variable
	* @access public
	*/

	public function setPriority($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('priority', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `priority` variable
	* @access public
	*/

	public function getPriority() {
		return $this->priority;
	}
	
// ------------------------------ End Field: priority --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}
	
// ------------------------------ End Field: type --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'priority' => (object) array(
										'Field'=>'priority',
										'Type'=>'int(3)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'group',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'name' => "ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `name` varchar(200) NOT NULL   ;",
			'priority' => "ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `priority` int(3) NULL   DEFAULT '0';",
			'type' => "ALTER TABLE  `{$this->_db->database}`.`fund_groups` ADD  `type` varchar(50) NULL   DEFAULT 'group';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Fund_groups_model.php */
/* Location: ./application/models/Fund_groups_model.php */
