<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Funds_model Class
 *
 * Manipulates `funds` table on database

CREATE TABLE `funds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `priority` int(10) DEFAULT '0',
  `group` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`)
);

ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `name` varchar(100) NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `priority` int(10) NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `group` int(20) NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Funds_model extends MY_Model {

	protected $id;
	protected $name;
	protected $priority;
	protected $group;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'funds';
		$this->_short_name = 'funds';
		$this->_fields = array("id","name","priority","group");
		$this->_required = array("name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: priority -------------------------------------- 

	/** 
	* Sets a value to `priority` variable
	* @access public
	*/

	public function setPriority($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('priority', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `priority` variable
	* @access public
	*/

	public function getPriority() {
		return $this->priority;
	}
	
// ------------------------------ End Field: priority --------------------------------------


// ---------------------------- Start Field: group -------------------------------------- 

	/** 
	* Sets a value to `group` variable
	* @access public
	*/

	public function setGroup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group` variable
	* @access public
	*/

	public function getGroup() {
		return $this->group;
	}
	
// ------------------------------ End Field: group --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'priority' => (object) array(
										'Field'=>'priority',
										'Type'=>'int(10)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'group' => (object) array(
										'Field'=>'group',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'name' => "ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `name` varchar(100) NOT NULL   ;",
			'priority' => "ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `priority` int(10) NULL   DEFAULT '0';",
			'group' => "ALTER TABLE  `{$this->_db->database}`.`funds` ADD  `group` int(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Funds_model.php */
/* Location: ./application/models/Funds_model.php */
