<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Payee_model Class
 *
 * Manipulates `payee` table on database

CREATE TABLE `payee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `payee` text NOT NULL,
  `two_lines` int(1) NOT NULL DEFAULT '0',
  `trash` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `payee` text NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `two_lines` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `trash` int(1) NOT NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Payee_model extends MY_Model {

	protected $id;
	protected $payee;
	protected $two_lines;
	protected $trash;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'payee';
		$this->_short_name = 'payee';
		$this->_fields = array("id","payee","two_lines","trash");
		$this->_required = array("payee","two_lines","trash");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: payee -------------------------------------- 

	/** 
	* Sets a value to `payee` variable
	* @access public
	*/

	public function setPayee($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('payee', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `payee` variable
	* @access public
	*/

	public function getPayee() {
		return $this->payee;
	}
	
// ------------------------------ End Field: payee --------------------------------------


// ---------------------------- Start Field: two_lines -------------------------------------- 

	/** 
	* Sets a value to `two_lines` variable
	* @access public
	*/

	public function setTwoLines($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('two_lines', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `two_lines` variable
	* @access public
	*/

	public function getTwoLines() {
		return $this->two_lines;
	}
	
// ------------------------------ End Field: two_lines --------------------------------------


// ---------------------------- Start Field: trash -------------------------------------- 

	/** 
	* Sets a value to `trash` variable
	* @access public
	*/

	public function setTrash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `trash` variable
	* @access public
	*/

	public function getTrash() {
		return $this->trash;
	}
	
// ------------------------------ End Field: trash --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'payee' => (object) array(
										'Field'=>'payee',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'two_lines' => (object) array(
										'Field'=>'two_lines',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'trash' => (object) array(
										'Field'=>'trash',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'payee' => "ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `payee` text NOT NULL   ;",
			'two_lines' => "ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `two_lines` int(1) NOT NULL   DEFAULT '0';",
			'trash' => "ALTER TABLE  `{$this->_db->database}`.`payee` ADD  `trash` int(1) NOT NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Payee_model.php */
/* Location: ./application/models/Payee_model.php */
