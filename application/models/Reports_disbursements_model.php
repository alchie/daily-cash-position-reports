<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Reports_disbursements_model Class
 *
 * Manipulates `reports_disbursements` table on database

CREATE TABLE `reports_disbursements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bank_id` int(20) DEFAULT '0',
  `amount` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `report_date` date NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `dp_id` int(20) DEFAULT NULL,
  `company` varchar(50) NOT NULL DEFAULT 'rcbdi',
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `bank_id` int(20) NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `amount` decimal(20,4) NOT NULL   DEFAULT '0.0000';
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `report_date` date NOT NULL   ;
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `description` varchar(200) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `type` varchar(100) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `dp_id` int(20) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `company` varchar(50) NOT NULL   DEFAULT 'rcbdi';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Reports_disbursements_model extends MY_Model {

	protected $id;
	protected $bank_id;
	protected $amount;
	protected $report_date;
	protected $description;
	protected $type;
	protected $dp_id;
	protected $company;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'reports_disbursements';
		$this->_short_name = 'reports_disbursements';
		$this->_fields = array("id","bank_id","amount","report_date","description","type","dp_id","company");
		$this->_required = array("amount","report_date","company");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: bank_id -------------------------------------- 

	/** 
	* Sets a value to `bank_id` variable
	* @access public
	*/

	public function setBankId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('bank_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `bank_id` variable
	* @access public
	*/

	public function getBankId() {
		return $this->bank_id;
	}
	
// ------------------------------ End Field: bank_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->amount;
	}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: report_date -------------------------------------- 

	/** 
	* Sets a value to `report_date` variable
	* @access public
	*/

	public function setReportDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `report_date` variable
	* @access public
	*/

	public function getReportDate() {
		return $this->report_date;
	}
	
// ------------------------------ End Field: report_date --------------------------------------


// ---------------------------- Start Field: description -------------------------------------- 

	/** 
	* Sets a value to `description` variable
	* @access public
	*/

	public function setDescription($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('description', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `description` variable
	* @access public
	*/

	public function getDescription() {
		return $this->description;
	}
	
// ------------------------------ End Field: description --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: dp_id -------------------------------------- 

	/** 
	* Sets a value to `dp_id` variable
	* @access public
	*/

	public function setDpId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('dp_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `dp_id` variable
	* @access public
	*/

	public function getDpId() {
		return $this->dp_id;
	}
	
// ------------------------------ End Field: dp_id --------------------------------------


// ---------------------------- Start Field: company -------------------------------------- 

	/** 
	* Sets a value to `company` variable
	* @access public
	*/

	public function setCompany($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('company', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `company` variable
	* @access public
	*/

	public function getCompany() {
		return $this->company;
	}
	
// ------------------------------ End Field: company --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'bank_id' => (object) array(
										'Field'=>'bank_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'amount' => (object) array(
										'Field'=>'amount',
										'Type'=>'decimal(20,4)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0.0000',
										'Extra'=>''
									),

			'report_date' => (object) array(
										'Field'=>'report_date',
										'Type'=>'date',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'description' => (object) array(
										'Field'=>'description',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'dp_id' => (object) array(
										'Field'=>'dp_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'company' => (object) array(
										'Field'=>'company',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'rcbdi',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'bank_id' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `bank_id` int(20) NULL   DEFAULT '0';",
			'amount' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `amount` decimal(20,4) NOT NULL   DEFAULT '0.0000';",
			'report_date' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `report_date` date NOT NULL   ;",
			'description' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `description` varchar(200) NULL   ;",
			'type' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `type` varchar(100) NULL   ;",
			'dp_id' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `dp_id` int(20) NULL   ;",
			'company' => "ALTER TABLE  `{$this->_db->database}`.`reports_disbursements` ADD  `company` varchar(50) NOT NULL   DEFAULT 'rcbdi';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Reports_disbursements_model.php */
/* Location: ./application/models/Reports_disbursements_model.php */
