<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Reports_model Class
 *
 * Manipulates `reports` table on database

CREATE TABLE `reports` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `beginning` int(10) DEFAULT NULL,
  `report_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `{$this->_db->database}`.`reports` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`reports` ADD  `beginning` int(10) NULL   ;
ALTER TABLE  `{$this->_db->database}`.`reports` ADD  `report_date` int(11) NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Reports_model extends MY_Model {

	protected $id;
	protected $beginning;
	protected $report_date;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'reports';
		$this->_short_name = 'reports';
		$this->_fields = array("id","beginning","report_date");
		$this->_required = array("report_date");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: beginning -------------------------------------- 

	/** 
	* Sets a value to `beginning` variable
	* @access public
	*/

	public function setBeginning($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('beginning', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `beginning` variable
	* @access public
	*/

	public function getBeginning() {
		return $this->beginning;
	}
	
// ------------------------------ End Field: beginning --------------------------------------


// ---------------------------- Start Field: report_date -------------------------------------- 

	/** 
	* Sets a value to `report_date` variable
	* @access public
	*/

	public function setReportDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `report_date` variable
	* @access public
	*/

	public function getReportDate() {
		return $this->report_date;
	}
	
// ------------------------------ End Field: report_date --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'beginning' => (object) array(
										'Field'=>'beginning',
										'Type'=>'int(10)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'report_date' => (object) array(
										'Field'=>'report_date',
										'Type'=>'int(11)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`reports` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'beginning' => "ALTER TABLE  `{$this->_db->database}`.`reports` ADD  `beginning` int(10) NULL   ;",
			'report_date' => "ALTER TABLE  `{$this->_db->database}`.`reports` ADD  `report_date` int(11) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Reports_model.php */
/* Location: ./application/models/Reports_model.php */
