<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Signatory_model Class
 *
 * Manipulates `signatory` table on database

CREATE TABLE `signatory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `signatory` varchar(255) NOT NULL,
  `priority` int(5) DEFAULT '0',
  `signatory1` int(1) NOT NULL DEFAULT '0',
  `signatory2` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `signatory` (`signatory`)
);

ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `signatory` varchar(255) NOT NULL   UNIQUE KEY;
ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `priority` int(5) NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `signatory1` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `signatory2` int(1) NOT NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.2.10
 */
 
class Signatory_model extends MY_Model {

	protected $id;
	protected $signatory;
	protected $priority;
	protected $signatory1;
	protected $signatory2;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'signatory';
		$this->_short_name = 'signatory';
		$this->_fields = array("id","signatory","priority","signatory1","signatory2");
		$this->_required = array("signatory","signatory1","signatory2");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: signatory -------------------------------------- 

	/** 
	* Sets a value to `signatory` variable
	* @access public
	*/

	public function setSignatory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('signatory', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `signatory` variable
	* @access public
	*/

	public function getSignatory() {
		return $this->signatory;
	}
	
// ------------------------------ End Field: signatory --------------------------------------


// ---------------------------- Start Field: priority -------------------------------------- 

	/** 
	* Sets a value to `priority` variable
	* @access public
	*/

	public function setPriority($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('priority', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `priority` variable
	* @access public
	*/

	public function getPriority() {
		return $this->priority;
	}
	
// ------------------------------ End Field: priority --------------------------------------


// ---------------------------- Start Field: signatory1 -------------------------------------- 

	/** 
	* Sets a value to `signatory1` variable
	* @access public
	*/

	public function setSignatory1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('signatory1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `signatory1` variable
	* @access public
	*/

	public function getSignatory1() {
		return $this->signatory1;
	}
	
// ------------------------------ End Field: signatory1 --------------------------------------


// ---------------------------- Start Field: signatory2 -------------------------------------- 

	/** 
	* Sets a value to `signatory2` variable
	* @access public
	*/

	public function setSignatory2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('signatory2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `signatory2` variable
	* @access public
	*/

	public function getSignatory2() {
		return $this->signatory2;
	}
	
// ------------------------------ End Field: signatory2 --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'signatory' => (object) array(
										'Field'=>'signatory',
										'Type'=>'varchar(255)',
										'Null'=>'NO',
										'Key'=>'UNI',
										'Default'=>'',
										'Extra'=>''
									),

			'priority' => (object) array(
										'Field'=>'priority',
										'Type'=>'int(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'signatory1' => (object) array(
										'Field'=>'signatory1',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'signatory2' => (object) array(
										'Field'=>'signatory2',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'signatory' => "ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `signatory` varchar(255) NOT NULL   UNIQUE KEY;",
			'priority' => "ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `priority` int(5) NULL   DEFAULT '0';",
			'signatory1' => "ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `signatory1` int(1) NOT NULL   DEFAULT '0';",
			'signatory2' => "ALTER TABLE  `{$this->_db->database}`.`signatory` ADD  `signatory2` int(1) NOT NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Signatory_model.php */
/* Location: ./application/models/Signatory_model.php */
