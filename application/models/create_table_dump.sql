-- Table structure for table `account_sessions` 

CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `advances_groups` 

CREATE TABLE `advances_groups` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `priority` int(3) DEFAULT '0',
  `type` varchar(50) DEFAULT 'group',
  PRIMARY KEY (`id`)
);

-- Table structure for table `advances_liquidated` 

CREATE TABLE `advances_liquidated` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `adv_id` int(20) NOT NULL,
  `cv_id` int(20) NOT NULL,
  `cv_date` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `num` varchar(50) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cv_id` (`cv_id`)
);

-- Table structure for table `advances_liquidation` 

CREATE TABLE `advances_liquidation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `db_id` int(20) NOT NULL,
  `name_id` int(20) NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `liquidated` int(1) NOT NULL DEFAULT '0',
  `notes` text,
  `group_id` int(20) DEFAULT NULL,
  `class_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `bank_accounts` 

CREATE TABLE `bank_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fund_id` int(10) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `fund_category` varchar(100) DEFAULT 'disposable',
  `priority` int(2) NOT NULL DEFAULT '0',
  `class` varchar(50) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `memo` text,
  `acct_type` varchar(20) NOT NULL DEFAULT 'checking',
  `short_name` varchar(10) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `check_templates` 

CREATE TABLE `check_templates` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `settings` text,
  `active` int(1) NOT NULL DEFAULT '1',
  `default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `checks` 

CREATE TABLE `checks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `signatory1` int(20) DEFAULT NULL,
  `signatory2` int(20) DEFAULT NULL,
  `acct_id` int(20) NOT NULL,
  `check_number` int(20) NOT NULL,
  `payee_id` int(20) DEFAULT NULL,
  `db_id` int(20) DEFAULT NULL,
  `signatory` varchar(100) DEFAULT NULL,
  `presigned` int(20) NOT NULL DEFAULT '0',
  `voided` int(1) NOT NULL DEFAULT '0',
  `cv_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `fund_groups` 

CREATE TABLE `fund_groups` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `priority` int(3) DEFAULT '0',
  `type` varchar(50) DEFAULT 'group',
  PRIMARY KEY (`id`)
);

-- Table structure for table `funds` 

CREATE TABLE `funds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `priority` int(10) DEFAULT '0',
  `group` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`)
);

-- Table structure for table `payee` 

CREATE TABLE `payee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `payee` text NOT NULL,
  `two_lines` int(1) NOT NULL DEFAULT '0',
  `trash` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `reports` 

CREATE TABLE `reports` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `beginning` int(10) DEFAULT NULL,
  `report_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `reports_deposits` 

CREATE TABLE `reports_deposits` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bank_id` int(20) DEFAULT '0',
  `amount` decimal(20,4) NOT NULL,
  `report_date` date NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `dbm_id` int(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `company` varchar(50) NOT NULL DEFAULT 'rcbdi',
  PRIMARY KEY (`id`)
);

-- Table structure for table `reports_disbursements` 

CREATE TABLE `reports_disbursements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bank_id` int(20) DEFAULT '0',
  `amount` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `report_date` date NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `dp_id` int(20) DEFAULT NULL,
  `company` varchar(50) NOT NULL DEFAULT 'rcbdi',
  PRIMARY KEY (`id`)
);

-- Table structure for table `signatory` 

CREATE TABLE `signatory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `signatory` varchar(255) NOT NULL,
  `priority` int(5) DEFAULT '0',
  `signatory1` int(1) NOT NULL DEFAULT '0',
  `signatory2` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `signatory` (`signatory`)
);

-- Table structure for table `user_accounts` 

CREATE TABLE `user_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
);

