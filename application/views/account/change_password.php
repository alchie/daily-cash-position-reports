<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
          <div class="panel-heading">Change Password</div>
          <div class="panel-body">
            <form method="post">
              <div class="form-group">
                <label>Current Password</label>
                <input type="password" class="form-control" placeholder="Current Password" name="current_password" required="required">
              </div>
              <div class="form-group">
                <label>New Password</label>
                <input type="password" class="form-control" placeholder="New Password" name="new_password" required="required">
              </div>
              <div class="form-group">
                <label>Repeat Password</label>
                <input type="password" class="form-control" placeholder="Repeat Password" name="repeat_password" required="required">
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-success" value="Submit">
              </div>
            </form>
          </div>
        </div>
    </div>
</div>
    
<?php $this->load->view('footer'); ?>