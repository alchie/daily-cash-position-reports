<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

<div class="btn-group pull-right" style="margin-left:10px;">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo ($current_class) ? $current_class->name : 'All Class'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo site_url("advances/index/{$name_id}"); ?>?filter=<?php echo $this->input->get('filter'); ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=0">- - - All Class - - -</a></li>
  <?php foreach($adv_groups as $adv_group) { if($adv_group->type=='group') continue; ?>
    <li class="<?php echo ($adv_group->id==$this->input->get('class')) ? 'active' : ''; ?>"><a href="<?php echo site_url("advances/index/{$name_id}"); ?>?filter=<?php echo $this->input->get('filter'); ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=<?php echo $adv_group->id; ?>"><?php echo $adv_group->name; ?></a></li>
  <?php } ?>
  </ul>
</div>

<div class="btn-group pull-right" style="margin-left:10px;">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo ($current_group) ? $current_group->name : 'All Groups'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo site_url("advances/index/{$name_id}"); ?>?filter=<?php echo $this->input->get('filter'); ?>&amp;group=0&amp;class=<?php echo $this->input->get('class'); ?>">- - - All Group - - -</a></li>
  <?php foreach($adv_groups as $adv_group) { if($adv_group->type=='class') continue; ?>
    <li class="<?php echo ($adv_group->id==$this->input->get('group')) ? 'active' : ''; ?>"><a href="<?php echo site_url("advances/index/{$name_id}"); ?>?filter=<?php echo $this->input->get('filter'); ?>&amp;group=<?php echo $adv_group->id; ?>&amp;class=<?php echo $this->input->get('class'); ?>"><?php echo $adv_group->name; ?></a></li>
  <?php } ?>
  </ul>
</div>

<div class="btn-group pull-right">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo ($current_name) ? $current_name->payee : 'All Payee'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo site_url("advances/index/0"); ?>?filter=<?php echo $this->input->get('filter'); ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=<?php echo $this->input->get('class'); ?>">- - - All Payee - - -</a></li>
  <?php foreach($names as $name) { ?>
    <li class="<?php echo ($name->name_id==$name_id) ? 'active' : ''; ?>"><a href="<?php echo site_url("advances/index/{$name->name_id}"); ?>?filter=<?php echo $this->input->get('filter'); ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=<?php echo $this->input->get('class'); ?>"><?php echo $name->payee; ?></a></li>
  <?php } ?>
  </ul>
</div>

<a href="<?php echo site_url("advances/groups"); ?>" class="pull-right btn btn-warning btn-xs" style="margin-right:10px;">Groups</a>

          <h3 class="panel-title pull-left">Advances for Liquidation (<strong><?php echo ($current_name) ? $current_name->payee : 'All Payee'; ?></strong>)</h3>
<center>
  <div class="btn-group">
<a class="btn btn-<?php echo ($filter=='liquidated') ? 'success' : 'default'; ?> btn-xs" href="<?php echo site_url(uri_string()) . "?filter=liquidated"; ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=<?php echo $this->input->get('class'); ?>">Liquidated (<?php echo $total->liquidated; ?>)</a>
<a class="btn btn-<?php echo ($filter=='unliquidated') ? 'success' : 'default'; ?> btn-xs" href="<?php echo site_url(uri_string()) . "?filter=unliquidated"; ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=<?php echo $this->input->get('class'); ?>">Unliquidated (<?php echo $total->unliquidated; ?>)</a>
<a class="btn btn-<?php echo ($filter=='all') ? 'success' : 'default'; ?> btn-xs" href="<?php echo site_url(uri_string()) . "?filter=all"; ?>&amp;group=<?php echo $this->input->get('group'); ?>&amp;class=<?php echo $this->input->get('class'); ?>">All Advances (<?php echo $total->all_count; ?>)</a>
</div>
</center>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
<?php if( $advances ) { ?>
      <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Payee</th>
                  <th>Notes</th>
                  <th>CV #</th>
                  <th>CV Date</th>
                  <th class="text-right">Unliquidated</th>
                  <th width="120px;">Action</th>
                </tr>
              </thead>
              <tbody>
<?php foreach($advances as $item) { ?>
<tr class="<?php echo ($item->liquidated) ? '' : 'danger'; ?>">
  <td class="text-left"><?php echo $item->payee; ?></td>
  <td class="text-left"><small><?php echo $item->notes; ?></small></td>
  <td class="text-left"><?php echo $item->cv_id; ?></td>
  <td class="text-left"><?php echo date('m/d/Y', strtotime($item->report_date)); ?></td>
  <td class="text-right"><?php echo number_format(($item->adv_amount - $item->liquidated_amount),2); ?></td>
  <td class="text-left">

<div class="btn-group pull-right">
<?php if( $item->liquidated ) { ?>
  <a href="<?php echo site_url("advances/liquidate/{$item->adv_id}"); ?>?next=<?php echo uri_string(); ?>" class="btn btn-success btn-xs">View Items</a>
<?php } else { ?>
  <a href="<?php echo site_url("advances/liquidate/{$item->adv_id}"); ?>?next=<?php echo uri_string(); ?>" class="btn btn-danger btn-xs">Liquidate</a>
<?php } ?>
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="<?php echo site_url("checks/write/{$item->db_id}") . "?edit_item=".$item->adv_id."&next=" . uri_string(); ?>?next=<?php echo uri_string(); ?>">Edit Item</a></li>
  </ul>
</div>



  </td>
</tr>
<?php } ?>
              </tbody>
              </table>

<?php echo $pagination; ?>

<?php } else { ?>
<p class="text-center">No Transactions</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>