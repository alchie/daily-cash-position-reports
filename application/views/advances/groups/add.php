<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Advances Group</h3>
        </div>
      <form method="post">
        <div class="panel-body">
        <div class="form-group">
          <label>Type</label>
          <select class="form-control" name="type">
              <option value="group" <?php echo ($this->input->get('type')=='group') ? 'SELECTED' : ''; ?>>Group</option>
              <option value="class" <?php echo ($this->input->get('type')=='class') ? 'SELECTED' : ''; ?>>Class</option>
          </select>
        </div>
        <div class="form-group">
          <label>Name</label>
          <input class="form-control" name="name">
        </div>
        <div class="form-group">
          <label>Priority</label>
          <input class="form-control" name="priority" value="0">
        </div>
        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a class="btn btn-warning" href="<?php echo site_url(($this->input->get('next')) ? $this->input->get('next') : "advances/groups"); ?>">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>