<?php defined('BASEPATH') OR exit('No direct script access allowed');  ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">

<?php if ( $adv->liquidated < $adv->adv_amount )  { ?>
<?php if(  !$this->input->get('edit_item') ) { ?>
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Liquidate (<strong><?php echo $adv->payee; ?> - <?php echo $adv->cv_id; ?> - <?php echo date("m/d/Y", strtotime($adv->report_date)); ?> - <?php echo number_format($adv->adv_amount,2); ?></strong>) </h3>
        </div>
      <form method="post">
      <input type="hidden" name="action" value="add_item">
        <div class="panel-body">
<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>CV #</label>
          <input class="form-control text-center" name="cv_id" value="" required>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>CV Date</label>
          <input class="form-control text-center datepicker" name="cv_date" value="<?php echo date("m/d/Y"); ?>"  required>
        </div>
</div>
</div>
<div class="row">
<div class="col-md-6">
          <div class="form-group">
          <label>Number</label>
          <input class="form-control text-center" name="number" value="">
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Amount</label>
          <input class="form-control text-center" name="amount" value="<?php echo number_format(($adv->adv_amount-$adv->liquidated),2); ?>"  required>
        </div>
</div>
</div>

          <div class="form-group">
          <label>Notes</label>
          <textarea class="form-control" name="notes"></textarea>
        </div>

        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo site_url($this->input->get('next')); ?>" class="btn btn-warning">Back</a>
        </div>
      </form>
      </div>

<?php } ?>
<?php } else { ?>
  <?php if( $adv->status == 0 ) { ?>
    <div class="alert alert-danger" role="alert"><strong>Not yet marked as liquidated!</strong> <a class="badge" href="<?php echo site_url("advances/liquidated/{$adv->adv_id}"); ?>?next=<?php echo ($this->input->get('next')) ? $this->input->get('next') : uri_string(); ?>">Fix Now</a></div>
  <?php } ?>
<?php } ?>
<?php if( $liquidated ) { ?>
        <div class="panel panel-default">
        <div class="panel-heading">
<div class="btn-group pull-right">
<?php if( $adv->status ) { ?>
  <a href="<?php echo site_url("advances/unliquidated/{$adv->adv_id}"); ?>?next=<?php echo $this->input->get('next'); ?>" class="btn btn-danger btn-xs confirm">Unliquidated</a>
<?php } ?>
        <a href="<?php echo site_url(($this->input->get('next'))?$this->input->get('next'):'advances'); ?>" class="btn btn-warning btn-xs">Back</a>
</div>
          <h3 class="panel-title">Items (<strong><?php echo $adv->payee; ?> - <?php echo $adv->cv_id; ?> - <?php echo date("m/d/Y", strtotime($adv->report_date)); ?> - <?php echo number_format($adv->adv_amount,2); ?></strong>)</h3>
        </div>
<div class="list-group">
<?php 
$total = 0;
foreach($liquidated as $liq) { ?>
  <div class="list-group-item">
<?php if( $adv->status == 0  ) { ?>
<a href="<?php echo site_url(uri_string()); ?>?next=<?php echo $this->input->get("next"); ?>&amp;edit_item=<?php echo $liq->id; ?>" class="btn btn-success btn-xs pull-right" style="margin-left:10px"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
<?php } ?>

<span class="badge"><?php echo number_format($liq->amount,2); $total+=$liq->amount; ?></span>

  JV#<?php echo $liq->cv_id; ?> - CV Date: <?php echo date('m/d/Y', strtotime($liq->cv_date)); ?> - <?php echo $liq->num; ?> <p><small><?php echo $liq->notes; ?></small> </p>

  </div>
<?php } ?>
  <div class="list-group-item active"><strong>TOTAL</strong> <span class="badge"><?php echo number_format($total,2); ?></span></div>
<?php if( ($adv->adv_amount-$total) > 0 ) { ?>
  <div class="list-group-item active" style="background-color:#DA6C6C"><strong>UNLIQUIDATED BALANCE</strong> <span class="badge"><?php echo number_format(($adv->adv_amount-$total),2); ?></span></div>
<?php } ?>
</div>
</div>
<?php } ?>


<?php if( ($adv->status == 0) && ( $this->input->get('edit_item') ) ) { ?>
 <div class="panel panel-default">
        <div class="panel-heading">
        <a href="<?php echo site_url(uri_string()); ?>?next=<?php echo $this->input->get("next"); ?>&amp;delete_item=<?php echo $liq->id; ?>" class="btn btn-danger btn-xs pull-right confirm" style="margin-left:10px"><span class="glyphicon glyphicon-remove"></span> Delete Item</a>
          <h3 class="panel-title">Edit Item </h3>
        </div>
<form method="post">
<input type="hidden" name="action" value="edit_item">
        <div class="panel-body">
<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>CV #</label>
          <input class="form-control text-center" name="cv_id" value="<?php echo $current_item->cv_id; ?>" required>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>CV Date</label>
          <input class="form-control text-center datepicker" name="cv_date" value="<?php echo date("m/d/Y", strtotime($current_item->cv_date)); ?>"  required>
        </div>
</div>
</div>
<div class="row">
<div class="col-md-6">
          <div class="form-group">
          <label>Number</label>
          <input class="form-control text-center" name="number" value="<?php echo $current_item->num; ?>">
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Amount</label>
          <input class="form-control text-center" name="amount" value="<?php echo number_format($current_item->amount,2); ?>"  required>
        </div>
</div>
</div>

          <div class="form-group">
          <label>Notes</label>
          <textarea class="form-control" name="notes"><?php echo $current_item->notes; ?></textarea>
        </div>

        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo site_url(uri_string()) . "?next=" . $this->input->get('next'); ?>" class="btn btn-warning pull-right">Cancel</a>
        </div>
      </form>
      </div>
<?php } ?>

      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>