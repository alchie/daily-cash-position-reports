<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Disbursement : <?php echo $current_account->bank_name; ?> (<?php echo $current_account->account_number; ?>)
<!-- Single button -->
<div class="btn-group pull-right">
  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Switch Account <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
<?php if( isset($disposables) ) { ?>
<?php foreach($disposables as $disp) { ?>
            <li><a href="<?php echo site_url("bank_accounts/add_disbursement/".$current_month."/".$current_day."/".$current_year."/".$disp->id); ?>"><?php echo $disp->bank_name; ?> (<?php echo $disp->account_number; ?>)</a></li>
<?php } ?>
<?php } ?>
  </ul>
</div>
        </div>
        <div class="panel-body">

        <div class="form-group">
          <label>Amount</label>
          <input type="hidden" name="bankid" value="" id="addDisbursementModal-bankid">
          <input type="text" name="amount" class="form-control text-right" value="0.00" id="addDisbursementModal-amount">
        </div>
        <div class="form-group">
          <label>Description</label>
          <input type="text" name="description" class="form-control" value="" id="addDisbursementModal-description">
        </div>

        <div class="form-group" id="addDisbursementModal-type">
          <label>Deposit Type</label>
          <label><input type="radio" name="type" value="check" checked class="addDisbursementModal-type" id="addDisbursementModal-type-check"> Check</label>
          <label><input type="radio" name="type" value="fund_transfer" class="addDisbursementModal-type" id="addDisbursementModal-type-fund_transfer"> Fund Transfer</label>
          <label><input type="radio" name="type" value="adj" id="addDisbursementModal-type-adj"> Adjustment</label>
        </div>
        <div class="form-group" id="addDisbursementModal-write_check_display">
            <label><input type="checkbox" name="write_check" value="1"> Write Check</label>
        </div>
        <div class="form-group" id="addDisbursementModal-banks" style="display:none;">
        <select class="form-control" name="source_id">
            <option value="">- - Select a Destination - -</option>
          <?php foreach($funds as $fund) { ?>
              <?php if( count( $fund->bank_accounts ) ) { ?>
                <optgroup label="<?php echo $fund->name; ?>">
                <?php foreach( $fund->bank_accounts as $bank_account ) { ?>
                <option value="<?php echo $bank_account->id; ?>"><?php echo $bank_account->bank_name; ?>(<?php echo $bank_account->account_number; ?>)</option>
              <?php } ?>
                </optgroup>
              <?php } ?>
          <?php } ?>
        </select>
        </div>

      </div>
    </div>
</div>
</div>
</div>
<?php $this->load->view('footer'); ?>