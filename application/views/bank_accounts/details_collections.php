<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$type = array('cash'=>'Cash','check'=>'Check', 'fund_transfer'=>'Fund Transfer', 'adj'=>'Adjustment');
?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

        <a style="margin-left:5px;" href="<?php echo site_url($this->input->get('next')); ?>" class="btn btn-warning btn-xs pull-right">Back</a>

          <h3 class="panel-title pull-left"><?php echo $collection; ?> COLLECTIONS</h3>

          <center>
          <form method="get">
<?php if($this->input->get('type')) { ?>
  <input type="hidden" name="type" value="<?php echo $this->input->get('type'); ?>">
<?php } ?>
<?php if($this->input->get('company')) { ?>
  <input type="hidden" name="company" value="<?php echo $this->input->get('company'); ?>">
<?php } ?>
          <table>
            <tr>
            <td>
            <div class="btn-group" role="group">
<?php 
$prev_start = ($this->input->get('start')) ? urldecode($this->input->get('start')) : date('m/d/Y'); 
$prev_start = date("m/d/Y", strtotime($prev_start . "-1 day"));

$prev_end = ($this->input->get('end')) ? urldecode($this->input->get('end')) : date('m/d/Y'); 
$prev_end = date("m/d/Y", strtotime($prev_end . "-1 day"));
?>
              <a class="btn btn-default btn-sm" href="<?php echo site_url(uri_string()); ?>?q=<?php echo urlencode($this->input->get('q')); ?>&amp;start=<?php echo urlencode($prev_start); ?>&amp;end=<?php echo urlencode($prev_end); ?>&amp;next=<?php echo urlencode($this->input->get('next')); ?>"><span class="glyphicon glyphicon-chevron-left"></span></a>
<?php 
$next_start = ($this->input->get('start')) ? urldecode($this->input->get('start')) : date('m/d/Y'); 
$next_start = date("m/d/Y", strtotime($next_start . "+1 day"));

$next_end = ($this->input->get('end')) ? urldecode($this->input->get('end')) : date('m/d/Y'); 
$next_end = date("m/d/Y", strtotime($next_end . "+1 day"));
?>
              <a class="btn btn-default  btn-sm" href="<?php echo site_url(uri_string()); ?>?q=<?php echo urlencode($this->input->get('q')); ?>&amp;start=<?php echo urlencode($next_start); ?>&amp;end=<?php echo urlencode($next_end); ?>&amp;next=<?php echo urlencode($this->input->get('next')); ?>"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            </td>
                            <td><input value="<?php echo $this->input->get('q'); ?>" name="q" type="text" class="form-control input-sm text-center" placeholder="Search for..."></td>
              <td><input value="<?php echo ($this->input->get('start')) ? $this->input->get('start') : date("m/d/Y"); ?>" name="start" type="text" class="datepicker form-control input-sm text-center" placeholder="date start"></td>
              
              <td><input value="<?php echo ($this->input->get('end')) ? $this->input->get('end') : date("m/d/Y"); ?>" name="end" type="text" class="datepicker form-control input-sm text-center" placeholder="date end"></td>
              
              <td><input type="submit" class="form-control input-sm" value="Submit"></td>

            </tr>
          </table>
          <input type="hidden" name="next" value="<?php echo $this->input->get('next'); ?>">
          </form>
          </center>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">


      <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th width="120px">Type</th>
                  <th>Date</th>
                  <th>Description</th>
                  <th class="text-right" width="20%">Deposited To</th>
                  <th class="text-right" width="20%">Debit</th>
                  <th class="text-right" width="20%">Credit</th>
                  <th class="text-right" width="20%">Amount</th>
                  
                  <th width="1px"></th>
                </tr>
              </thead>
              <tbody>
<?php 
$total_debits = 0;
$total_credits = 0;
$total = ($bank_account->beg_deposit-$bank_account->beg_disbursement);
?>
              <tr class="warning">
                <td colspan="6">Beginning Balance</td>
                <td></td>
                <td class="text-right"><strong><?php echo number_format(($bank_account->beg_deposit-$bank_account->beg_disbursement),2); ?></strong></td>
              </tr>
<?php if( $items ) { ?>
              <?php 
              foreach( $items as $item ) { 
                $item_type = ($item->item_type=='dep') ? 'deposit' : 'disbursement';
if($item->item_type=='dep') {
  $total_debits += $item->amount;
}
if($item->item_type=='dis') {
  $total_credits += $item->amount;
}
                ?>
                <tr>
                  <td>


                    <?php echo ($item->type) ? $type[$item->type] : '-'; ?>


                    </td>
                  <td><a href="<?php echo site_url("reports/view/" . date('m', strtotime($item->report_date)) . "/" . date('d', strtotime($item->report_date)) . "/" . date('Y', strtotime($item->report_date)) ); ?>"><?php echo date('m/d/Y', strtotime($item->report_date)); ?></a></td>
                  <td class="small"><?php echo $item->description; ?></td>
                  <td class="text-right"><?php echo $item->deposited_to; ?></td>
                  <td class="text-right"><?php echo ($item->item_type=='dep') ? number_format($item->amount,2) : ''; ?></td>
                  <td class="text-right">

                    <?php echo ($item->item_type=='dis') ? number_format($item->amount,2) : ''; ?>

                  
                    
                  </td>
                  <td class="text-right"><?php 
                  if($item->item_type=='dep') {
                      $total += $item->amount;
                  } else {
                      $total -= $item->amount;
                  }
                  echo number_format($total,2); 
                  ?></td>
                  <td class="text-right">
                    <a href="<?php echo site_url("bank_accounts/edit_item/{$item_type}/{$item->id}"); ?>?start=<?php echo urlencode($this->input->get('start')); ?>&end=<?php echo urlencode($this->input->get('end')); ?>&next=<?php echo $this->input->get('next'); ?>&back=<?php echo uri_string(); ?>&company=<?php echo $this->input->get('company'); ?>" class="btn btn-xs btn-success">Edit</a>
                  </td>
                </tr>
              <?php } ?>

<?php } else { ?>
<tr>
  <td colspan="8"><br><br><br><br><p class="text-center">No Transactions</p><br><br><br><br></td>
</tr>
<?php } ?>
              <tr class="warning">
                <td colspan="3">Ending Balance</td>
                <td></td>
                <td class="text-right bold"><?php echo number_format($total_debits,2); ?></td>
                <td class="text-right bold"><?php echo number_format($total_credits,2); ?></td>
                <td></td>
                
                <td class="text-right"><strong><?php echo number_format(($bank_account->end_deposit-$bank_account->end_disbursement),2); ?></strong></td>
              </tr>
              </tbody>
              </table>

        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>