<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$type = array('cash'=>'Cash','check'=>'Check', 'fund_transfer'=>'Fund Transfer', 'adj'=>'Adjustment');
?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">
        <a style="margin-left:5px;" href="<?php echo site_url($this->input->get('next')); ?>" class="btn btn-warning btn-xs pull-right">Back</a>
          <a href="<?php echo site_url("bank_accounts/details/{$bank_account->id}/disbursements"); ?>?start=<?php echo urlencode($this->input->get('start')); ?>&end=<?php echo urlencode($this->input->get('end')); ?>&next=<?php echo urlencode($this->input->get('next')); ?>" class="btn btn-success btn-xs pull-right">Disbursements</a>

          <h3 class="panel-title pull-left"><?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) Deposits</h3>

          <center>
          <form method="get">
          
          <table>
            <tr>
              <td><input value="<?php echo ($this->input->get('start')) ? $this->input->get('start') : date("m/d/Y"); ?>" name="start" type="text" class="datepicker form-control input-sm text-center" placeholder="date start"></td>
              <td><input value="<?php echo ($this->input->get('end')) ? $this->input->get('end') : date("m/d/Y"); ?>" name="end" type="text" class="datepicker form-control input-sm text-center" placeholder="date end"></td>
              <td><input type="submit" class="form-control input-sm" value="Submit"></td>
            </tr>
          </table>
          <input type="hidden" name="next" value="<?php echo $this->input->get('next'); ?>">
          </form>
          </center>
          <div class="clearfix"></div>

        </div>
        <div class="panel-body">

<?php if( $items ) { ?>
        <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Description</th>
                  <th class="text-right">Amount</th>
                  <th width="1px"></th>
                </tr>
              </thead>
              <tbody>
              <?php 
              $total = 0;
              foreach( $items as $item ) { ?>
                <tr>
                  <td><?php echo ($item->type) ? $type[$item->type] : ''; ?></td>
                  <td><a href="<?php echo site_url("reports/view/" . date('m', strtotime($item->report_date)) . "/" . date('d', strtotime($item->report_date)) . "/" . date('Y', strtotime($item->report_date)) ); ?>"><?php echo date('m/d/Y', strtotime($item->report_date)); ?></a></td>
                  <td><?php echo $item->description; ?></td>
                  <td class="text-right"><?php echo number_format($item->amount,2); $total+=$item->amount; ?></td>
                  <td class="text-right">
                    <a href="<?php echo site_url("bank_accounts/edit_item/deposit/{$item->id}"); ?>?start=<?php echo urlencode($this->input->get('start')); ?>&end=<?php echo urlencode($this->input->get('end')); ?>&next=<?php echo $this->input->get('next'); ?>&back=<?php echo uri_string(); ?>" class="btn btn-xs btn-warning">Edit</a>
                  </td>
                </tr>
              <?php } ?>
              <tr class="success">
                  <td colspan="3" class="text-right bold">TOTAL</td>
                  <td class="text-right bold"><?php echo number_format($total,2); ?></td>
                  <td></td>
                </tr>
              </tbody>
              </table>

              <center><?php echo $pagination; ?></center>
<?php } else { ?>
<p class="text-center">No Transactions</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>