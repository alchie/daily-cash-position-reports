<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Edit Bank Account</h3>
        </div>
      <form method="post">
        <div class="panel-body">
<div class="row">
<div class="col-md-5">
        <div class="form-group">
          <label>Bank Name</label>
          <input class="form-control" name="bank_name" value="<?php echo $bank_account->bank_name; ?>">
        </div>
</div>
<div class="col-md-5">
          <div class="form-group">
          <label>Branch Name</label>
          <input class="form-control" name="branch" value="<?php echo $bank_account->branch; ?>">
        </div>
</div>
<div class="col-md-2">
          <div class="form-group">
          <label>Short Name</label>
          <input class="form-control" name="short_name" value="<?php echo $bank_account->short_name; ?>">
        </div>
</div>
</div>
<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Account Number</label>
          <input class="form-control" name="account_number" value="<?php echo $bank_account->account_number; ?>">
        </div>
</div>
<div class="col-md-6">

          <div class="form-group">
          <label>Account Type</label>
          <select name="acct_type" class="form-control">
            <option value="">- - Select a Account Type - -</option>
<?php foreach(array(
            'checking' => 'Checking',
            'saving' => 'Savings',
            'time_deposit' => 'Time Deposit',
            'investment' => 'Investment',
            ) as $type_id=>$type_name) { ?>
            <option value="<?php echo $type_id; ?>" <?php echo ($bank_account->acct_type==$type_id) ? "SELECTED":""; ?>><?php echo $type_name; ?></option>
          <?php } ?>
         
          </select>
        </div>

</div>
</div>
<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Fund Category</label>
          <select class="form-control" name="fund_category">
            <option value="disposable" <?php echo ($bank_account->fund_category=='disposable') ? 'selected' : ''; ?>>Disposable Fund</option>
            <option value="time_deposit" <?php echo ($bank_account->fund_category=='time_deposit') ? 'selected' : ''; ?>>Time Deposit</option>
            <option value="dollar" <?php echo ($bank_account->fund_category=='dollar') ? 'selected' : ''; ?>>Dollar Account</option>
            <option value="long_term" <?php echo ($bank_account->fund_category=='long_term') ? 'selected' : ''; ?>>Long-term Investment</option>
          </select>
        </div>
</div>
<div class="col-md-6">
        <div class="form-group">
          <label>Priority</label>
          <input class="form-control" name="priority" value="<?php echo ($bank_account->priority) ? $bank_account->priority : 0; ?>">
        </div>
</div>
</div>
<div class="row">
  <div class="col-md-6">
        <div class="form-group">
          <label>Fund</label>
          <select class="form-control" name="fund_id">
            <?php foreach($funds_data as $fund) { ?>
              <option value="<?php echo $fund->id; ?>" <?php echo ($bank_account->fund_id==$fund->id) ? 'selected' : ''; ?>><?php echo $fund->name; ?> (<?php echo $fund->group_name; ?>)</option>
            <?php } ?>
          </select>
        </div>
</div>
  <div class="col-md-6">
        <div class="form-group">
          <label>Class</label>
          <select name="class" class="form-control">
            <option value="">- - Select a class - -</option>
           <?php foreach($classes as $class) { ?>
            <option value="<?php echo $class->id; ?>" <?php echo ($bank_account->class==$class->id) ? "SELECTED":""; ?>><?php echo $class->name; ?></option>
          <?php } ?>
          </select>
        </div>
</div>
</div>

        <div class="form-group">
          <label>Memo</label>
          <textarea class="form-control" name="memo"><?php echo $bank_account->memo; ?></textarea>
        </div>

        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo site_url("funds"); ?>" class="btn btn-warning">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>