<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$fund_categories = array(
'disposable' => 'Disposable',
'time_deposit' => 'Time Deposit',
'dollar' => 'Dollar',
'long_term' => 'Long Term',
  ); 

?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
        <a href="<?php echo $delete_item_url; ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
          <h3 class="panel-title">Edit <?php echo ( $item->bank_id==0 ) ? 'Collection' : 'Deposit'; ?> Item</h3>
        </div>
      <form method="post">
        <div class="panel-body">
<?php if( $item->bank_id ) { ?>
          <div class="form-group">
            <label>Bank Account</label>
            <select class="form-control" name="bank_id">
                <?php foreach($funds as $fund) { ?>
                  <optgroup label="<?php echo $fund->name; ?>">
                    <?php foreach($fund->bank_accounts as $bank) { print_r( $bank ); ?>
                      <option value="<?php echo $bank->id; ?>" <?php echo ($bank->id==$item->bank_id) ? 'SELECTED' : ''; ?>><?php echo $bank->bank_name; ?> (<?php echo $bank->account_number; ?>) - <?php echo $fund_categories[$bank->fund_category]; ?></option>
                    <?php } ?>
                  </optgroup>
                <?php } ?>
            </select>
          </div>
<?php } ?>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Date</label>
                <input class="form-control datepicker" name="date" value="<?php echo date('m/d/Y', strtotime($item->report_date)); ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Amount</label>
                <input class="form-control text-right" name="amount" value="<?php echo number_format($item->amount,2); ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Type</label>
                <select name="type" class="form-control">
                  <option type="">- No Type -</option>
                  <?php foreach(array('cash'=>'Cash','check'=>'Check', 'fund_transfer'=>'Fund Transfer', 'adj'=>'Adjustment') as $type_id=>$type_name) { ?>
                    <option value="<?php echo $type_id; ?>" <?php echo ($item->type==$type_id) ? 'SELECTED' : ''; ?>><?php echo $type_name; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Company</label>
                <select name="company" class="form-control">
                  <?php foreach(array(
                    'rcbdi'=>'The Roman Catholic Bishop of Davao, Inc.',
                    'adsai'=>'Archdiocesan Stewardship Apostolates, Inc.') as $company_id=>$company_name) { ?>
                    <option value="<?php echo $company_id; ?>" <?php echo ($item->company==$company_id) ? 'SELECTED' : ''; ?>><?php echo $company_name; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Description</label>
            <input class="form-control" name="description" value="<?php echo $item->description; ?>">
          </div>
        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Update">
          <a class="btn btn-warning" href="<?php echo $redirect_url; ?>">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>