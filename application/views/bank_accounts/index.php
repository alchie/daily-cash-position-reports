<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">
          <a href="<?php echo site_url("funds/add"); ?>" class="btn btn-success btn-xs pull-right">Add Fund</a>
          <h3 class="panel-title">Funds</h3>
        </div>
        <div class="panel-body">
            <table class="table table-default">
              <thead>
                <tr>
                  <th>Fund Name</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach( $funds as $fund ) { ?>
                <tr>
                  <td><?php echo $fund->name; ?></td>
                  <td>

                  <a href="<?php echo site_url("bank_accounts/add/". $fund->id); ?>" class="btn btn-success btn-xs">Add Account</a>
                  <a href="<?php echo site_url("funds/edit/". $fund->id); ?>" class="btn btn-warning btn-xs">Edit</a>
                  <a href="<?php echo site_url("funds/delete/". $fund->id); ?>" class="btn btn-danger btn-xs confirm">Delete</a>

                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>