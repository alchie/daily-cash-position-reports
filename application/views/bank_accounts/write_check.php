<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">    
      <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">

<?php if( date('Y-m-d') != "{$current_year}-{$current_month}-{$current_day}") { ?>
<div class="alert alert-danger" role="alert"><strong>WARNING! </strong> Please be aware of the DATE (<strong><?php echo date('F d, Y', strtotime("{$current_month}/{$current_day}/{$current_year}")); ?></strong>)  you will be using!</div>
<?php } ?>
        <div class="panel panel-warning">
        <div class="panel-heading">
          <h3 class="panel-title">Write Check : <strong><?php echo $current_account->bank_name; ?> (<?php echo $current_account->account_number; ?>) <span class="badge"><?php echo $current_account->fund_name; ?></span> <a href="<?php echo site_url("welcome/index/{$current_month}/{$current_year}"); ?>" class="badge"><?php echo date('F d, Y', strtotime("{$current_month}/{$current_day}/{$current_year}")); ?></a></strong>
<!-- Single button -->
<div class="btn-group pull-right">
  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Switch Account <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" style="min-width:400px">
<?php if( isset($disposables) ) { ?>
<?php foreach($disposables as $disp) { 
  if($current_account->id == $disp->id) {
    continue;
  }
  ?>
            <li><a href="<?php echo site_url("bank_accounts/write_check/".$current_month."/".$current_day."/".$current_year."/".$disp->id); ?>">
<span class="badge pull-right"><?php echo $disp->fund_name; ?></span>
            <?php echo $disp->bank_name; ?> (<?php echo $disp->account_number; ?>)</a></li>
<?php } ?>
<?php } ?>
  </ul>
</div>
        </div>
<form method="post">
        <div class="panel-body">

<div class="row">
<div class="col-md-3">
        <div class="form-group">
          <label>Voucher #</label>
          <input class="form-control text-center" name="voucher_number" required="required" />
        </div>
</div>
<div class="col-md-9">
          <div class="form-group">
            <small><a href="<?php echo site_url("payee/add") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Payee</a></small>
          <label>Payee</label>
          <select class="form-control" name="payee_id" required="">
            <option value="">- - SELECT PAYEE - -</option>
            <?php foreach($payees as $payee) { ?>
              <option value="<?php echo $payee->id; ?>" <?php echo (($this->input->get('payee_id')) && ($this->input->get('payee_id')==$payee->id)) ? 'SELECTED' : ''; ?>><?php echo $payee->payee; ?></option>
            <?php } ?>
          </select>
        </div>
</div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
        <small><a href="<?php echo site_url("checks/add/{$current_account->id}") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Register Checkbook</a></small>
          <label>Check Number</label>
          <select class="form-control" name="check_number">
            <?php foreach($check_items as $item) { ?>
              <option value="<?php echo $item->check_number; ?>"><?php echo $item->check_number; ?></option>
            <?php } ?>
          </select>
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
          <label>Amount</label>
          <input type="text" name="amount" class="form-control text-right" value="0.00" id="addDisbursementModal-amount">
        </div>
  </div>
</div>

        <div class="form-group">
          <label>Description</label>
          <input type="text" name="description" class="form-control" value="" id="addDisbursementModal-description">
        </div>

<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Signatory 1</label>
          <select class="form-control" name="signatory1">
            <?php foreach($signatories1 as $sig) { ?>
              <option value="<?php echo $sig->id; ?>"><?php echo $sig->signatory; ?></option>
            <?php } ?>
            <option value="0">- - Blank - -</option>
          </select>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Signatory 2</label>
          <select class="form-control" name="signatory2">
            <?php foreach($signatories2 as $sig) { ?>
              <option value="<?php echo $sig->id; ?>"><?php echo $sig->signatory; ?></option>
            <?php } ?>
            <option value="0">- - Blank - -</option>
          </select>
        </div>
</div>
</div>

      </div>
      <div class="panel-footer">

      <button type="submit" class="btn btn-success">Submit</button>

      </div>
    </div>
    </form>

<?php if( $latest ) { ?>
        <div class="panel panel-default">
        <div class="panel-heading">
<a class="pull-right btn btn-default btn-xs" href="<?php echo site_url( str_replace('write_check', 'checks_written', uri_string()) ); ?>">Show All</a>
        Last 10 Written Checks</div>
        
          <div class="list-group">
<?php foreach($latest as $lat) { ?>
            <div class="list-group-item">
                <div class="btn-group pull-right">
                  <a href="<?php echo site_url("checks/print_check/{$lat->check_id}/{$lat->db_id}"); ?>" class="btn btn-warning btn-xs">Print Check</a>
                  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url("bank_accounts/edit_item/disbursement/{$lat->db_id}"); ?>?back=<?php echo uri_string(); ?>">Edit Disbursement</a></li>
                    <li><a href="<?php echo site_url("checks/write/{$lat->db_id}"); ?>?back=<?php echo uri_string(); ?>">Edit Check</a></li>
                  </ul>
                </div>
            <strong>#<?php echo $lat->check_number; ?> - <?php echo number_format($lat->amount,2); ?></strong> <small>- <?php echo $lat->description; ?></small>
            </div>
<?php } ?>
          </div>
        </div>
<?php } ?>
</div>
</div>
</div>
<?php $this->load->view('footer'); ?>