<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

        <a href="<?php echo site_url("check_templates/add"); ?>" class="btn btn-success btn-xs pull-right">Add Template</a>

          <h3 class="panel-title"><strong>Check templates</strong></h3>

        </div>
        <div class="panel-body">
<?php if( $templates ) { ?>

      <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Template Name</th>
                  <th width="110px">Actions</th>
                </tr>
              </thead>
              <tbody>
<?php foreach($templates as $template) { ?>
<tr class="<?php echo ($template->default) ? 'warning' : ''; ?>">
  <td class="text-left"><?php echo $template->name; ?></td>
  <td class="text-left">
    <a href="<?php echo site_url("check_templates/edit/{$template->id}"); ?>" class="btn btn-success btn-xs">Edit</a>
    <a href="<?php echo site_url("check_templates/delete/{$template->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
  </td>
</tr>
<?php } ?>
              </tbody>
              </table>

<?php echo $pagination; ?>

<?php } else { ?>
<p class="text-center">No Entries</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>