<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Template</h3>
        </div>
      <form method="post">
        <div class="panel-body">
<div class="row">
<div class="col-md-12">
        <div class="form-group">
          <label>Template Name</label>
          <input class="form-control" name="name" />
        </div>
</div>
</div>

        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo site_url("signatory"); ?>" class="btn btn-warning">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>