<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$settings = json_decode($template->settings);
?>
    <div class="container">
<form method="post">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        
        <div class="panel panel-default">
        <div class="panel-heading">
          <div class="btn-group pull-right">
          <button class="btn btn-success btn-xs" type="submit"> <span class="glyphicon glyphicon-save"></span> Save</button>
          <?php if( $template->default == '0') { ?>
            <a href="<?php echo site_url("check_templates/set_as_default/{$template->id}"); ?>" class="btn btn-warning btn-xs">Set as Default</a>
          <?php } ?>
        </div>
          <h3 class="panel-title">Edit Templates</h3>
        </div>
      
        <div class="panel-body">

        <div class="form-group">
          <label class="pull-right">
            <input type="checkbox" name="active" value="1" <?php echo ($template->active) ? 'CHECKED' : ''; ?>> Active
          </label>
          <label>Template Name</label>
          <input class="form-control" name="name" value="<?php echo $template->name; ?>" />
        </div>

<div class="row">
  <div class="col-md-6">


        <div class="form-group">
          <label>Payee Class (.payee) CSS</label>
          <textarea class="form-control" name="settings[payee]"><?php echo (isset($settings->payee)) ? $settings->payee : ''; ?></textarea>
        </div>

        <div class="form-group">
          <label>Date Class (.date) CSS</label>
          <textarea class="form-control" name="settings[date]"><?php echo (isset($settings->date)) ? $settings->date : ''; ?></textarea>
        </div>

        <div class="form-group">
          <label>Amount Class (.amount) CSS</label>
          <textarea class="form-control" name="settings[amount]"><?php echo (isset($settings->amount)) ? $settings->amount : ''; ?></textarea>
        </div>

        <div class="form-group">
          <label>Amount in Words Class (.amount-words) CSS</label>
          <textarea class="form-control" name="settings[amount_words]"><?php echo (isset($settings->amount_words)) ? $settings->amount_words : ''; ?></textarea>
        </div>



        <div class="form-group">
          <label>Signatory 1 Class (.signatory1) CSS</label>
          <textarea class="form-control" name="settings[signatory1]"><?php echo (isset($settings->signatory1)) ? $settings->signatory1 : ''; ?></textarea>
        </div>

        <div class="form-group">
          <label>Signatory 2 Class (.signatory2) CSS</label>
          <textarea class="form-control" name="settings[signatory2]"><?php echo (isset($settings->signatory2)) ? $settings->signatory2 : ''; ?></textarea>
        </div>
</div>
<div class="col-md-6">
        <div class="form-group">
          <label>Detail Class (.detail) CSS</label>
          <textarea class="form-control" name="settings[detail]"><?php echo (isset($settings->detail)) ? $settings->detail : ''; ?></textarea>
        </div>
          <div class="form-group">
          <label>Signatories Class (.signatory) CSS</label>
          <textarea class="form-control" name="settings[signatory]"><?php echo (isset($settings->signatory)) ? $settings->signatory : ''; ?></textarea>
        </div>

  <div class="form-group">
          <label>Instructions</label>
          <textarea rows="16" class="form-control" name="settings[instructions]"><?php echo (isset($settings->instructions)) ? $settings->instructions : ''; ?></textarea>
        </div>
  </div>
</div>


        </div>
      </div>


      </div>
    </div>
      </form>
</div>
<?php $this->load->view('footer'); ?>