<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

<div class="pull-right col-md-4">
   <form method="get">
    <div class="input-group input-group-sm">
      <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-search"></span></span>
      <input name="q" type="text" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
      <span class="input-group-btn">
                <a href="<?php echo site_url("checks/add/{$bank_account->id}"); ?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span> Add Checkbook</a>
      </span>
    </div><!-- /input-group -->
  </form>
</div>


          <h3 class="panel-title pull-left">

<div class="btn-group pull-left" style="margin-right: 10px;">
  <button type="button" class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
<?php 
$previous_fund = 0;
foreach($checking_accounts as $cacct) { ?>

<?php if( ($previous_fund) && ($previous_fund!=$cacct->fund_id) ) { ?>
<li role="separator" class="divider"></li>
<?php } ?>
    <li><a style="width: 400px;" href="<?php echo site_url("checks/index/{$cacct->id}"); ?>?amount=<?php echo $this->input->get('amount'); ?>"><?php echo $cacct->bank_name; ?> (<?php echo $cacct->account_number; ?>) <span class="badge pull-right"><?php echo $cacct->fund_name; ?></span></a></li>
<?php 
$previous_fund = $cacct->fund_id;
} ?>
  </ul>
</div>

            <strong><?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>)</strong><br>
<span class="badge"><?php echo $bank_account->fund_name; ?></span>
          </h3>
<center>
  <div class="btn-group">
<a class="btn btn-<?php echo ($filter=='assigned') ? 'success' : 'default'; ?> btn-xs" href="<?php echo site_url(uri_string()) . "?filter=assigned"; ?>">Assigned (<?php echo $bank_account->total_assigned; ?>)</a>
<a class="btn btn-<?php echo ($filter=='unassigned') ? 'success' : 'default'; ?> btn-xs" href="<?php echo site_url(uri_string()) . "?filter=unassigned"; ?>">Unassigned (<?php echo $bank_account->total_unassigned; ?>)</a>
<a class="btn btn-<?php echo ($filter=='all') ? 'success' : 'default'; ?> btn-xs" href="<?php echo site_url(uri_string()) . "?filter=all"; ?>">All Checks (<?php echo $bank_account->total_checks; ?>)</a>
</div>
</center>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
<?php if( $check_items ) { ?>
      <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Check Number</th>
                  <th>Date</th>
                  <th>Payee</th>
                  <th class="text-right">Amount</th>
                  <th width="50px;">Action</th>
                </tr>
              </thead>
              <tbody>
<?php foreach($check_items as $item) { ?>
<tr class="<?php echo ($item->voided) ? 'danger' : ''; ?>">
  <td class="text-left"><?php echo $item->check_number; ?></td>
  <td class="text-left"><?php echo $item->check_date; ?></td>
  <td class="text-left"><?php echo $item->payee_name; ?></td>
  <td class="text-right"><?php echo number_format($item->amount,2); ?></td>
  <td class="text-left">
<?php if($item->db_id) { ?>
    <?php if($item->rd_id) { ?>
      <a href="<?php echo site_url("checks/write/{$item->db_id}"); ?>" class="btn btn-warning btn-block btn-xs">Edit</a>
    <?php } else { ?>
      <a href="<?php echo site_url("checks/fix/{$item->id}"); ?>?next=<?php echo uri_string(); ?>" class="btn btn-danger btn-xs">Error (Fix)</a>
    <?php } ?>
<?php } else { ?>
    <a href="<?php echo site_url("checks/delete/{$item->id}"); ?>?next=<?php echo uri_string(); ?>" class="btn btn-danger btn-block btn-xs">Delete</a>
<?php } ?>
  </td>
</tr>
<?php } ?>
              </tbody>
              </table>

<?php echo $pagination; ?>

<?php } else { ?>
<p class="text-center">No Transactions</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>