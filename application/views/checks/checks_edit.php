<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Disbursement <strong class="pull-right"><?php echo $db_data->bank_name; ?> (<?php echo $db_data->account_number; ?>)</strong></h3>
        </div>
        <div class="panel-body">
<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Check Date</label>
          <div class="form-control text-center"><?php echo date('F d, Y', strtotime($db_data->report_date)); ?></div>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Amount</label>
          <div class="form-control text-center"><?php echo number_format($db_data->amount,2); ?></div>
        </div>
</div>
</div>

<div class="form-group">
          <label>Description</label>
          <div class="form-control"><?php echo $db_data->description; ?></div>
</div>

</div>
</div>


<form method="post">
        <div class="panel panel-success">
        <div class="panel-heading">
          <div class="btn-group pull-right">
          <input type="submit" value="Update" class="btn btn-success btn-xs">
          <a href="<?php echo site_url("checks/write/{$current_check->db_id}"); ?>" class="btn btn-warning btn-xs">Back</a>
          <a target="check_<?php echo $current_check->id; ?>" href="<?php echo site_url("checks/print_check/{$current_check->id}/{$current_check->db_id}"); ?>" class="btn btn-primary btn-xs">Print Check</a>
        </div>
          <h3 class="panel-title">Update Check</h3>
        </div>
        <div class="panel-body">
<div class="row">
<div class="col-md-2">
        <div class="form-group">
          <label>Voucher #</label>
          <input class="form-control text-center" name="voucher_number" required="required" value="<?php echo $current_check->cv_id; ?>" />
        </div>
</div>
<div class="col-md-3">
        <div class="form-group">
          <label>Check Number</label>
          <div class="form-control text-center"><?php echo $current_check->check_number; ?></div>
        </div>
</div>
<?php $current_payee = ($this->input->get('payee_id')) ? $this->input->get('payee_id') : $current_check->payee_id; ?>
<div class="col-md-7">
          <div class="form-group">
            <small><a href="<?php echo site_url("payee/add") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Payee</a></small>
          <label>Payee</label>
          <select class="form-control" name="payee_id" required="">
            <option value="">- - SELECT PAYEE - -</option>
            <?php foreach($payees as $payee) { ?>
              <option value="<?php echo $payee->id; ?>" <?php echo ($current_payee==$payee->id) ? 'SELECTED' : ''; ?>><?php echo $payee->payee; ?></option>
            <?php } ?>
          </select>
        </div>
</div>
</div>

<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Signatory 1</label>
          <select class="form-control" name="signatory1">
            <?php foreach($signatories1 as $sig) { ?>
              <option value="<?php echo $sig->id; ?>" <?php echo ($current_check->signatory1==$sig->id) ? 'SELECTED' : ''; ?>><?php echo $sig->signatory; ?></option>
            <?php } ?>
            <option value="0" <?php echo ($current_check->signatory1==0) ? 'SELECTED' : ''; ?>>- - Blank - -</option>
          </select>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Signatory 2</label>
          <select class="form-control" name="signatory2">
            <?php foreach($signatories2 as $sig) { ?>
              <option value="<?php echo $sig->id; ?>" <?php echo ($current_check->signatory2==$sig->id) ? 'SELECTED' : ''; ?>><?php echo $sig->signatory; ?></option>
            <?php } ?>
            <option value="0" <?php echo ($current_check->signatory2==0) ? 'SELECTED' : ''; ?>>- - Blank - -</option>
          </select>
        </div>
</div>
</div>
 
        </div>
      </div>
     </form>




      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>