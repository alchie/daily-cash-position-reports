<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$settings = json_decode($template->settings);
//print_r( $check_data );
$bank_id = ($this->input->get('bank_id')) ? $this->input->get('bank_id') : $check_data->acct_id;
$date_url = date('m/d/Y', strtotime($check_data->check_date));
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Print Check</title>
<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    .detail {
      position: absolute;
      top: 0;
      left: 0;
    }
    .detail {<?php echo (isset($settings->detail)) ? $settings->detail : ''; ?>}
    .payee {<?php echo (isset($settings->payee)) ? $settings->payee : ''; ?>}
    .date {<?php echo (isset($settings->date)) ? $settings->date : ''; ?>}
    .amount {<?php echo (isset($settings->amount)) ? $settings->amount : ''; ?>}
    .amount-words {<?php echo (isset($settings->amount_words)) ? $settings->amount_words : ''; ?>}
    .signatory {<?php echo (isset($settings->signatory)) ? $settings->signatory : ''; ?>}
    .signatory-1 {<?php echo (isset($settings->signatory1)) ? $settings->signatory1 : ''; ?>}
    .signatory-2 {<?php echo (isset($settings->signatory2)) ? $settings->signatory2 : ''; ?>}
a[href]:after { content: none !important; } 
.hide-print {
   display: none;
}
</style>

<style type="text/css">
.print-topnav {
    position: fixed;
    width: 500px;
    bottom: 0;
    right: 0;
    margin-top: 0;
    border: 1px solid #000;
    border-bottom: none;
    padding: 3px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: #CCC;
    font-size: 11px;
    z-index: 99;
    font-family: sans-serif;
    font-size: 14px;
    padding: 5px 10px;
     padding-bottom: 10px;
}
.print-topnav2 {
  bottom:28px;
  z-index: 98;
  padding-top:5px;
  padding-bottom: 15px;
}
.print-topnav3 {
  bottom:62px;
  z-index: 98;
  padding-top:10px;
  padding-bottom: 15px;
}
    .detail {
      position: absolute;
      top: 0;
      left: 0;
    }
    .detail {<?php echo (isset($settings->detail)) ? $settings->detail : ''; ?>}
    .payee {<?php echo (isset($settings->payee)) ? $settings->payee : ''; ?>}
    .date {<?php echo (isset($settings->date)) ? $settings->date : ''; ?>}
    .amount {<?php echo (isset($settings->amount)) ? $settings->amount : ''; ?>}
    .amount-words {<?php echo (isset($settings->amount_words)) ? $settings->amount_words : ''; ?>}
    .signatory {<?php echo (isset($settings->signatory)) ? $settings->signatory : ''; ?>}
    .signatory-1 {<?php echo (isset($settings->signatory1)) ? $settings->signatory1 : ''; ?>}
    .signatory-2 {<?php echo (isset($settings->signatory2)) ? $settings->signatory2 : ''; ?>}
</style>
</head>
<body>

<div class="print-topnav hide-print text-center allcaps">
<strong><a href="<?php echo site_url("bank_accounts/write_check/{$date_url}/{$bank_id}"); ?>">Back</a></strong> &middot;
Print Options 
<?php if( $templates ) { ?>
<select style="width: 220px;" onchange="this.options[this.selectedIndex].value &amp;&amp; (window.location = this.options[this.selectedIndex].value);">
<optgroup label="Check Templates">
<?php foreach( $templates as $temp ) { ?>
  <option <?php echo ($temp->id==$this->session->userdata('print_check_template')) ? 'SELECTED' : ''; ?> value="<?php echo site_url("checks/print_check/{$check_data->id}/{$check_data->db_id}") . "?template=" . $temp->id ; ?>&hidden_signatories=<?php echo ($this->input->get('hidden_signatories')) ? 1 : 0; ?>"><?php echo $temp->name; ?></option>
<?php } ?>
        </optgroup>
  </select>
<?php } ?>
 &middot; 
<?php if( $this->session->userdata('hidden_signatories') ) { ?>
<a href="<?php echo site_url("checks/print_check/{$check_data->id}/{$check_data->db_id}"); ?>?show_signatories=1">Show Signatories</a>
<?php } else { ?>
<a href="<?php echo site_url("checks/print_check/{$check_data->id}/{$check_data->db_id}"); ?>?hide_signatories=1">Hide Signatories</a>
<?php } ?>
  </div>
<?php if(isset($settings->instructions) && ($settings->instructions)) { ?>
<div class="print-topnav print-topnav3 hide-print text-center allcaps">
<strong>Template Instruction:</strong>
<br><?php echo $settings->instructions; ?>
</div>
<?php } ?>
<div class="print-topnav print-topnav2 hide-print text-center allcaps">
<strong>Other Checks:</strong>
<?php if( $latest ) { ?>
<select style="width: 180px;" onchange="this.options[this.selectedIndex].value &amp;&amp; (window.location = this.options[this.selectedIndex].value);">
<optgroup label="Latest Checks">
  <option value="#" DISABLED SELECTED> - - Select Check - -</option>
<?php foreach( $latest as $lat ) { ?>
  <option <?php echo ($lat->check_id==$check_data->id) ? 'SELECTED' : ''; ?> value="<?php echo site_url("checks/print_check/{$lat->check_id}/{$lat->db_id}"); ?>">#<?php echo $lat->check_number; ?> - <?php echo number_format($lat->amount,2); ?> - <?php echo $lat->description; ?></option>
<?php } ?>
        </optgroup>
  </select>
<?php } ?>

<?php if( $disposables ) { ?>
<select style="width: 180px;" onchange="this.options[this.selectedIndex].value &amp;&amp; (window.location = this.options[this.selectedIndex].value);">
<optgroup label="Bank Accounts">
<?php 

foreach( $disposables as $disposable ) { ?>
  <option <?php echo ($bank_id==$disposable->id) ? 'SELECTED' : ''; ?> value="<?php echo site_url(uri_string()); ?>?bank_id=<?php echo $disposable->id; ?>"><?php echo $disposable->bank_name; ?> (<?php echo $disposable->account_number; ?>)</option>
<?php } ?>
        </optgroup>
  </select>
<?php } ?>

</div>
             <div class="detail payee"><?php echo strtoupper($check_data->payee_name); ?></div>
             <div class="detail date"><?php echo date("F d, Y",  strtotime($check_data->check_date)); ?></div>
             <div class="detail amount"><?php echo number_format( $check_data->amount , 2 ); ?></div>
             <div class="detail amount-words"><?php echo strtoupper( number2word( number_format($check_data->amount,2) ) ); ?></div>
<?php if( !$this->session->userdata('hidden_signatories') ) { ?>
             <div class="detail signatory signatory-1"><?php echo $check_data->signatory1; ?></div>
             <div class="detail signatory signatory-2"><?php echo $check_data->signatory2; ?></div>
<?php } ?>
<script>
<!--
  //window.print();
-->
</script>         
</body>
</html>
