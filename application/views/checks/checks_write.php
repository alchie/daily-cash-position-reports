<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-md-6">
        <div class="panel panel-default">
        <div class="panel-heading">
<a href="<?php echo site_url("bank_accounts/edit_item/disbursement/{$db_data->id}"); ?>?back=<?php echo uri_string(); ?>" class="btn btn-warning btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
          <h3 class="panel-title">
          Disbursement : 
            <a href="<?php echo site_url("bank_accounts/details/{$db_data->bank_id}") . "?start=" . urlencode( date('m/d/Y', strtotime($db_data->report_date)) ) . "&end=" . urlencode( date('m/d/Y', strtotime($db_data->report_date)) ) ; ?>"><strong style="margin-right: 10px;"><?php echo $db_data->bank_name; ?> (<?php echo $db_data->account_number; ?>)</strong></a></h3>
        </div>
        <div class="panel-body">
<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Check Date</label>
          <div class="form-control text-center"><?php echo date('F d, Y', strtotime($db_data->report_date)); ?></div>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Amount</label>
          <div class="form-control text-center"><?php echo number_format($db_data->amount,2); ?></div>
        </div>
</div>
</div>

<div class="form-group">
          <label>Description</label>
          <div class="form-control"><?php echo $db_data->description; ?></div>
</div>

</div>
</div>

<?php if( $db_data->active_checks == 0) { ?>
<form method="post">
      <input type="hidden" name="action" value="assign_check">
        <div class="panel panel-<?php echo ( $check_items ) ? 'success' : 'danger'; ?>">
        <div class="panel-heading">
<?php if( $check_items ) { ?>
          <input type="submit" value="Assign Check" class="btn btn-primary btn-xs pull-right">
<?php } ?>
          <h3 class="panel-title">Assign a Check : <a href="<?php echo site_url("checks/index/{$db_data->bank_id}"); ?>"><strong style="margin-right: 10px;"><?php echo $db_data->bank_name; ?> (<?php echo $db_data->account_number; ?>)</strong></a></h3>
        </div>
        <div class="panel-body">

<?php if( $check_items ) { ?>
<div class="row">
<div class="col-md-2">
        <div class="form-group">
          <label>Voucher #</label>
          <input class="form-control text-center" name="voucher_number" required="required" />
        </div>
</div>
<div class="col-md-3">
        <div class="form-group">
<small><a href="<?php echo site_url("checks/add/{$db_data->bank_id}") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span></a></small>
          <label>Check Number</label>
          <select class="form-control" name="check_number">
            <?php foreach($check_items as $item) { ?>
              <option value="<?php echo $item->check_number; ?>"><?php echo $item->check_number; ?></option>
            <?php } ?>
          </select>
        </div>
</div>
<div class="col-md-7">
          <div class="form-group">
            <small><a href="<?php echo site_url("payee/add") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Payee</a></small>
          <label>Payee</label>
          <select class="form-control" name="payee_id" required="">
            <option value="">- - SELECT PAYEE - -</option>
            <?php foreach($payees as $payee) { ?>
              <option value="<?php echo $payee->id; ?>" <?php echo (($this->input->get('payee_id')) && ($this->input->get('payee_id')==$payee->id)) ? 'SELECTED' : ''; ?>><?php echo $payee->payee; ?></option>
            <?php } ?>
          </select>
        </div>
</div>
</div>

<div class="row">
<div class="col-md-6">
        <div class="form-group">
          <label>Signatory 1</label>
          <select class="form-control" name="signatory1">
            <?php foreach($signatories1 as $sig) { ?>
              <option value="<?php echo $sig->id; ?>"><?php echo $sig->signatory; ?></option>
            <?php } ?>
            <option value="0">- - Blank - -</option>
          </select>
        </div>
</div>
<div class="col-md-6">
          <div class="form-group">
          <label>Signatory 2</label>
          <select class="form-control" name="signatory2">
            <?php foreach($signatories2 as $sig) { ?>
              <option value="<?php echo $sig->id; ?>"><?php echo $sig->signatory; ?></option>
            <?php } ?>
            <option value="0">- - Blank - -</option>
          </select>
        </div>
</div>
</div>
 <?php } else { ?>
 <center>
  <p>There are no more checks available!</p>
   <a href="<?php echo site_url("checks/add/{$db_data->bank_id}"); ?>?next=<?php echo uri_string(); ?>" class="btn btn-success btn-xs">Add Checkbook</a>   
 </center>
 <?php } ?>

        </div>
      </div>
     </form>
<?php } ?>

<?php if( $checks_assigned ) { ?>
<?php $active_check = false; ?>
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Assigned Checks <a class="pull-right" href="<?php echo site_url("checks/index/{$db_data->bank_id}"); ?>"><strong style="margin-right: 10px;"><?php echo $db_data->bank_name; ?> (<?php echo $db_data->account_number; ?>)</strong></a></h3>
        </div>
        <div class="panel-body">

<table class="table table-default">
    <thead>
      <tr>
        <th>CV #</th>
        <th>Check #</th>
        <th>Payee</th>
        <th width="120px"></th>
      </tr>
    </thead>
  <?php foreach($checks_assigned as $check) { 
if( $check->voided==0 ) {
  $active_check = $check;
}
    ?>
    <tr class="<?php echo ($check->voided==1) ? 'danger' : ''; ?>">
      <td><?php echo $check->cv_id; ?></td>
      <td><?php echo $check->check_number; ?></td>
      <td><?php echo $check->payee_name; ?></td>
      <td>


<div class="btn-group pull-right">
<?php if( $db_data->active_checks == 0) { ?>
  <a href="<?php echo site_url("checks/activate/{$check->id}/{$check->db_id}"); ?>" class="btn btn-warning btn-xs confirm">Activate</a>
<?php } else { ?>
  <?php if($check->voided==0) { ?>
  <a href="<?php echo site_url("checks/print_check/{$check->id}/{$check->db_id}"); ?>" class="btn btn-success btn-xs">Print Check</a>
<?php } ?>
<?php } ?>
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <?php if($check->voided==0) { ?>
      <li><a href="<?php echo site_url("checks/edit/{$check->id}/{$check->db_id}") . "?next=" . uri_string(); ?>">Edit Check</a></li>
      <li><a href="<?php echo site_url("checks/void/{$check->id}/{$check->db_id}") . "?next=" . uri_string(); ?>" class="confirm">Void Check</a></li>
    <?php } ?>
    <li><a href="<?php echo site_url("checks/remove_assignment/{$check->id}/{$check->db_id}"); ?>" class="confirm">Remove Assignment</a></li>
  </ul>
</div>

      </td>
    </tr>
  <?php } ?>
</table>

</div>
</div>
<?php } ?>

</div>
<div class="col-md-6">
<?php if( $checks_assigned && ($db_data->type != 'fund_transfer')) { ?>

        <div class="panel panel-default">
        <div class="panel-heading">
<?php if(($db_data->advances_balance > 0) || (is_null( $db_data->advances_balance )) ) { ?>
        <a href="<?php echo site_url(uri_string()) . "?next=" . $this->input->get('next') . "&amp;add_item=1" ; ?>" class="btn btn-success btn-xs pull-right">Add Item</a>
<?php } ?>
          <h3 class="panel-title">Advances for Liquidation</h3>
        </div>
<?php if( $this->input->get('add_item') || (!$advances) ) { 
if(($db_data->advances_balance > 0) || (is_null( $db_data->advances_balance )) ) { 
$adv_amount = (is_null($db_data->advances_balance)) ? $db_data->amount : $db_data->advances_balance;
  ?>
 <div class="panel-body">
<form method="post">
  <input type="hidden" name="action" value="add_advances">
<div class="row">
    <div class="col-md-8">
      <div class="form-group">
        <small><a href="<?php echo site_url("payee/add") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Payee</a></small>
          <label>Name</label>
          <select class="form-control" name="payee_id" required="">
            <option value="">- - SELECT PAYEE - -</option>
            <?php 
$active_payee = ($active_check) ? $active_check->payee_id : 0;
$active_payee = ($this->input->get('payee_id')) ? $this->input->get('payee_id') : $active_payee;
            foreach($payees as $payee) { ?>
              <option value="<?php echo $payee->id; ?>" <?php echo (($active_payee) && ($active_payee==$payee->id)) ? 'SELECTED' : ''; ?>><?php echo $payee->payee; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control text-right" name="amount" value="<?php echo number_format($adv_amount,2); ?>">
      </div>
    </div>
</div>
<div class="row">
      <div class="col-md-12">
      <div class="form-group">
        <label>Notes</label>
        <input type="text" class="form-control text-left" name="notes" value="<?php echo ( !$this->input->get('add_item') && (!$advances) ) ? $db_data->description : ''; ?>">
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <small><a href="<?php echo site_url("advances/groups_add") . "?type=group&amp;next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Group</a></small>
          <label>Group</label>
          <select class="form-control" name="group_id">
            <option value="">- - SELECT GROUP - -</option>
            <?php 
            foreach($adv_groups as $adv_group) { if($adv_group->type=='class') continue; ?>
              <option value="<?php echo $adv_group->id; ?>" <?php echo ($this->input->get('new_id')==$adv_group->id) ? 'selected' : ''; ?>><?php echo $adv_group->name; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <small><a href="<?php echo site_url("advances/groups_add") . "?type=class&amp;next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Class</a></small>
          <label>Class</label>
          <select class="form-control" name="class_id">
            <option value="">- - SELECT CLASS - -</option>
            <?php 
            foreach($adv_groups as $adv_group) { if($adv_group->type=='group') continue; ?>
              <option value="<?php echo $adv_group->id; ?>" <?php echo ($this->input->get('new_id')==$adv_group->id) ? 'selected' : ''; ?>><?php echo $adv_group->name; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-10">
<button type="submit" class="btn btn-success btn-lg btn-block">Add</button>
  </div>
  <div class="col-md-2">
  <a class="btn btn-danger btn-lg btn-block" href="<?php echo site_url(uri_string()) . "?next=" . $this->input->get('next'); ?>">Cancel</a>
  </div>
</div>

</form>
</div>
<?php } ?>
<?php } ?>


<?php if($advances) { 
$total_adv = 0;
  ?>
<table class="table">
<tr>
<th>Name</th>
<th>Notes</th>
<th class="text-right">Amount</th>
<th></th>
</tr>
<?php foreach($advances as $adv) { ?>


<?php if(($this->input->get('edit_item')) && ($this->input->get('edit_item')==$adv->id)) { ?>
<tr >
  <td colspan="4">


<form method="post">
  <input type="hidden" name="action" value="edit_advances">
<div class="row">
    <div class="col-md-8">
      <div class="form-group">
        <small><a href="<?php echo site_url("payee/add") . "?next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Payee</a></small>
          <label>Name</label>
          <select class="form-control" name="payee_id" required="">
            <option value="">- - SELECT PAYEE - -</option>
            <?php 
$active_payee = ($active_check) ? $active_check->payee_id : 0;
$active_payee = ($this->input->get('payee_id')) ? $this->input->get('payee_id') : $active_payee;
            foreach($payees as $payee) { ?>
              <option value="<?php echo $payee->id; ?>" <?php echo (($current_item) && ($current_item->name_id==$payee->id)) ? 'SELECTED' : ''; ?>><?php echo $payee->payee; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control text-right" name="amount" value="<?php echo number_format($current_item->amount,2); ?>">
      </div>
    </div>
</div>
<div class="row">
      <div class="col-md-12">
      <div class="form-group">
        <label>Notes</label>
        <input type="text" class="form-control text-left" name="notes" value="<?php echo $current_item->notes; ?>">
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <small><a href="<?php echo site_url("advances/groups_add") . "?type=group&amp;next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Group</a></small>
          <label>Group</label>
          <select class="form-control" name="group_id">
            <option value="">- - SELECT GROUP - -</option>
            <?php 
            foreach($adv_groups as $adv_group) { if($adv_group->type=='class') continue; ?>
              <option value="<?php echo $adv_group->id; ?>" <?php echo (($current_item) && ($current_item->group_id==$adv_group->id)) ? 'SELECTED' : ''; ?>><?php echo $adv_group->name; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <small><a href="<?php echo site_url("advances/groups_add") . "?type=class&amp;next=" . uri_string(); ?>" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add Class</a></small>
          <label>Class</label>
          <select class="form-control" name="class_id">
            <option value="">- - SELECT CLASS - -</option>
            <?php 
            foreach($adv_groups as $adv_group) { if($adv_group->type=='group') continue; ?>
              <option value="<?php echo $adv_group->id; ?>" <?php echo (($current_item) && ($current_item->class_id==$adv_group->id)) ? 'SELECTED' : ''; ?>><?php echo $adv_group->name; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-10">
<button type="submit" class="btn btn-warning btn-lg btn-block">Update</button>
  </div>
  <div class="col-md-2">
  <a class="btn btn-danger btn-lg btn-block" href="<?php echo site_url(uri_string()) . "?next=" . $this->input->get('next'); ?>">Cancel</a>
  </div>
</div>
</form>


  </td>
</tr>
<?php } else { ?>

  <tr class="<?php echo ($adv->liquidated) ? '' : 'danger'; ?>">
    <td><a href="<?php echo site_url("advances/index/{$adv->name_id}"); ?>"><?php echo $adv->payee; ?></a></td>
    <td><?php echo $adv->notes; ?></td>
    <td class="text-right"><?php echo number_format($adv->amount,2); $total_adv += $adv->amount; ?></td>
    <td width="120px" class="text-right">

<div class="btn-group pull-right">
<a href="<?php echo site_url( uri_string() ); ?>?edit_item=<?php echo $adv->id; ?>&amp;next=<?php echo $this->input->get('next'); ?>" class="btn btn-warning btn-xs">Edit Item</a>

  <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
  <li>
<?php if( $adv->liquidated ) { ?>
  <a href="<?php echo site_url("advances/liquidate/{$adv->id}"); ?>?next=<?php echo uri_string(); ?>">View Items</a>
<?php } else { ?>
  <a href="<?php echo site_url("advances/liquidate/{$adv->id}"); ?>?next=<?php echo uri_string(); ?>">Liquidate</a>
 <?php } ?> </li>
<?php if( !$adv->liquidated ) { ?>
    <li><a href="<?php echo site_url("checks/remove_advances/{$adv->id}"); ?>?next=<?php echo uri_string(); ?>" class="confirm">Delete Item</a></li>
  <?php } ?>
  </ul>
</div>


    </td>
  </tr>

<?php } ?>

<?php } ?>
<tr class="info">
  <td><strong>TOTAL</strong></td>
  <td></td>
  <td class="text-right"><strong><?php echo number_format($total_adv,2); ?></strong></td>
  <td></td>
</tr>
</table>
<?php } ?>

</div>
</div>

<?php } ?>

      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>