<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<center><small>
				<p><a href="http://www.trokis.com/" target="footer_credits">Trokis Philippines</a> 
				&copy; 2016 
				<br>Developed by: 
				<a href="http://www.chesteralan.com/" target="footer_credits">Chester Alan Tagudin</a> 
				</p>
			</small></center>
		</div>
	</div> 
</div>
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jqueryui/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/numeral.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
  </body>
</html>
