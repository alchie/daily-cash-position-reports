<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">
          <a href="<?php echo site_url("funds/groups_add"); ?>" class="btn btn-success btn-xs pull-right">Add Group</a>
          <a href="<?php echo site_url("funds"); ?>" class="btn btn-warning btn-xs pull-right" style="margin-right:5px">Back</a>
          <h3 class="panel-title">Fund Groups</h3>
        </div>
        <div class="panel-body">
<?php if( $groups ) { ?>
<table class="table table-default table-condensed">
              <thead>
                <tr>
                  <th>Fund Name</th>
                  <th>Priority</th>
                  <th>Type</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach($groups as $group) { ?>
              <tr>
                <td><?php echo $group->name; ?></td>
                <td><?php echo $group->priority; ?></td>
                <td><?php echo $group->type; ?></td>
                <td>
                  <a href="<?php echo site_url("funds/groups_edit/". $group->id); ?>" class="btn btn-warning btn-xs">Edit</a>

                  <a href="<?php echo site_url("funds/groups_delete/". $group->id); ?>" class="btn btn-danger btn-xs confirm">Delete</a>

                </td>
              </tr>
              <?php } ?>
              </tbody>
</table>
<?php } else { ?>
<p class="text-center">No Groups</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>