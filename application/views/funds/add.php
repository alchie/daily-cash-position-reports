<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Funds</h3>
        </div>
      <form method="post">
        <div class="panel-body">
        <div class="form-group">
          <label>Fund Name</label>
          <input class="form-control" name="name">
        </div>
        <div class="form-group">
          <label>Priority</label>
          <input class="form-control" name="priority" value="0">
        </div>
        <div class="form-group">
          <label>Group</label>
          <select name="group" class="form-control">
            <option value="">- - Select a group - -</option>
          <?php foreach($groups as $group) { ?>
            <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
          <?php } ?>
          </select>
        </div>
        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>