<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">
        <a href="<?php echo site_url("funds/index"); ?>" class="pull-right btn btn-warning btn-xs"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
          <h3 class="panel-title">Trash </h3> 
        </div>
        <div class="panel-body">

<ul class="nav nav-tabs" style="margin-bottom:10px;">
 <li role="presentation" class="<?php echo (!$category) ? 'active' : ''; ?>"><a href="<?php echo site_url("funds/{$method}"); ?>">All Accounts</a></li>
  <li role="presentation" class="<?php echo ($category=='disposable') ? 'active' : ''; ?>"><a href="<?php echo site_url("funds/{$method}/disposable"); ?>">Disposable</a></li>
    <li role="presentation" class="<?php echo ($category=='dollar') ? 'active' : ''; ?>"><a href="<?php echo site_url("funds/{$method}/dollar"); ?>">Dollar Accounts</a></li>
  <li role="presentation" class="<?php echo ($category=='time_deposit') ? 'active' : ''; ?>"><a href="<?php echo site_url("funds/{$method}/time_deposit"); ?>">Time Deposits</a></li>
  <li role="presentation" class="<?php echo ($category=='long_term') ? 'active' : ''; ?>"><a href="<?php echo site_url("funds/{$method}/long_term"); ?>">Long-term Investments</a></li>
 <li role="presentation" class="<?php echo ($category=='checking_accounts') ? 'active' : ''; ?>"><a href="<?php echo site_url("funds/{$method}/checking_accounts"); ?>">Checking Accounts</a></li>
</ul>

            <table class="table table-default table-condensed">
              <thead>
                <tr>
                  <th>Fund Name</th>
                  <th width="25%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php 
$fund_categories = array(
'disposable' => 'Disposable',
'time_deposit' => 'Time Deposit',
'dollar' => 'Dollar',
'long_term' => 'Long Term',
  ); 
              foreach( $funds as $fund ) { ?>
                <tr class="warning">
                  <td><?php echo $fund->priority; ?>. <strong><?php echo $fund->name; ?></strong> <small><?php echo $fund->group_name; ?></small></td>
                  <td class="text-right">
                  </td>
                </tr>
                <?php if( $fund->bank_accounts_count > 0) { ?>
                      <?php foreach( $fund->bank_accounts as $bank_account ) { ?>
                        <tr>
                          <td style="padding-left:30px"><?php echo $bank_account->priority; ?>. <?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) <small class="font80p pull-right"><?php echo $fund_categories[$bank_account->fund_category]; ?> &middot; <?php echo $bank_account->class_name ?></small>
                          </td>
                          <td width="15%">
                  <a href="<?php echo site_url("bank_accounts/restore/". $bank_account->id) . "?next=" . uri_string(); ?>" class="btn btn-success btn-xs confirm">Restore</a>
                          </td>
                        </tr>
                      <?php } ?>
                  <?php } ?>
              <?php } ?>
              </tbody>
            </table>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>