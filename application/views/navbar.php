<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">Daily Cash Position Report</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          <li><a href="<?php echo site_url('reports'); ?>">Reports</a></li>
         
<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lists <span class="caret"></span></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo site_url('funds'); ?>">Funds</a></li>
               <li><a href="<?php echo site_url('funds/index/checking_accounts'); ?>">Checking Accounts</a></li>
              <li><a href="<?php echo site_url('payee'); ?>">Payee</a></li>
              <li><a href="<?php echo site_url('signatory'); ?>">Signatories</a></li>
              <li><a href="<?php echo site_url('advances'); ?>">Advances</a></li>
              
            </ul>
          </li>

          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">System <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url('check_templates'); ?>">Check Templates</a></li>
              <li><a href="<?php echo site_url('system_users'); ?>">User Accounts</a></li>
              <li><a href="<?php echo site_url('system_backup'); ?>">Database Backup</a></li>
            </ul>
          </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
<?php if( isset($other_checks) && ($other_checks)) { ?>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other Checks <span class="caret"></span></a>
<ul class="dropdown-menu" style="min-width:400px;">
    <?php foreach($other_checks as $other_check) { ?>
            <li>

            <a href="<?php echo site_url("checks/write/" . $other_check->id ); ?>"><strong><?php echo number_format($other_check->amount,2); ?></strong>
            <span class="badge pull-right"><?php echo $other_check->bank_name; ?></span>
              <br><small><?php echo $other_check->description; ?></small>
            </a>

            </li>
    <?php } ?>
</ul>
<?php } ?>
          </li>
<?php if( ($this->session->userdata('report_date')) && ( $this->session->userdata('report_date') != date('Y-m-d') )) {
$report_month = date("m", strtotime($this->session->userdata('report_date')));
$report_day = date("d", strtotime($this->session->userdata('report_date')));
$report_year = date("Y", strtotime($this->session->userdata('report_date')));
  ?>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><strong> <?php echo date("F d, Y", strtotime($this->session->userdata('report_date'))); ?></strong> <span class="caret"></span></a>
<ul class="dropdown-menu" style="min-width:400px;">
            <li><a href="<?php echo site_url("reports/view/".$report_month."/".$report_day."/".$report_year.""); ?>">Report</a></li>
            <li role="separator" class="divider"></li>
            <li><strong class="header" style="padding-left:20px">Write Check</strong></li>
<?php if( isset($disposables) ) { ?>
<?php foreach($disposables as $disp) { ?>
            <li>
            <a href="<?php echo site_url("bank_accounts/write_check/".$report_month."/".$report_day."/".$report_year."/".$disp->id); ?>">
<span class="badge pull-right"><?php echo $disp->fund_name; ?></span>
            <?php echo $disp->bank_name; ?> (<?php echo $disp->account_number; ?>)</a></li>
<?php } ?>
<?php } ?>
          </ul>
            </li>
<?php } ?>
            <li class="active dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><strong>TODAY:</strong> <?php echo date("F d, Y"); ?> <span class="caret"></span></a>
<ul class="dropdown-menu" style="min-width:400px;">
            <li><a href="<?php echo site_url("reports/view/".date("m")."/".date("d")."/".date("Y").""); ?>">Report</a></li>
            <li role="separator" class="divider"></li>
            <li><strong class="header" style="padding-left:20px">Write Check</strong></li>
<?php if( isset($disposables) ) { ?>
<?php foreach($disposables as $disp) { ?>
            <li>
            <a href="<?php echo site_url("bank_accounts/write_check/".date("m")."/".date("d")."/".date("Y")."/".$disp->id); ?>">
<span class="badge pull-right"><?php echo $disp->fund_name; ?></span>
            <?php echo $disp->bank_name; ?> (<?php echo $disp->account_number; ?>)</a></li>
<?php } ?>
<?php } ?>
          </ul>
            </li>
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->name; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('account/change_password'); ?>">Change Password</a></li>
            <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
          </ul>
        </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>