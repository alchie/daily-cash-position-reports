<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="panel panel-default">
        <div class="panel-heading">

<div class="pull-right col-md-4">
   <form method="get">
    <div class="input-group input-group-sm">
      <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-search"></span></span>
      <input name="q" type="text" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
      <span class="input-group-btn">
        <a href="<?php echo site_url("payee/add"); ?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span> Add Payee</a>
      </span>
    </div><!-- /input-group -->
  </form>
</div>

          <h3 class="panel-title"><strong>Payee</strong> 
<?php if( isset($trash_page) ) { ?>
            <a href="<?php echo site_url("payee/index"); ?>"><span class="glyphicon glyphicon-arrow-left"></span></a>
<?php } else { ?>
            <a href="<?php echo site_url("payee/trash"); ?>"><span class="glyphicon glyphicon-trash"></span></a>
<?php } ?>

          </h3>
<div class="clearfix"></div>
        </div>
        <div class="panel-body">
<?php if( $payees ) { ?>

      <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Payee Name</th>
                  <th width="180px">Actions</th>
                </tr>
              </thead>
              <tbody>
<?php foreach($payees as $payee) { ?>
<tr>
  <td class="text-left"><?php echo $payee->payee; ?></td>
  <td class="text-left">
<?php if( isset($trash_page) ) { ?>
    <a href="<?php echo site_url("payee/activate/{$payee->id}") . "?next=" . uri_string(); ?>" class="btn btn-warning btn-xs confirm">Restore</a>
<?php } else { ?>
    <a href="<?php echo site_url("payee/checks/{$payee->id}"); ?>" class="btn btn-primary btn-xs">Checks</a>
    <a href="<?php echo site_url("payee/edit/{$payee->id}"); ?>" class="btn btn-success btn-xs">Edit</a>
    <a href="<?php echo site_url("payee/deactivate/{$payee->id}") . "?next=" . uri_string(); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
<?php } ?>
  </td>
</tr>
<?php } ?>
              </tbody>
              </table>

<?php echo $pagination; ?>

<?php } else { ?>
<p class="text-center">No Entries</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>