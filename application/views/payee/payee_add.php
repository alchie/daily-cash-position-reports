<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Payee</h3>
        </div>
      <form method="post">
        <div class="panel-body">
<div class="row">
<div class="col-md-12">
        <div class="form-group">
          <label>Payee Name</label>
          <textarea class="form-control" name="name" rows="<?php echo ($this->input->get('multiple')) ? 20 : 2; ?>"></textarea>
        </div>
</div>
</div>

        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo ($this->input->get('next')) ? site_url($this->input->get('next')) : site_url("payee"); ?>" class="btn btn-warning">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>