<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-4 col-md-4 col-md-offset-4">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Report</h3>
        </div>
        <form method="post">
        <div class="panel-body">
            <div class="form-group">
              <label>Beginning Balance</label>
              <input class="form-control datepicker text-center" type="text" name="beginning">
            </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
        </form>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>