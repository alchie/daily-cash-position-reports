<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
<form method="get" action="<?php echo site_url("reports/{$this->input->get('uri')}/{$month}/{$day}/{$year}"); ?>">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Filters : Detailed Report</h3>
        </div>
        <div class="panel-body">

<div class="row">
  <div class="col-md-6">
          <h3>Fund Category</h3>
<?php foreach(array(
  'disposable'=>'Disposable',
  'time_deposit'=>'Time Deposit',
  'dollar'=>'Dollar',
  'long_term'=>'Long Term',
  ) as $k=>$v) { ?>          
 <div class="checkbox">
    <label>
      <input type="checkbox" name="filters[category][]" value="<?php echo($k); ?>"> <?php echo($v); ?>
    </label>
  </div>
<?php } ?>
          <h3>Group</h3>
<?php foreach($groups as $group) { 
if( $group->type=='class') {
  continue;
}
  ?>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="filters[group][]" value="<?php echo $group->id; ?>"> <?php echo $group->name; ?>
    </label>
  </div>
<?php } ?>

<h3>Class</h3>
<?php foreach($groups as $group) { 
if( $group->type=='group') {
  continue;
}
  ?>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="filters[class][]" value="<?php echo $group->id; ?>"> <?php echo $group->name; ?>
    </label>
  </div>
<?php } ?>
  </div>
  <div class="col-md-6">
      <h3>Options</h3>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="ending_balance_only" value="1"> Ending Balance Only
        </label>
      </div>
  </div>
</div>
        </div>
<div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
        </div>
      </div>
    </form>
      </div>

    </div>
</div>

<?php $this->load->view('footer'); ?>