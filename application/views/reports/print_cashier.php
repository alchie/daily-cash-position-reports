<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Daily Cash Position Reports</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/styles.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/jqueryui/jquery-ui.min.css'); ?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
  body {
    font-size: 11px;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    border: 1px solid #000;
    padding:4px;
  }
  .navlinks {
    font-size: 20px;
    position: absolute;
    margin-left: 20px;
  }
  .navlinks.next {
    right: 0;
    margin-right: 20px;
  }

  .center {
    font-size: 20px;
    margin: auto;
    text-align: center;
  }
  @media print {
    .no-print {
      display: none;
    }
  }
</style>
  </head>
  <body style="padding:0;">

<a href="<?php echo site_url("reports/print_cashier/" . date('m/d/Y', strtotime($reports->next_day))); ?>" class="navlinks next no-print"><?php echo date('F d, Y', strtotime($reports->next_day)); ?> &gt;&gt;</a>

<a href="<?php echo site_url("reports/print_cashier/" . date('m/d/Y', strtotime($reports->previous_day))); ?>" class="navlinks previous no-print">&lt;&lt; <?php echo date('F d, Y', strtotime($reports->previous_day)); ?></a>

<p class="center no-print"><a href="<?php echo site_url("reports/view/" . date('m/d/Y', strtotime($reports->currentDate))); ?>">Home</a></p>

<h4 class="bold">Daily Cash Position Report</h4>
<h5><?php echo date('F d, Y', strtotime($reports->currentDate)); ?></h5>

<?php 
$beg_cash = 0;
$beg_check = 0;
$balance = 0; 
?>
<table border="1" class="table table-default">
<tr class="success">
  <td class="allcaps bold">Undeposited Funds</td>
  <th width="15%" class="text-center bold allcaps font100p">Beginning Balance</th>
  <th width="15%" class="text-center bold allcaps font100p">Collections</th>
  <th width="15%" class="text-center bold allcaps font100p">Deposits</th>
  <th width="15%" class="text-center bold allcaps font100p">Ending Balance</th>
</tr>

<tr>
  <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cash</td>
  <td class="text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_cash_deposit-$current_report->beg_cash_disbursement),2);
  $beg_cash += ($current_report->beg_cash_deposit-$current_report->beg_cash_disbursement);
  } else {
    echo '0.00';
  } 
?>
  </td> 
  <td class="text-right">
<?php if($current_report) {
  echo number_format($current_report->cash_deposit,2);
  $beg_cash += $current_report->cash_deposit;
  } else {
    echo '0.00';
  } 
?>
</td> 
  <td class="text-right">
<?php if($current_report) {
  echo number_format($current_report->cash_disbursement,2);
  $beg_cash -= $current_report->cash_disbursement;
  } else {
    echo '0.00';
  } 
?>
  </td> 
  <td class="text-right"><?php echo number_format($beg_cash,2); ?></td> 
</tr>
<tr>
  <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Check</td>
  <td class="text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_check_deposit-$current_report->beg_check_disbursement),2);
  $beg_check += ($current_report->beg_check_deposit-$current_report->beg_check_disbursement);
  } else {
    echo '0.00';
  } 
?>
  </td> 
  <td class="text-right">
<?php if($current_report) {
  echo number_format($current_report->check_deposit,2);
  $beg_check += $current_report->check_deposit;
  } else {
    echo '0.00';
  } 
?>
  </td> 
  <td class="text-right">
<?php if($current_report) {
  echo number_format($current_report->check_disbursement,2);
  $beg_check -= $current_report->check_disbursement;
  } else {
    echo '0.00';
  } 
?>
  </td> 
  <td class="text-right"><?php echo number_format($beg_check,2); ?></td> 
</tr>
<tr>
  <td class="allcaps bold">TOTAL</td>
  <td class="text-right bold font120p">
<?php if($current_report) {
  echo number_format(($current_report->beg_deposit-$current_report->beg_disbursement),2);
  $balance = $balance + ($current_report->beg_deposit-$current_report->beg_disbursement); 
  } else {
    echo '0.00';
  } 
?>    
  </td> 
  <td class="text-right bold font120p">
<?php 
if($current_report) {
                  echo number_format($current_report->deposit,2); 
                  $balance = $balance + $current_report->deposit; 
} else {
  echo '0.00';
}
?>
  </td>
  <td class="text-right bold font120p">
<?php 
if($current_report) {
                  echo number_format($current_report->disbursement,2); 
                  $balance = $balance - $current_report->disbursement; 
} else {
  echo '0.00';
}
?>
  </td>
  <td class="text-right bold font120p"><?php echo number_format($balance,2); ?></td>
</tr>
</table>

<?php 
$limit = 6;
$end_balances = array();
for($i=1;$i <= 2;$i++) { ?>
<table border="1" class="table table-default">
  <tr class="success">
    <td></td>
    <?php foreach( $funds as $key=>$fund ) {
      $end_balances[$fund->id] = 0;
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {       
      ?>
        <td class="text-center bold allcaps font100p" width="12%"><?php echo $fund->name; ?></td>
    <?php } } ?>
  </tr>
  <tr>
    <td class="allcaps bold">Beginning Balance</td>
     <?php foreach( $funds as $key=>$fund ) {
   if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_beg = 0;
        $beg_bal = ($fund->beg_deposit-$fund->beg_disbursement);
        $end_balances[$fund->id] = $beg_bal;
      ?>
        <td class="text-center allcaps font100p"><?php echo ((int) $beg_bal != 0) ? number_format($beg_bal,2) : ''; ?></td>
    <?php } } ?>
  </tr>

  <tr>
    <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cash Deposits</td>
     <?php foreach( $funds as $key=>$fund ) {
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_d1 = 0;
        $end_balances[$fund->id] += $fund->cash_deposit;
      ?>
        <td class="text-center allcaps font100p"><?php echo ((int) $fund->cash_deposit != 0) ? number_format($fund->cash_deposit,2) : ''; ?></td>
    <?php } } ?>
  </tr>

  <tr>
    <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Check Deposits</td>
     <?php foreach( $funds as $key=>$fund ) {
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_d1 = 0;
        $end_balances[$fund->id] += $fund->check_deposit;
      ?>
        <td class="text-center allcaps font100p"><?php echo ((int) $fund->check_deposit != 0) ? number_format($fund->check_deposit,2) : ''; ?></td>
    <?php } } ?>
  </tr>

  <tr>
    <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fund Transfers</td>
     <?php foreach( $funds as $key=>$fund ) {
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_d1 = 0;
        $end_balances[$fund->id] += $fund->fund_transfers;
      ?>
        <td class="text-center allcaps font100p"><?php echo ((int) $fund->fund_transfers != 0) ? number_format($fund->fund_transfers,2) : ''; ?></td>
    <?php } } ?>
  </tr>

  <tr>
    <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adjustments</td>
     <?php foreach( $funds as $key=>$fund ) {
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_d1 = 0;
        $end_balances[$fund->id] += $fund->adjustments;
      ?>
        <td class="text-center allcaps font100p"><?php echo ((int) $fund->adjustments != 0) ? number_format($fund->adjustments,2) : ''; ?></td>
    <?php } } ?>
  </tr>

  <tr>
    <td class="allcaps bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Disbursements</td>
    <?php foreach( $funds as $key=>$fund ) {
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_d2 = 0;
        $end_balances[$fund->id] -= $fund->disbursement;
      ?>
        <td class="text-center allcaps font100p"><?php echo ((int) $fund->disbursement != 0) ? "(" .number_format($fund->disbursement,2) . ")" : ''; ?></td>
    <?php } } ?>
  </tr>

<tr>
    <td class="allcaps bold">Ending Balance</td>
     <?php foreach( $funds as $key=>$fund ) {
    if( ($key >= (($i-1) * $limit)) && ($key < ($i * $limit)) ) {
      $fund_balance_d2 = 0;
      ?>
        <td class="text-center allcaps font120p bold"><?php echo number_format($end_balances[$fund->id],2); ?></td>
    <?php } } ?>
  </tr>

</table>
<?php } ?>

  <p class="allcaps bold" style="margin-top:30px;">Prepared by: <?php echo $this->session->name; ?></p>

</body>
</html>