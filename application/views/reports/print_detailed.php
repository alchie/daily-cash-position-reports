<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$fund_categories = array(
'disposable' => 'Disposable',
'time_deposit' => 'Time Deposit',
'dollar' => 'Dollar',
'long_term' => 'Long Term',
  ); 
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Daily Cash Position Reports</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/styles.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/jqueryui/jquery-ui.min.css'); ?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
  body {
    font-size: 11px;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    border: 1px solid #000;
  }
  .navlinks {
    font-size: 20px;
    position: absolute;
    margin-left: 20px;
  }
  .navlinks.next {
    right: 0;
    margin-right: 20px;
  }

  .center {
    font-size: 20px;
    margin: auto;
    text-align: center;
  }
  @media print {
    .no-print {
      display: none;
    }
  }
</style>
  </head>
  <body style="padding:0;">


<a href="<?php echo site_url("reports/print_detailed/" . date('m/d/Y', strtotime($reports->next_day))); ?>" class="navlinks next no-print"><?php echo date('F d, Y', strtotime($reports->next_day)); ?> &gt;&gt;</a>

<a href="<?php echo site_url("reports/print_detailed/" . date('m/d/Y', strtotime($reports->previous_day))); ?>" class="navlinks previous no-print">&lt;&lt; <?php echo date('F d, Y', strtotime($reports->previous_day)); ?></a>

<p class="center no-print">
<a href="<?php echo site_url("reports/all_accounts/" . date('m/d/Y', strtotime($reports->currentDate))); ?>">Home</a>
&middot;
<a href="<?php echo site_url("reports/print_summary/" . date('m/d/Y', strtotime($reports->currentDate))); ?>">Summary</a>
&middot;
<a href="<?php echo site_url("reports/filters/" . date('m/d/Y', strtotime($reports->currentDate))); ?>?uri=print_detailed">Filters</a></p>

<h4 class="bold">Funds Report (Detailed)</h4>
<h5 class=""><?php echo date('F d, Y - l', strtotime($reports->currentDate)); ?></h5>
<?php 
$total_disp_beg = 0;
$total_deposits = 0;
$total_disbursements = 0;
$total_disp_end = 0;
$total_time_deposits = 0;
$total_dollars = 0;
$total_long_term = 0;
$total_balance = 0;
$total_beg = 0;
$total_deposits = 0;
$total_disbursements = 0;
$total_end = 0;

$balance = 0;
if($current_report) {
  $balance = $balance + ($current_report->beg_deposit - $current_report->beg_disbursement); 
  $total_disp_beg += ($current_report->beg_deposit - $current_report->beg_disbursement);
}

if($current_report) {
  if( floatval($current_report->deposit) != 0)  {
                  $balance = $balance + $current_report->deposit; 
                  //$total_deposits += $current_report->deposit;
  }
}
if($current_report) {
  if( floatval($current_report->disbursement) != 0)  {
                  $balance = $balance - $current_report->disbursement; 
                  //$total_disbursements += $current_report->disbursement;
  }
}
$balance = (floatval($balance) == 0) ? 0 : $balance;
$total_disp_end += $balance;

?>

            <table class="table table-default table-condensed" border="1">
              <tbody>
                <tr>
                  <th class="allcaps"></th>
<?php if( !$this->input->get('ending_balance_only') ) { ?>
                  <th  class="text-right allcaps" width="10%">Beg. Balance</th>
                  <th  class="text-right allcaps" width="10%">Collections</th>
                  <th  class="text-right allcaps" width="10%">Deposits</th>
<?php } ?>
                  <th  class="text-right allcaps" width="10%">Ending Balance</th>
                </tr>
              <tr class="">
                  <td class="bold allcaps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Undeposited Funds</td>

<?php if( !$this->input->get('ending_balance_only') ) { ?>

                  <td  class="bold text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_deposit - $current_report->beg_disbursement),2);
  } else {
    echo '0.00';
  } 

?>

</td>
                  <td  class="text-right">
<?php 
if($current_report) {
  if( floatval($current_report->deposit) != 0)  {
                  echo number_format($current_report->deposit,2); 
  }
} else {
  echo '';
}
?>
                  </td>
                  <td  class="text-right">
<?php 
if($current_report) {
  if( floatval($current_report->disbursement) != 0)  {
                  echo number_format($current_report->disbursement,2); 
  }
} else {
  echo '';
}
?>
                  </td>
<?php } ?>
                  <td  class="bold text-right"><?php 
                  echo number_format($balance,2); 
                  ?></td>

                </tr>
<tr>
  <td colspan="<?php echo ( !$this->input->get('ending_balance_only') ) ? 5 : 2; ?>"></td>
</tr>
                <tr>
                  <th></th>
<?php if( !$this->input->get('ending_balance_only') ) { ?>
                  <th class="text-right allcaps" width="10%">Beg. Balance</th>
                  <th class="text-right allcaps" width="10%">Deposits</th>
                  <th class="text-right allcaps" width="10%">Disbursements</th>
<?php } ?>
                  <th class="text-right allcaps" width="10%">Ending Balance</th>
                </tr>

              
<?php foreach( $funds as $fund ) { 
$fund_balance_beg = 0;
$fund_balance_d1 = 0;
$fund_balance_d2 = 0;
$fund_balance_end = 0;
  ?>
<?php if( $fund->bank_accounts_count > 0) { ?>
                <tr class="warning">
                  <td class="bold"><?php echo $fund->name; ?></td>
<?php if( !$this->input->get('ending_balance_only') ) { ?>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
<?php } ?>
                  <td class="text-right"></td>
                </tr>
<?php foreach( $fund->bank_accounts as $bank_account ) { 
$balance = 0;

$balance = $balance + ($bank_account->beg_deposit-$bank_account->beg_disbursement); 
$fund_balance_beg += ($bank_account->beg_deposit-$bank_account->beg_disbursement);

$balance = $balance + $bank_account->deposit; 
$fund_balance_d1 += $bank_account->deposit;

$balance = $balance - $bank_account->disbursement; 
$fund_balance_d2 += $bank_account->disbursement;

$fund_balance_end += $balance;
                        ?>
                        <tr>
                          <td style="padding-left:30px"><?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) <small style="float:right;"><?php echo $fund_categories[$bank_account->fund_category]; ?> &middot; <?php echo $bank_account->class_name ?></small>
                          </td>

<?php if( !$this->input->get('ending_balance_only') ) { ?>

                  <td class="text-right">
<?php echo number_format(($bank_account->beg_deposit-$bank_account->beg_disbursement),2); ?>
</td>
                  <td class="text-right">
                  <?php echo number_format($bank_account->deposit,2); ?>
                  </td>
                  <td class="text-right">
                  <?php echo number_format($bank_account->disbursement,2); ?>
                  </td>
<?php } ?>
                  <td class="text-right"><?php echo number_format($balance,2); ?></td>
                        </tr>
                      <?php } ?>
<?php 
$total_beg += $fund_balance_beg;
$total_deposits += $fund_balance_d1; 
$total_disbursements += $fund_balance_d2; 
$total_end += $fund_balance_end;
?>
            <tr class="success">
                  <td class="bold allcaps">TOTAL <?php echo $fund->name; ?></td>
<?php if( !$this->input->get('ending_balance_only') ) { ?>
                  <td class="text-right bold">
<?php echo number_format($fund_balance_beg,2); ?>
</td>
                  <td class="text-right bold">
<?php echo number_format($fund_balance_d1,2); ?>
</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_d2,2); 
  
?></td>
<?php } ?>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_end,2); 
  
?></td>
                </tr>

                  <?php } ?>
              <?php } ?>
<tr>
  <td colspan="<?php echo ( !$this->input->get('ending_balance_only') ) ? 5 : 2; ?>"></td>
</tr>
<tr class="">
      <td class="bold allcaps font130p">TOTAL BALANCES</td>
<?php if( !$this->input->get('ending_balance_only') ) { ?>
      <td class="text-right bold font130p"><?php echo number_format($total_beg,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_deposits,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_disbursements,2); ?></td>
<?php } ?>
      <td class="text-right bold font130p"><?php echo number_format($total_end,2); ?></td>
</tr>
              </tbody>
            </table>

            <p class="allcaps bold" style="margin-top:30px;">Prepared by: <?php echo $this->session->name; ?></p>






</body></html>