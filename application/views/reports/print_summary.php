<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Daily Cash Position Reports</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/styles.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/jqueryui/jquery-ui.min.css'); ?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
  body {
    font-size: 11px;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    border: 1px solid #000;
  }
  .navlinks {
    font-size: 20px;
    position: absolute;
    margin-left: 20px;
  }
  .navlinks.next {
    right: 0;
    margin-right: 20px;
  }

  .center {
    font-size: 20px;
    margin: auto;
    text-align: center;
  }
  @media print {
    .no-print {
      display: none;
    }
  }
</style>
  </head>
  <body style="padding:0;">


<a href="<?php echo site_url("reports/print_summary/" . date('m/d/Y', strtotime($reports->next_day))); ?>" class="navlinks next no-print"><?php echo date('F d, Y', strtotime($reports->next_day)); ?> &gt;&gt;</a>

<a href="<?php echo site_url("reports/print_summary/" . date('m/d/Y', strtotime($reports->previous_day))); ?>" class="navlinks previous no-print">&lt;&lt; <?php echo date('F d, Y', strtotime($reports->previous_day)); ?></a>

<p class="center no-print">
<a href="<?php echo site_url("reports/summary/" . date('m/d/Y', strtotime($reports->currentDate))); ?>">Home</a> &middot;
<a href="<?php echo site_url("reports/print_detailed/" . date('m/d/Y', strtotime($reports->currentDate))); ?>">Detailed</a>
</p>


<h4 class="bold">Funds Report (Summary)</h4>
<h5 class=""><?php echo date('F d, Y - l', strtotime($reports->currentDate)); ?></h5>
<?php 
$total_disp_beg = 0;
$total_deposits = 0;
$total_disbursements = 0;
$total_disp_end = 0;
$total_time_deposits = 0;
$total_dollars = 0;
$total_long_term = 0;
$total_balance = 0;
?>

            <table class="table table-default table-condensed" border="1">
              <tbody>
                <tr>
                  <th class="allcaps"></th>
                  <th colspan="2" class="text-right allcaps" width="10%">Beg. Balance</th>
                  <th colspan="2" class="text-right allcaps" width="10%">Collections</th>
                  <th colspan="2" class="text-right allcaps" width="10%">Deposits</th>
                  <th colspan="2" class="text-right allcaps" width="10%">Ending Balance</th>
                </tr>
              <tr class="">
<?php $balance = 0; ?>
                  <td class="bold allcaps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Undeposited Funds</td>
                  <td colspan="2" class="bold text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_deposit - $current_report->beg_disbursement),2);
  $balance = $balance + ($current_report->beg_deposit - $current_report->beg_disbursement); 
  $total_disp_beg += ($current_report->beg_deposit - $current_report->beg_disbursement);
  } else {
    echo '0.00';
  } 

?>

</td>
                  <td colspan="2" class="text-right">
<?php 
if($current_report) {
  if( floatval($current_report->deposit) != 0)  {
                  echo number_format($current_report->deposit,2); 
                  $balance = $balance + $current_report->deposit; 
                  $total_deposits += $current_report->deposit;
  }
} else {
  echo '';
}
?>
                  </td>
                  <td colspan="2" class="text-right">
<?php 
if($current_report) {
  if( floatval($current_report->disbursement) != 0)  {
                  echo number_format($current_report->disbursement,2); 
                  $balance = $balance - $current_report->disbursement; 
                  $total_disbursements += $current_report->disbursement;
  }
} else {
  echo '';
}
?>
                  </td>
                  <td colspan="2" class="bold text-right"><?php echo number_format($balance,2); 
$total_disp_end += $balance;
                  ?></td>

                </tr>

<tr>
  <td colspan="9"></td>
</tr>
<?php 

foreach( $groups as $group ) { 
$total_group_funds = 0;
  ?>

<tr style="background-color: #EEE;">
                  <th class="allcaps"><?php echo $group->name; ?></th>
                  <th class="text-right allcaps" width="10%">Beg. Balance</th>
                  <th class="text-right allcaps" width="10%">Deposits</th>
                  <th class="text-right allcaps" width="10%">Disbursements</th>
                  <th class="text-right allcaps" width="10%">Ending Balance</th>
                  <th class="text-right allcaps" width="10%">Time Deposits</th>
                  <th class="text-right allcaps" width="10%">Dollar Accounts</th>
                  <th class="text-right allcaps" width="10%">Long-Term Investments</th>
                  <th class="text-right allcaps" width="10%">Total</th>
                </tr>

  <?php 
foreach( $funds as $fund ) { 
  if( $group->id != $fund->group) {
    continue;
  }
$fund_balance_end = 0;
$fund_total = 0;
  ?>

            <tr class="">
                  <td class="bold allcaps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $fund->name; ?></td>
                  <td class="text-right bold">
<?php 
  echo ( floatval($fund->beg_deposit-$fund->beg_disbursement) != 0) ? number_format(($fund->beg_deposit-$fund->beg_disbursement),2) : ''; 
  $total_disp_beg += ($fund->beg_deposit-$fund->beg_disbursement);
  $fund_balance_end += ($fund->beg_deposit-$fund->beg_disbursement);
?>
</td>
                  <td class="text-right">
<?php 
  echo (floatval($fund->deposit) != 0) ? number_format($fund->deposit,2) : '';
  $total_deposits += $fund->deposit; 
  $fund_balance_end += $fund->deposit;
?>
</td>
                  <td class="text-right">
<?php 
  echo (floatval($fund->disbursement) != 0) ? "(" .number_format($fund->disbursement,2). ")" : ''; 
  $total_disbursements += $fund->disbursement;
  $fund_balance_end -= $fund->disbursement; 
?></td>
                  <td class="text-right bold">
<?php 
  echo (floatval($fund_balance_end) != 0) ? number_format($fund_balance_end,2) : ''; 
  $total_disp_end += $fund_balance_end;
  $fund_total += $fund_balance_end;
?></td>

                  <td class="text-right">
<?php 
  $time_deposit = ($fund->time_deposit_debits - $fund->time_deposit_credits);
  echo (floatval($time_deposit) != 0) ? number_format($time_deposit,2) : ''; 
  $total_time_deposits += $time_deposit;
  $fund_total += $time_deposit;
?></td>

                  <td class="text-right">
<?php 
  $dollar = ($fund->dollar_debits - $fund->dollar_credits);
  echo (floatval($dollar) != 0) ? number_format($dollar,2) : ''; 
  $total_dollars += $dollar;
  $fund_total += $dollar;
?></td>

                  <td class="text-right">
<?php 
  $long_term = ($fund->long_term_debits - $fund->long_term_credits);
  echo (floatval($long_term) != 0) ? number_format($long_term,2) : ''; 
  $total_long_term += $long_term;
  $fund_total += $long_term;
?></td>

                  <td class="text-right bold font110p">
<?php 
  echo number_format($fund_total,2); 
  $total_balance += $fund_total;
  $total_group_funds += $fund_total;
?></td>
                </tr>

              <?php } ?>
<tr>
  <td colspan="8" style="background-color: #CCC;" class="bold allcaps text-right">Total <?php echo $group->name; ?></td>
  <td style="background-color: #CCC;" class="text-right bold font130p"><?php  echo number_format($total_group_funds,2); ?></td>
</tr>
<tr>
  <td colspan="9"></td>
</tr>
       <?php } ?>
<tr class="">
      <td class="bold allcaps font130p">TOTAL BALANCES</td>
      <td class="text-right bold font130p"><?php echo number_format($total_disp_beg,2); ?></td>
      <td class="text-right bold font110p"><?php echo number_format($total_deposits,2); ?></td>
      <td class="text-right bold font110p">(<?php echo number_format($total_disbursements,2); ?>)</td>
      <td class="text-right bold font130p"><?php echo number_format($total_disp_end,2); ?></td>
      <td class="text-right bold font110p"><?php echo number_format($total_time_deposits,2); ?></td>
      <td class="text-right bold font110p"><?php echo number_format($total_dollars,2); ?></td>
      <td class="text-right bold font110p"><?php echo number_format($total_long_term,2); ?></td>
      <td class="text-right bold font150p"><?php echo number_format($total_balance,2); ?></td>
</tr>
              </tbody>
            </table>
            <p class="allcaps bold" style="margin-top:30px;">Prepared by: <?php echo $this->session->name; ?></p>





</body></html>