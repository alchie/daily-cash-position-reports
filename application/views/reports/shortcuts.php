<div class="btn-group pull-right hide-print" role="group">

 <a href="<?php echo site_url("reports/select/" . date('m/d/Y', strtotime($reports->previous_day)) ); ?>?f=<?php echo ((isset($cat)&&($cat))?$cat:'view'); ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-chevron-left"></span></a>

 <a href="<?php echo site_url("welcome/index/{$reports->month}/{$reports->year}"); ?>?f=<?php echo ((isset($cat)&&($cat))?$cat:'view'); ?>" class="btn btn-warning btn-xs">View Calendar</a>

<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $reports->monthName; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php for($i=1;$i<=12;$i++) { ?>
    <li class="<?php echo ($i==$reports->month) ? 'active' : ''; ?>"><a href="<?php echo site_url("reports/select/{$i}/{$reports->day}/{$reports->year}"); ?>?f=<?php echo ((isset($cat)&&($cat))?$cat:'view'); ?>"><?php echo date('F', strtotime($i . "/1/1990")); ?></a></li>
  <?php } ?>
  </ul>
</div>

<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $reports->day; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
<?php for($i=1;$i<=$reports->monthDays;$i++) { ?>
    <li class="<?php echo ($i==$reports->day) ? 'active' : ''; ?>"><a href="<?php echo site_url("reports/select/{$reports->month}/{$i}/{$reports->year}"); ?>?f=<?php echo ((isset($cat)&&($cat))?$cat:'view'); ?>"><?php echo $i; ?></a></li>
<?php } ?>
  </ul>
</div>

<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $reports->year; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
<?php 
for($i=$years['min_year'];$i<($years['max_year']+1);$i++) { ?>
    <li class="<?php echo ($i==$reports->year) ? 'active' : ''; ?>"><a href="<?php echo site_url("reports/select/{$reports->month}/{$reports->day}/{$i}"); ?>?f=<?php echo ((isset($cat)&&($cat))?$cat:'view'); ?>"><?php echo $i; ?></a></li>
<?php } ?>
  </ul>
</div>

<a href="<?php echo site_url("reports/select/" . date('m/d/Y', strtotime($reports->next_day)) ); ?>?f=<?php echo ((isset($cat)&&($cat))?$cat:'view'); ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-chevron-right"></span></a>

</div>

<div class="btn-group pull-right hide-print" role="group" style="margin-right:10px;">
<div class="btn-group">
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Print <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <li><a href="<?php echo site_url("reports/print_collection/{$reports->month}/{$reports->day}/{$reports->year}"); ?>">Cash Collection Report</a></li>
  <li><a href="<?php echo site_url("reports/print_cashier/{$reports->month}/{$reports->day}/{$reports->year}"); ?>">Cash Position Report</a></li>
  <li><a href="<?php echo site_url("reports/print_detailed/{$reports->month}/{$reports->day}/{$reports->year}"); ?>">Funds Report (Detailed)</a></li>
  <li><a href="<?php echo site_url("reports/print_summary/{$reports->month}/{$reports->day}/{$reports->year}"); ?>">Funds Report (Summary)</a></li>
  </ul>
</div>
</div>