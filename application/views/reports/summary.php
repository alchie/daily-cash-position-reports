<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

<?php $this->load->view('reports/shortcuts'); ?>

          <h3 class="panel-title">Summary - <?php echo date('F d, Y', strtotime("{$current_month}/{$current_day}/{$current_year}")); ?></h3>
        </div>
        <div class="panel-body">

<ul class="nav nav-tabs" style="margin-bottom:10px;">
  <li role="presentation"><a href="<?php echo site_url("reports/view/{$current_month}/{$current_day}/{$current_year}"); ?>">Disposable Funds</a></li>
    <li role="presentation"><a href="<?php echo site_url("reports/dollar/{$current_month}/{$current_day}/{$current_year}"); ?>">Dollar Accounts</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/time_deposits/{$current_month}/{$current_day}/{$current_year}"); ?>">Time Deposits</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/long_term/{$current_month}/{$current_day}/{$current_year}"); ?>">Long-term Investments</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/all_accounts/{$current_month}/{$current_day}/{$current_year}"); ?>">All Accounts</a></li>
  <li role="presentation" class="active"><a href="<?php echo site_url("reports/summary/{$current_month}/{$current_day}/{$current_year}"); ?>">Summary</a></li>
</ul>

<?php 

$total_beg = 0;
$total_deposits = 0;
$total_disbursements = 0;
$total_end = 0;
?>

            <table class="table table-striped table-condensed">
              <thead>
                <tr>
                  <th>Funds</th>
                  <th class="text-right" width="15%">Beg. Balance</th>
                  <th class="text-right" width="15%">Deposits</th>
                  <th class="text-right" width="15%">Disbursements</th>
                  <th class="text-right" width="15%">Ending Balance</th>
                </tr>
              </thead>
              <tbody>


              <tr class="">
<?php $balance = 0; ?>
                  <td class="bold allcaps">Undeposited Funds</td>
                  <td class="bold text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_deposit - $current_report->beg_disbursement),2);
  $balance = $balance + ($current_report->beg_deposit - $current_report->beg_disbursement); 
  $total_beg += ($current_report->beg_deposit - $current_report->beg_disbursement);
  } else {
    echo '0.00';
  } 

?>

</td>
                  <td class="bold text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->deposit,2); 
                  $balance = $balance + $current_report->deposit; 
                  //$total_deposits += $current_report->deposit;
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="bold text-right">(<?php 
if($current_report) {
                  echo number_format($current_report->disbursement,2); 
                  $balance = $balance - $current_report->disbursement; 
                 // $total_disbursements += $current_report->disbursement;
} else {
  echo '0.00';
}
?>)</td>
                  <td class="bold text-right"><?php 
        $balance = (intval($balance) == 0) ? '0' : $balance;
        echo number_format($balance,2); 
$total_end += $balance;
                  ?></td>
                </tr>

<?php foreach( $funds as $fund ) { 
$fund_balance_beg = 0;
$fund_balance_d1 = 0;
$fund_balance_d2 = 0;
$fund_balance_end = 0;
  ?>
            <tr class="">
                  <td class="bold allcaps"><?php echo $fund->name; ?></td>
                  <td class="text-right bold">
<?php 
  echo number_format(($fund->beg_deposit-$fund->beg_disbursement),2); 
  $total_beg += ($fund->beg_deposit-$fund->beg_disbursement);
  $fund_balance_end += ($fund->beg_deposit-$fund->beg_disbursement);
?>
</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund->deposit,2);
  $total_deposits += $fund->deposit; 
  $fund_balance_end += $fund->deposit;
?>
</td>
                  <td class="text-right bold">
(<?php 
  echo number_format($fund->disbursement,2); 
  $total_disbursements += $fund->disbursement; 
  $fund_balance_end -= $fund->disbursement;
?>)</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_end,2); 
  $total_end += $fund_balance_end;
?></td>
                </tr>

              <?php } ?>
<tr class="">
      <td class="bold allcaps font130p">TOTAL BALANCES</td>
      <td class="text-right bold font130p"><?php echo number_format($total_beg,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_deposits,2); ?></td>
      <td class="text-right bold font130p">(<?php echo number_format($total_disbursements,2); ?>)</td>
      <td class="text-right bold font130p"><?php echo number_format($total_end,2); ?></td>
</tr>
              </tbody>
            </table>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('reports/view_modals'); ?>
<?php $this->load->view('footer'); ?>