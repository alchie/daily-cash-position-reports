<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

<?php $this->load->view('reports/shortcuts'); ?>

<?php 
$total_beg = 0;
$total_deposits = 0;
$total_disbursements = 0;
$total_end = 0;
?>

          <h3 class="panel-title">Time Deposits - <?php echo date('F d, Y', strtotime("{$current_month}/{$current_day}/{$current_year}")); ?></h3>
        </div>
        <div class="panel-body">

<ul class="nav nav-tabs" style="margin-bottom:10px;">
  <li role="presentation"><a href="<?php echo site_url("reports/view/{$current_month}/{$current_day}/{$current_year}"); ?>">Disposable Funds</a></li>
    <li role="presentation"><a href="<?php echo site_url("reports/dollar/{$current_month}/{$current_day}/{$current_year}"); ?>">Dollar Accounts</a></li>
  <li role="presentation" class="active"><a href="<?php echo site_url("reports/time_deposits/{$current_month}/{$current_day}/{$current_year}"); ?>">Time Deposits</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/long_term/{$current_month}/{$current_day}/{$current_year}"); ?>">Long-term Investments</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/all_accounts/{$current_month}/{$current_day}/{$current_year}"); ?>">All Accounts</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/summary/{$current_month}/{$current_day}/{$current_year}"); ?>">Summary</a></li>
</ul>

            <table class="table table-default table-condensed">
              <thead>
                <tr>
                  <th>Funds</th>
                  <th class="text-right" width="15%">Beg. Balance</th>
                  <th class="text-right" width="15%">Deposits</th>
                  <th class="text-right" width="15%">Disbursements</th>
                  <th class="text-right" width="15%">Ending Balance</th>
                </tr>
              </thead>
              <tbody>
<?php foreach( $funds as $fund ) { 
$fund_balance_beg = 0;
$fund_balance_d1 = 0;
$fund_balance_d2 = 0;
$fund_balance_end = 0;
  ?>
<?php if( $fund->bank_accounts_count > 0) { ?>
                <tr class="warning">
                  <td class="bold"><?php echo $fund->name; ?></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                </tr>
<?php foreach( $fund->bank_accounts as $bank_account ) { 
$balance = 0;
                        ?>
                        <tr>
                          <td style="padding-left:30px">
<a href="<?php echo site_url("bank_accounts/details/{$bank_account->id}"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&next=<?php echo urlencode(uri_string()); ?>">
                          <?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>)
</a>
                          </td>
                  <td class="text-right">
<?php echo number_format(($bank_account->beg_deposit-$bank_account->beg_disbursement),2); ?>
<?php 
$balance = $balance + ($bank_account->beg_deposit-$bank_account->beg_disbursement); 
$fund_balance_beg += ($bank_account->beg_deposit-$bank_account->beg_disbursement);
?>
</td>
                  <td class="text-right"><a class="add_deposits" href="#addDepositModal" data-toggle="modal" data-title="<?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) Deposits" data-bankid="<?php echo $bank_account->id; ?>">
                  <?php echo number_format($bank_account->deposit,2); ?>
<?php 
$balance = $balance + $bank_account->deposit; 
$fund_balance_d1 += $bank_account->deposit;
?>
                  </a></td>
                  <td class="text-right"><a class="add_disbursement" href="#addDisbursementModal" data-toggle="modal" data-title="<?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) Disbursements" data-bankid="<?php echo $bank_account->id; ?>">
                  <?php echo number_format($bank_account->disbursement,2); ?>
<?php 
$balance = $balance - $bank_account->disbursement; 
$fund_balance_d2 += $bank_account->disbursement;
?>
                  </a></td>
                  <td class="text-right">
<?php echo number_format($balance,2); 
$fund_balance_end += $balance;
?></td>
                        </tr>
                      <?php } ?>

            <tr class="success">
                  <td class="bold allcaps">TOTAL <?php echo $fund->name; ?></td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_beg,2); 
  $total_beg += $fund_balance_beg;
?>
</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_d1,2);
  $total_deposits += $fund_balance_d1; 
?>
</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_d2,2); 
  $total_disbursements += $fund_balance_d2; 
?></td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_end,2); 
  $total_end += $fund_balance_end;
?></td>
                </tr>

                  <?php } ?>
              <?php } ?>
<tr>
  <td colspan="5"><hr></td>
</tr>
<tr class="">
      <td class="bold allcaps font130p">TOTAL BALANCES</td>
      <td class="text-right bold font130p"><?php echo number_format($total_beg,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_deposits,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_disbursements,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_end,2); ?></td>
</tr>
              </tbody>
            </table>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('reports/view_modals'); ?>
<?php $this->load->view('footer'); ?>