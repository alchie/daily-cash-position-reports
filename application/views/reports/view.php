<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

<?php $this->load->view('reports/shortcuts'); ?>

<?php 
$total_beg = 0;
$total_deposits = 0;
$total_disbursements = 0;
$total_end = 0;
?>

          <h3 class="panel-title">Disposable Accounts - <?php echo date('F d, Y', strtotime("{$current_month}/{$current_day}/{$current_year}")); ?></h3>
        </div>
        <div class="panel-body">

<ul class="nav nav-tabs" style="margin-bottom:10px;">
  <li role="presentation" class="active"><a href="<?php echo site_url("reports/view/{$current_month}/{$current_day}/{$current_year}"); ?>">Disposable Funds</a></li>
    <li role="presentation"><a href="<?php echo site_url("reports/dollar/{$current_month}/{$current_day}/{$current_year}"); ?>">Dollar Accounts</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/time_deposits/{$current_month}/{$current_day}/{$current_year}"); ?>">Time Deposits</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/long_term/{$current_month}/{$current_day}/{$current_year}"); ?>">Long-term Investments</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/all_accounts/{$current_month}/{$current_day}/{$current_year}"); ?>">All Accounts</a></li>
  <li role="presentation"><a href="<?php echo site_url("reports/summary/{$current_month}/{$current_day}/{$current_year}"); ?>">Summary</a></li>
</ul>

            <table class="table table-default table-condensed">
              <thead>
                <tr>
                  <th>Undeposited Funds</th>
                  <th class="text-right" width="15%">Beg. Balance</th>
                  <th class="text-right" width="15%">Collections</th>
                  <th class="text-right" width="15%">Deposits</th>
                  <th class="text-right" width="15%">Ending Balance</th>
                </tr>
              </thead>
              <tbody>

                <tr>
                  <td class="info bold" colspan="5">The Roman Catholic Bishop of Davao, Inc. (RCBDI)</td>
                </tr>

<?php $cash_balance = 0; ?>
<tr>
                  <td class="bold allcaps">- - - - <a href="<?php echo site_url("bank_accounts/collections/cash"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>&amp;company=rcbdi">Cash Collections</a></td>
                  <td class="text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_cash_deposit - $current_report->beg_cash_disbursement),2);
  $cash_balance += ($current_report->beg_cash_deposit - $current_report->beg_cash_disbursement); 
  } else {
    echo '0.00';
  } 

?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->cash_deposit,2); 
                  $cash_balance += $current_report->cash_deposit; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->cash_disbursement,2); 
                  $cash_balance -= $current_report->cash_disbursement; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right"><?php 
                  $cash_balance = (intval($cash_balance) == 0) ? 0 : $cash_balance;
                  echo number_format($cash_balance,2); 
                  ?></td>
                </tr>

<?php $check_balance = 0; ?>
<tr>
                  <td class="bold allcaps">- - - - <a href="<?php echo site_url("bank_accounts/collections/check"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>&amp;company=rcbdi">Check Collections</a></td>
                  <td class="text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_check_deposit - $current_report->beg_check_disbursement),2);
  $check_balance += ($current_report->beg_check_deposit - $current_report->beg_check_disbursement); 
  } else {
    echo '0.00';
  } 

?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->check_deposit,2); 
                  $check_balance += $current_report->check_deposit; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->check_disbursement,2); 
                  $check_balance -= $current_report->check_disbursement; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right"><?php 
$check_balance = (intval($check_balance) == 0) ? 0 : $check_balance;
                  echo number_format($check_balance,2); ?></td>
                </tr>

<tr class="warning">
<td class="bold allcaps">- - - - <a href="<?php echo site_url("bank_accounts/collections"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>">Total RCBDI Collections</a></td>
<td class="text-right bold">
<?php 
if($current_report) {
                  echo number_format(($current_report->beg_cash_deposit - $current_report->beg_cash_disbursement)+($current_report->beg_check_deposit - $current_report->beg_check_disbursement),2); 
} else {
  echo '0.00';
}
?>
</td>
<td class="text-right bold">
<?php 
if($current_report) {
                  echo number_format(($current_report->cash_deposit+$current_report->check_deposit),2); 
} else {
  echo '0.00';
}
?>
</td>
<td class="text-right bold">
  <?php 
if($current_report) {
                  echo number_format(($current_report->cash_disbursement+$current_report->check_disbursement),2); 
} else {
  echo '0.00';
}
?>
</td>
<td class="text-right bold">
  <?php 
if($current_report) {
                  echo number_format(($cash_balance+$check_balance),2); 
} else {
  echo '0.00';
}
?>
</td>
</tr>


                <tr>
                  <td class="info bold" colspan="5">Archdiocesan Stewardship Apostolates, Inc. (ADSAI)</td>
                </tr>


<?php $cash_balance_adsai = 0; ?>
<tr>
                  <td class="bold allcaps">- - - - <a href="<?php echo site_url("bank_accounts/collections/cash"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>&amp;company=adsai">Cash Collections</a></td>
                  <td class="text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_cash_deposit_adsai - $current_report->beg_cash_disbursement_adsai),2);
  $cash_balance_adsai += ($current_report->beg_cash_deposit_adsai - $current_report->beg_cash_disbursement_adsai); 
  } else {
    echo '0.00';
  } 

?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->cash_deposit_adsai,2); 
                  $cash_balance_adsai += $current_report->cash_deposit_adsai; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->cash_disbursement_adsai,2); 
                  $cash_balance_adsai -= $current_report->cash_disbursement_adsai; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right"><?php 
                  $cash_balance_adsai = (intval($cash_balance_adsai) == 0) ? 0 : $cash_balance_adsai;
                  echo number_format($cash_balance_adsai,2); 
                  ?></td>
                </tr>

<?php $check_balance_adsai = 0; ?>
<tr>
                  <td class="bold allcaps">- - - - <a href="<?php echo site_url("bank_accounts/collections/check"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>&amp;company=adsai">Check Collections</a></td>
                  <td class="text-right">
<?php if($current_report) {
  echo number_format(($current_report->beg_check_deposit_adsai - $current_report->beg_check_disbursement_adsai),2);
  $check_balance_adsai += ($current_report->beg_check_deposit_adsai - $current_report->beg_check_disbursement_adsai); 
  } else {
    echo '0.00';
  } 

?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->check_deposit_adsai,2); 
                  $check_balance_adsai += $current_report->check_deposit_adsai; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right">
<?php 
if($current_report) {
                  echo number_format($current_report->check_disbursement_adsai,2); 
                  $check_balance_adsai -= $current_report->check_disbursement_adsai; 
} else {
  echo '0.00';
}
?>
                  </td>
                  <td class="text-right"><?php 
$check_balance_adsai = (intval($check_balance_adsai) == 0) ? 0 : $check_balance_adsai;
                  echo number_format($check_balance_adsai,2); ?></td>
                </tr>


<tr class="warning">
<td class="bold allcaps">- - - - <a href="<?php echo site_url("bank_accounts/collections"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>&amp;company=adsai">Total ADSAI Collections</a></td>
<td class="text-right bold">
<?php 
if($current_report) {
                  echo number_format(($current_report->beg_cash_deposit_adsai - $current_report->beg_cash_disbursement_adsai)+($current_report->beg_check_deposit_adsai - $current_report->beg_check_disbursement_adsai),2); 
} else {
  echo '0.00';
}
?>
</td>
<td class="text-right bold">
<?php 
if($current_report) {
                  echo number_format(($current_report->cash_deposit_adsai+$current_report->check_deposit_adsai),2); 
} else {
  echo '0.00';
}
?>
</td>
<td class="text-right bold">
  <?php 
if($current_report) {
                  echo number_format(($current_report->cash_disbursement_adsai+$current_report->check_disbursement_adsai),2); 
} else {
  echo '0.00';
}
?>
</td>
<td class="text-right bold">
  <?php 
if($current_report) {
                  echo number_format(($cash_balance_adsai+$check_balance_adsai),2); 
} else {
  echo '0.00';
}
?>
</td>
</tr>

              <tr class="success">
<?php $balance = 0; ?>
                  <td class="bold allcaps">
<a href="<?php echo site_url("bank_accounts/collections"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>">
                  Total Collections
</a>
                  </td>
                  <td class="bold text-right">
<?php if($current_report) {
  echo number_format((($current_report->beg_deposit - $current_report->beg_disbursement) + ($current_report->beg_deposit_adsai - $current_report->beg_disbursement_adsai)),2);
  $balance = $balance + ($current_report->beg_deposit - $current_report->beg_disbursement) + ($current_report->beg_deposit_adsai - $current_report->beg_disbursement_adsai); 
  } else {
    echo '0.00';
  } 

?>

</td>
                  <td class="bold text-right"><a class="add_deposits" href="#addDepositModal" data-toggle="modal" data-title="Add Collections" data-bankid="0">
<?php 
if($current_report) {
                  echo number_format(($current_report->deposit+$current_report->deposit_adsai),2); 
                  $balance = $balance + $current_report->deposit + $current_report->deposit_adsai; 
} else {
  echo '0.00';
}
?>
                  </a></td>
                  <td class="bold text-right"><a class="add_disbursement" href="#addDisbursementModal" data-toggle="modal" data-title="Deposits" data-bankid="0">
<?php 
if($current_report) {
                  echo number_format(($current_report->disbursement+$current_report->disbursement_adsai),2); 
                  $balance = $balance - $current_report->disbursement + $current_report->disbursement_adsai; 
} else {
  echo '0.00';
}
?>
                  </a></td>
                  <td class="bold text-right"><?php 
$balance = (intval($balance) == 0) ? 0 : $balance;
                  echo number_format($balance,2); ?></td>
                </tr>

              
                  <tr>
                    <td colspan="5"><hr></td>
                  </tr>

</tbody>
</table>

<table class="table table-default table-condensed">
              <thead>
                <tr>
                  <th>Funds</th>
                  <th class="text-right" width="15%">Beg. Balance</th>
                  <th class="text-right" width="15%">Deposits</th>
                  <th class="text-right" width="15%">Disbursements</th>
                  <th class="text-right" width="15%">Ending Balance</th>
                </tr>
              </thead>
              <tbody>


<?php foreach( $funds as $fund ) { 
$fund_balance_beg = 0;
$fund_balance_d1 = 0;
$fund_balance_d2 = 0;
$fund_balance_end = 0;
  ?>
<?php if( $fund->bank_accounts_count > 0) { ?>
                <tr class="warning">
                  <td class="bold">
                    <?php echo $fund->name; ?>
                  </td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                </tr>
<?php foreach( $fund->bank_accounts as $bank_account ) { 
$balance = 0;
                        ?>
                        <tr>
                          <td style="padding-left:30px">
<a href="<?php echo site_url("bank_accounts/details/{$bank_account->id}"); ?>?start=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&end=<?php echo urlencode("{$reports->month}/{$reports->day}/{$reports->year}"); ?>&amp;next=<?php echo urlencode(uri_string()); ?>">
                          <?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>)
</a>
                          </td>
                  <td class="text-right">
<?php echo number_format(($bank_account->beg_deposit-$bank_account->beg_disbursement),2); ?>
<?php 
$balance = $balance + ($bank_account->beg_deposit-$bank_account->beg_disbursement); 
$fund_balance_beg += ($bank_account->beg_deposit-$bank_account->beg_disbursement);
?>

</td>
                  <td class="text-right"><a class="add_deposits" href="#addDepositModal" data-toggle="modal" data-title="<?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) Deposits" data-bankid="<?php echo $bank_account->id; ?>">
                  <?php echo number_format($bank_account->deposit,2); ?>
<?php 
$balance = $balance + $bank_account->deposit; 
$fund_balance_d1 += $bank_account->deposit;
?>
                  </a></td>
                  <td class="text-right"><a class="add_disbursement" href="#addDisbursementModal" data-toggle="modal" data-title="<?php echo $bank_account->bank_name; ?> (<?php echo $bank_account->account_number; ?>) Disbursements" data-bankid="<?php echo $bank_account->id; ?>">
                  <?php echo number_format($bank_account->disbursement,2); ?>
<?php 
$balance = $balance - $bank_account->disbursement; 
$fund_balance_d2 += $bank_account->disbursement;
?>
                  </a></td>
                  <td class="text-right">
<?php echo number_format($balance,2); 
$fund_balance_end += $balance;
?></td>
                        </tr>
                      <?php } ?>

            <tr class="success">
                  <td class="bold allcaps">TOTAL <?php echo $fund->name; ?></td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_beg,2); 
  $total_beg += $fund_balance_beg;
?>
</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_d1,2);
  $total_deposits += $fund_balance_d1; 
?>
</td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_d2,2); 
  $total_disbursements += $fund_balance_d2; 
?></td>
                  <td class="text-right bold">
<?php 
  echo number_format($fund_balance_end,2); 
  $total_end += $fund_balance_end;
?></td>
                </tr>
  <tr>
  <td colspan="5"><hr></td>
</tr>
                  <?php } ?>
              <?php } ?>
<tr class="">
      <td class="bold allcaps font130p">TOTAL BALANCES</td>
      <td class="text-right bold font130p"><?php echo number_format($total_beg,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_deposits,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_disbursements,2); ?></td>
      <td class="text-right bold font130p"><?php echo number_format($total_end,2); ?></td>
</tr>
              </tbody>
            </table>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('reports/view_modals'); ?>
<?php $this->load->view('footer'); ?>