<script>
  var ajaxURL = '<?php echo site_url("reports/existing/{$current_month}/{$current_day}/{$current_year}"); ?>';
  var deleteDepositURL = '<?php echo site_url("reports/delete_deposit/{$current_month}/{$current_day}/{$current_year}"); ?>';
  var deleteDisbursementURL = '<?php echo site_url("reports/delete_disbursement/{$current_month}/{$current_day}/{$current_year}"); ?>';
  var currentUri = '<?php echo urlencode( uri_string() ); ?>';
</script>
<!-- addDepositModal -->
<div class="modal fade" id="addDepositModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addDepositModal-title">Deposit</h4>
      </div>
<form method="post" action="<?php echo site_url("reports/add_deposit/{$current_month}/{$current_day}/{$current_year}") . "?next=" . uri_string() ; ?>">      
      <div class="modal-body">
        <input type="hidden" name="bankid" value="" id="addDepositModal-bankid">
<div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Company</label>
                <select name="company" class="form-control">
                  <?php foreach(array(
                    'rcbdi'=>'The Roman Catholic Bishop of Davao, Inc.',
                    'adsai'=>'Archdiocesan Stewardship Apostolates, Inc.') as $company_id=>$company_name) { ?>
                    <option value="<?php echo $company_id; ?>"  ><?php echo $company_name; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
<div class="col-md-6">
        <div class="form-group">
          <label>Amount</label>
          <input type="text" name="amount" class="form-control text-right" value="0.00" id="addDepositModal-amount">
        </div>
</div>
</div>
        <div class="form-group">
          <label>Description</label>
          <input type="text" name="description" class="form-control" value="" id="addDepositModal-description">
        </div>
        <div class="form-group" id="addDepositModal-type">
          <label>Deposit Type</label>
          <label id="addDepositModal-type-cash"><input type="radio" name="type" value="cash" class="addDepositModal-type"> Cash</label>
          <label><input type="radio" name="type" value="check" checked class="addDepositModal-type" id="addDepositModal-type-check"> Check</label>
          <label id="addDepositModal-type-adj"><input type="radio" name="type" value="adj" class="addDepositModal-type"> Adjustment</label>
        </div>
      </div>
      <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" id="addDepositModal-submit">Save changes</button>
      </div>
      <div class="modal-body">
             <div class="form-group">
          <div id="addDepositModal-existing" class="text-center" ajaxLoader="<?php echo base_url("assets/images/ajax-loader.gif"); ?>"></div>
        </div>
      </div>
</form>
    </div>
  </div>
</div>

<!-- addDisbursementModal -->
<div class="modal fade" id="addDisbursementModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addDisbursementModal-title">Disbursement</h4>
      </div>
<form method="post" action="<?php echo site_url("reports/add_disbursement/{$current_month}/{$current_day}/{$current_year}") . "?next=" . uri_string(); ?>" id="addDisbursementModal-form">
      <div class="modal-body">
<div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Company</label>
                <select name="company" class="form-control">
                  <?php foreach(array(
                    'rcbdi'=>'The Roman Catholic Bishop of Davao, Inc.',
                    'adsai'=>'Archdiocesan Stewardship Apostolates, Inc.') as $company_id=>$company_name) { ?>
                    <option value="<?php echo $company_id; ?>"  ><?php echo $company_name; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
<div class="col-md-6">
        <div class="form-group">
          <label>Amount</label>
          <input type="hidden" name="bankid" value="" id="addDisbursementModal-bankid">
          <input type="text" name="amount" class="form-control text-right" value="0.00" id="addDisbursementModal-amount">
        </div>
</div>
</div>
        <div class="form-group">
          <label>Description</label>
          <input type="text" name="description" class="form-control" value="" id="addDisbursementModal-description">
        </div>

        <div class="form-group" id="addDisbursementModal-type">
          <label>Deposit Type</label>
          <label><input type="radio" name="type" value="check" checked class="addDisbursementModal-type" id="addDisbursementModal-type-check"> Check</label>
          <label><input type="radio" name="type" value="adj" id="addDisbursementModal-type-adj"> Adjustment</label>
          <label><input type="radio" name="type" value="fund_transfer" class="addDisbursementModal-type" id="addDisbursementModal-type-fund_transfer"> Fund Transfer</label>
        </div>
        <div class="form-group" id="addDisbursementModal-write_check_display">
            <label><input type="checkbox" name="write_check" value="1"> Write Check</label>
        </div>
        <div class="form-group" id="addDisbursementModal-banks" style="display:none;">
        <select class="form-control" name="dest_id">
            <option value="">- - Select a Destination - -</option>
          <?php foreach($funds as $fund) { ?>
              <?php if( count( $fund->bank_accounts ) ) { ?>
                <optgroup label="<?php echo $fund->name; ?>">
                <?php foreach( $fund->bank_accounts as $bank_account ) { ?>
                <option value="<?php echo $bank_account->id; ?>"><?php echo $bank_account->bank_name; ?>(<?php echo $bank_account->account_number; ?>)</option>
              <?php } ?>
                </optgroup>
              <?php } ?>
          <?php } ?>
        </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="addDisbursementModal-submit">Save changes</button>
      </div>
</form>
      <div class="modal-body">
             <div class="form-group">
         <div id="addDisbursementModal-existing" class="text-center" ajaxLoader="<?php echo base_url("assets/images/ajax-loader.gif"); ?>"></div>
        </div>
      </div>

    </div>
  </div>
</div>