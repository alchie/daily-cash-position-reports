<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

        <a href="<?php echo site_url("signatory/add"); ?>" class="btn btn-success btn-xs pull-right">Add Signatory</a>

          <h3 class="panel-title"><strong>Signatory</strong></h3>

        </div>
        <div class="panel-body">
<?php if( $signatories ) { ?>

     <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Signatory Name</th>
                  <th class="text-center">Signatory 1</th>
                  <th class="text-center">Signatory 2</th>
                  <th width="110px">Actions</th>
                </tr>
              </thead>
              <tbody>
<?php foreach($signatories as $sig) { ?>
<tr>
  <td class="text-left"><?php echo $sig->signatory; ?></td>
  <td class="text-center"><span style="color:<?php echo ($sig->signatory1) ? 'green' : 'red'; ?>;" class="glyphicon glyphicon-<?php echo ($sig->signatory1) ? 'ok' : 'remove'; ?>"></span></td>
  <td class="text-center"><span style="color:<?php echo ($sig->signatory2) ? 'green' : 'red'; ?>;" class="glyphicon glyphicon-<?php echo ($sig->signatory2) ? 'ok' : 'remove'; ?>"></span></td>
  <td class="text-left">
    <a href="<?php echo site_url("signatory/edit/{$sig->id}"); ?>" class="btn btn-success btn-xs">Edit</a>
    <a href="<?php echo site_url("signatory/delete/{$sig->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
  </td>
</tr>
<?php } ?>
              </tbody>
              </table>

<?php echo $pagination; ?>

<?php } else { ?>
<p class="text-center">No Entries</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>