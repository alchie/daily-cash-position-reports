<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Edit Signatory</h3>
        </div>
      <form method="post">
        <div class="panel-body">
<div class="row">
<div class="col-md-10">
        <div class="form-group">
          <label>Signatory Name</label>
          <input class="form-control" name="name" value="<?php echo $signatory->signatory; ?>" />
        </div>
</div>
<div class="col-md-2">
         <div class="form-group">
          <label>Priority</label>
          <input class="form-control" name="priority" value="<?php echo ($signatory->priority) ? $signatory->priority : 0; ?>" />
        </div>
</div>
</div>

<div class="form-group">
          <label><input type="checkbox" name="signatory1" value="1" <?php echo ($signatory->signatory1) ? 'CHECKED' : ''; ?> /> Signatory 1</label>
          <label><input type="checkbox" name="signatory2" value="1" <?php echo ($signatory->signatory2) ? 'CHECKED' : ''; ?> /> Signatory 2</label>
        </div>

        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo site_url("signatory"); ?>" class="btn btn-warning">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>