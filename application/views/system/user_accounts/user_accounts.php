<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">

        <a href="<?php echo site_url("system_users/add"); ?>" class="btn btn-success btn-xs pull-right">Add User</a>

          <h3 class="panel-title"><strong>User Accounts</strong></h3>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
<?php if( $users ) { ?>
      <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th width="110px">Actions</th>
                </tr>
              </thead>
              <tbody>
<?php foreach($users as $user) { ?>
<tr>
  <td class="text-left"><?php echo $user->name; ?></td>
  <td class="text-left"><?php echo $user->username; ?></td>
  <td class="text-left">
    <a href="<?php echo site_url("system_users/edit/{$user->id}"); ?>" class="btn btn-success btn-xs">Edit</a>
    <a href="<?php echo site_url("system_users/delete/{$user->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
  </td>
</tr>
<?php } ?>
              </tbody>
              </table>

<?php echo $pagination; ?>

<?php } else { ?>
<p class="text-center">No Entries</p>
<?php } ?>
        </div>
      </div>
      </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>