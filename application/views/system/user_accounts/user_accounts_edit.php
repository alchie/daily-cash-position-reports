<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
    <div class="container">
    <div class="row">
    
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add User</h3>
        </div>
      <form method="post">
        <div class="panel-body">

        <div class="form-group <?php echo (form_error('full_name')) ? 'has-error' : ''; ?>">
            <label class="control-label">Full Name</label>
            <input class="form-control" type="text" name="full_name" value="<?php echo $user->name; ?>" REQUIRED>
        </div>        
        <div class="form-group <?php echo (form_error('username')) ? 'has-error' : ''; ?>">
            <label class="control-label">Username</label>
            <input class="form-control" type="text" name="username" value="<?php echo $user->username; ?>" REQUIRED>
        </div>
        <div class="form-group <?php echo (form_error('password')) ? 'has-error' : ''; ?>">
            <label class="control-label">Password</label>
            <input class="form-control" type="password" name="password" REQUIRED>
        </div>
        <div class="form-group <?php echo (form_error('password2')) ? 'has-error' : ''; ?>">
            <label class="control-label">Repeat Password</label>
            <input class="form-control" type="password" name="password2" REQUIRED>
        </div>
        </div>
        <div class="panel-footer">
          <input class="btn btn-success" type="submit" value="Submit">
          <a href="<?php echo site_url("payee"); ?>" class="btn btn-warning">Back</a>
        </div>
      </form>
      </div>
      </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>