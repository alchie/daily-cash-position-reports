(function($){
  $('select').selectpicker({
    liveSearch : true,
  });
  $('.datepicker').datepicker();
  $('.confirm').click(function(){
    if(confirm('Are you sure?')) {
      return true;
    } else {
      return false;
    }
  });
  $('.add_deposits').click(function(){
    $('#addDepositModal-title').text($(this).attr('data-title'));
    $('#addDepositModal-bankid').val($(this).attr('data-bankid'));  
    $('#addDepositModal-existing').html($('<img>').prop('src', $('#addDepositModal-existing').attr('ajaxLoader')));
    //$('#addDepositModal-amount').val($(this).text().replace(/,/g,'').trim()); 
    $('#addDepositModal-banks').hide();
    $('#addDepositModal-type-adj').show();
    $('#addDepositModal-type-fundtransfer').show();
    if( $(this).attr('data-bankid') == '0') {
      $('#addDepositModal-type-adj').hide();
      $('#addDepositModal-type-fundtransfer').hide();
    } 
    $('.addDepositModal-type').prop('checked', false).click(function(){
      $('#addDepositModal-banks').hide();
    });
    $('#addDepositModal-type-check').prop('checked', true);
    $('#addDepositModal-type-fundtransfer-checked').click(function(){
        $('#addDepositModal-banks').show();
    });
  });
  $('.add_disbursement').click(function(){
    var bankid = $(this).attr('data-bankid');
    $('#addDisbursementModal-title').text($(this).attr('data-title'));
    $('#addDisbursementModal-bankid').val(bankid);
    //$('#addDisbursementModal-amount').val($(this).text().replace(/,/g,'').trim());
    if( bankid=='0') {
      $('#addDisbursementModal-form').hide();
    } else {
      $('#addDisbursementModal-form').show();
    }
    $('.addDisbursementModal-type-adj').click(function(){
      $('#addDisbursementModal-write_check_display').hide();
    });
    $('#addDisbursementModal-type-adj').click(function(){
      $('#addDisbursementModal-write_check_display').hide();
      $('#addDisbursementModal-banks').hide();
    });
    $('#addDisbursementModal-type-check').click(function(){
      $('#addDisbursementModal-write_check_display').show();
      $('#addDisbursementModal-banks').hide();
    });
    $('#addDisbursementModal-type-fund_transfer').click(function(){
        $('#addDisbursementModal-banks').show();
        $('#addDisbursementModal-write_check_display').show();
    });
    $('#addDisbursementModal-existing').html($('<img>').prop('src', $('#addDisbursementModal-existing').attr('ajaxLoader')));
  });
  
  var showTable = function(data, what, bankid) {
    
    var deleteURL = deleteDepositURL;
    if(what == 'disbursement') {
      var deleteURL = deleteDisbursementURL;
    }

    //tbody
    var tbody = $('<tbody>');
      for(i in data) {
          var tr = $('<tr>');

          var td1 = $('<td class="text-left">').text( data[i].type );

          if(what == 'disbursement') {
            if ((data[i].type == 'check') && (bankid > 0)) {
              var tlink = $('<a>').prop('href', data[i].write_check).text( data[i].type );
              td1.html( tlink );
            }
          } 
          tr.append(td1);
          
        

        var td2 = $('<td class="text-left">').text( data[i].description );
        tr.append(td2);

        var td3 = $('<td class="text-right">').text(parseFloat(data[i].amount).toFixed(2));
        tr.append(td3);

if((what == 'deposit') || (data[i].bank_id != '0'))  {
        var td4 = $('<td class="text-right">');
        var elink = $('<a>').prop('href', data[i].edit_uri).addClass('btn btn-warning btn-xs').text('Edit').prop('style', 'margin-right:5px');
        var dlink = $('<a>').prop('href', deleteURL + '?id=' + data[i].id + '&next=' + currentUri).addClass('btn btn-danger btn-xs').text('Delete').click(function(){
          if(confirm('Are you sure you want to delete this?')) {
            return true;
          } else {
            return false;
          }
        });
        td4.append( elink );
        td4.append( dlink );
        tr.append(td4);
}
if((what == 'disbursement') && (data[i].bank_id == '0'))  {
  var td5 = $('<td class="text-right">').text( data[i].deposited_to );
  tr.append(td5);
}
        tbody.append(tr);
      }

    //thead
    var thead = $('<thead>');
    var tr = $('<tr>');

    //if(what == 'deposit') {
      var th1 = $('<th class="text-left">').text('TYPE');
      tr.append(th1);
    //}

    var th2 = $('<th class="text-left">').text('DESCRIPTION');
    tr.append(th2);

    var th3 = $('<th class="text-right">').text('AMOUNT');
    tr.append(th3);

if(what == 'deposit') {
    var th4 = $('<th class="text-right">').text('ACTION');
    tr.append(th4);
}
if(what == 'disbursement')  {
  if(bankid == '0')  {
    var th4 = $('<th class="text-right">').text('DEPOSITED TO');
  } else {
    var th4 = $('<th class="text-right">').text('ACTION');
  }
  tr.append(th4);
}

    thead.append(tr);

    var table = $('<table>').addClass('table table-striped table-condensed');
      table.append( thead );
      table.append( tbody );
      if(what == 'deposit') {
        $('#addDepositModal-existing').html(table);
      }
      if(what == 'disbursement') {
        $('#addDisbursementModal-existing').html(table);
      }
  };

  $('#addDepositModal').on('shown.bs.modal', function () {
    $('#addDepositModal-amount').focus().select();
    var bankid = $('#addDepositModal-bankid').val();
    $.ajax({
      url: ajaxURL,
      method: 'GET',
      data : {
        what : 'deposit',
        bankid : bankid,
      },
      dataType: 'json',
      success : function(msg) {
        showTable(msg, 'deposit', bankid);
      }
    });
  });
  $('#addDisbursementModal').on('shown.bs.modal', function () {
    $('#addDisbursementModal-amount').focus().select();
    var bankid = $('#addDisbursementModal-bankid').val();
    $.ajax({
      url: ajaxURL,
      method: 'GET',
      data : {
        what : 'disbursement',
        bankid : bankid,
      },
      dataType: 'json',
      success : function(msg) {
        showTable(msg, 'disbursement', bankid);
      }
    });
  });
})(jQuery);