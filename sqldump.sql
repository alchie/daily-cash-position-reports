-- Table structure for table `account_sessions` 

CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `bank_accounts` 

CREATE TABLE `bank_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fund_id` int(10) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `fund_category` varchar(100) DEFAULT 'disposable',
  `priority` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `funds` 

CREATE TABLE `funds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `priority` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `reports` 

CREATE TABLE `reports` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `beginning` int(10) DEFAULT NULL,
  `report_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `reports_deposits` 

CREATE TABLE `reports_deposits` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bank_id` int(20) NOT NULL DEFAULT '0',
  `amount` decimal(20,4) NOT NULL,
  `report_date` date NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `dbm_id` int(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `reports_disbursements` 

CREATE TABLE `reports_disbursements` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bank_id` int(20) NOT NULL DEFAULT '0',
  `amount` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `report_date` date NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `dp_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `user_accounts` 

CREATE TABLE `user_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
);

